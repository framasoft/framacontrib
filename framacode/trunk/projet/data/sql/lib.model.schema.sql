
# This is a fix for InnoDB in MySQL >= 4.1.x
# It "suspends judgement" for fkey relationships until are tables are set.
SET FOREIGN_KEY_CHECKS = 0;

#-----------------------------------------------------------------------------
#-- project
#-----------------------------------------------------------------------------

DROP TABLE IF EXISTS `project`;


CREATE TABLE `project`
(
	`id` INTEGER  NOT NULL AUTO_INCREMENT,
	`name` VARCHAR(255),
	`label` VARCHAR(255),
	`create_at` DATETIME,
	`update_at` DATETIME,
	`versionning_url` VARCHAR(255),
	`project_url` VARCHAR(255),
	`website` VARCHAR(255),
	`visible` TINYINT,
	`position` INTEGER,
	PRIMARY KEY (`id`),
	UNIQUE KEY `project_name` (`name`),
	UNIQUE KEY `project_label` (`label`)
)Type=InnoDB;

#-----------------------------------------------------------------------------
#-- license
#-----------------------------------------------------------------------------

DROP TABLE IF EXISTS `license`;


CREATE TABLE `license`
(
	`id` INTEGER  NOT NULL AUTO_INCREMENT,
	`name` VARCHAR(255),
	`label` VARCHAR(255),
	`url` VARCHAR(255),
	`logo` VARCHAR(255),
	PRIMARY KEY (`id`),
	UNIQUE KEY `license_name` (`name`),
	UNIQUE KEY `license_label` (`label`)
)Type=InnoDB;

#-----------------------------------------------------------------------------
#-- project_has_license
#-----------------------------------------------------------------------------

DROP TABLE IF EXISTS `project_has_license`;


CREATE TABLE `project_has_license`
(
	`project_id` INTEGER  NOT NULL,
	`license_id` INTEGER  NOT NULL,
	PRIMARY KEY (`project_id`,`license_id`),
	CONSTRAINT `project_has_license_FK_1`
		FOREIGN KEY (`project_id`)
		REFERENCES `project` (`id`)
		ON DELETE CASCADE,
	INDEX `project_has_license_FI_2` (`license_id`),
	CONSTRAINT `project_has_license_FK_2`
		FOREIGN KEY (`license_id`)
		REFERENCES `license` (`id`)
		ON DELETE CASCADE
)Type=InnoDB;

#-----------------------------------------------------------------------------
#-- contributor
#-----------------------------------------------------------------------------

DROP TABLE IF EXISTS `contributor`;


CREATE TABLE `contributor`
(
	`id` INTEGER  NOT NULL AUTO_INCREMENT,
	`pseudo` VARCHAR(255),
	`email` VARCHAR(255),
	`first_name` VARCHAR(255),
	`last_name` VARCHAR(255),
	`website` VARCHAR(255),
	`show_real_name` TINYINT,
	PRIMARY KEY (`id`),
	UNIQUE KEY `contributor_pseudo` (`pseudo`)
)Type=InnoDB;

#-----------------------------------------------------------------------------
#-- project_has_contributor
#-----------------------------------------------------------------------------

DROP TABLE IF EXISTS `project_has_contributor`;


CREATE TABLE `project_has_contributor`
(
	`project_id` INTEGER  NOT NULL,
	`contributor_id` INTEGER  NOT NULL,
	`contributor_type_id` INTEGER  NOT NULL,
	PRIMARY KEY (`project_id`,`contributor_id`),
	CONSTRAINT `project_has_contributor_FK_1`
		FOREIGN KEY (`project_id`)
		REFERENCES `project` (`id`)
		ON DELETE CASCADE,
	INDEX `project_has_contributor_FI_2` (`contributor_id`),
	CONSTRAINT `project_has_contributor_FK_2`
		FOREIGN KEY (`contributor_id`)
		REFERENCES `contributor` (`id`)
		ON DELETE CASCADE,
	INDEX `project_has_contributor_FI_3` (`contributor_type_id`),
	CONSTRAINT `project_has_contributor_FK_3`
		FOREIGN KEY (`contributor_type_id`)
		REFERENCES `contributor_type` (`id`)
		ON DELETE CASCADE
)Type=InnoDB;

#-----------------------------------------------------------------------------
#-- contributor_type
#-----------------------------------------------------------------------------

DROP TABLE IF EXISTS `contributor_type`;


CREATE TABLE `contributor_type`
(
	`id` INTEGER  NOT NULL AUTO_INCREMENT,
	`label` VARCHAR(255),
	PRIMARY KEY (`id`),
	UNIQUE KEY `contributor_type_label` (`label`)
)Type=InnoDB;

#-----------------------------------------------------------------------------
#-- contributor_type_i18n
#-----------------------------------------------------------------------------

DROP TABLE IF EXISTS `contributor_type_i18n`;


CREATE TABLE `contributor_type_i18n`
(
	`id` INTEGER  NOT NULL,
	`culture` VARCHAR(7)  NOT NULL,
	`name` VARCHAR(255),
	PRIMARY KEY (`id`,`culture`),
	CONSTRAINT `contributor_type_i18n_FK_1`
		FOREIGN KEY (`id`)
		REFERENCES `contributor_type` (`id`)
		ON DELETE CASCADE
)Type=InnoDB;

#-----------------------------------------------------------------------------
#-- file
#-----------------------------------------------------------------------------

DROP TABLE IF EXISTS `file`;


CREATE TABLE `file`
(
	`id` INTEGER  NOT NULL AUTO_INCREMENT,
	`project_id` INTEGER  NOT NULL,
	`name` VARCHAR(255),
	`label` VARCHAR(255),
	`size` FLOAT,
	`create_at` DATETIME,
	`nb_download` INTEGER,
	`version` VARCHAR(20),
	PRIMARY KEY (`id`),
	UNIQUE KEY `file_filename` (`name`),
	UNIQUE KEY `file_label` (`label`),
	KEY `file_version`(`version`),
	INDEX `file_FI_1` (`project_id`),
	CONSTRAINT `file_FK_1`
		FOREIGN KEY (`project_id`)
		REFERENCES `project` (`id`)
		ON DELETE CASCADE
)Type=InnoDB;

#-----------------------------------------------------------------------------
#-- file_i18n
#-----------------------------------------------------------------------------

DROP TABLE IF EXISTS `file_i18n`;


CREATE TABLE `file_i18n`
(
	`id` INTEGER  NOT NULL,
	`culture` VARCHAR(7)  NOT NULL,
	`note` TEXT,
	`changelog` TEXT,
	PRIMARY KEY (`id`,`culture`),
	CONSTRAINT `file_i18n_FK_1`
		FOREIGN KEY (`id`)
		REFERENCES `file` (`id`)
		ON DELETE CASCADE
)Type=InnoDB;

#-----------------------------------------------------------------------------
#-- documentation
#-----------------------------------------------------------------------------

DROP TABLE IF EXISTS `documentation`;


CREATE TABLE `documentation`
(
	`id` INTEGER  NOT NULL AUTO_INCREMENT,
	`project_id` INTEGER  NOT NULL,
	`label` VARCHAR(255),
	`filename` VARCHAR(255),
	`position` INTEGER,
	PRIMARY KEY (`id`),
	UNIQUE KEY `documentation_label` (`label`),
	INDEX `documentation_FI_1` (`project_id`),
	CONSTRAINT `documentation_FK_1`
		FOREIGN KEY (`project_id`)
		REFERENCES `project` (`id`)
		ON DELETE CASCADE
)Type=InnoDB;

#-----------------------------------------------------------------------------
#-- documentation_page
#-----------------------------------------------------------------------------

DROP TABLE IF EXISTS `documentation_page`;


CREATE TABLE `documentation_page`
(
	`id` INTEGER  NOT NULL AUTO_INCREMENT,
	`documentation_id` INTEGER  NOT NULL,
	`label` VARCHAR(255),
	`position` INTEGER,
	PRIMARY KEY (`id`),
	UNIQUE KEY `documentation_page_label` (`label`),
	INDEX `documentation_page_FI_1` (`documentation_id`),
	CONSTRAINT `documentation_page_FK_1`
		FOREIGN KEY (`documentation_id`)
		REFERENCES `documentation` (`id`)
		ON DELETE RESTRICT
)Type=InnoDB;

#-----------------------------------------------------------------------------
#-- documentation_i18n
#-----------------------------------------------------------------------------

DROP TABLE IF EXISTS `documentation_i18n`;


CREATE TABLE `documentation_i18n`
(
	`id` INTEGER  NOT NULL,
	`culture` VARCHAR(7)  NOT NULL,
	`name` VARCHAR(255),
	PRIMARY KEY (`id`,`culture`),
	CONSTRAINT `documentation_i18n_FK_1`
		FOREIGN KEY (`id`)
		REFERENCES `documentation` (`id`)
		ON DELETE CASCADE
)Type=InnoDB;

#-----------------------------------------------------------------------------
#-- documentation_page_i18n
#-----------------------------------------------------------------------------

DROP TABLE IF EXISTS `documentation_page_i18n`;


CREATE TABLE `documentation_page_i18n`
(
	`id` INTEGER  NOT NULL,
	`culture` VARCHAR(7)  NOT NULL,
	`name` VARCHAR(255),
	`content` TEXT,
	PRIMARY KEY (`id`,`culture`),
	CONSTRAINT `documentation_page_i18n_FK_1`
		FOREIGN KEY (`id`)
		REFERENCES `documentation_page` (`id`)
		ON DELETE CASCADE
)Type=InnoDB;

#-----------------------------------------------------------------------------
#-- project_i18n
#-----------------------------------------------------------------------------

DROP TABLE IF EXISTS `project_i18n`;


CREATE TABLE `project_i18n`
(
	`id` INTEGER  NOT NULL,
	`culture` VARCHAR(7)  NOT NULL,
	`description` TEXT,
	PRIMARY KEY (`id`,`culture`),
	CONSTRAINT `project_i18n_FK_1`
		FOREIGN KEY (`id`)
		REFERENCES `project` (`id`)
		ON DELETE CASCADE
)Type=InnoDB;

#-----------------------------------------------------------------------------
#-- article
#-----------------------------------------------------------------------------

DROP TABLE IF EXISTS `article`;


CREATE TABLE `article`
(
	`id` INTEGER  NOT NULL AUTO_INCREMENT,
	`label` VARCHAR(255),
	PRIMARY KEY (`id`),
	UNIQUE KEY `article_label` (`label`)
)Type=InnoDB;

#-----------------------------------------------------------------------------
#-- article_i18n
#-----------------------------------------------------------------------------

DROP TABLE IF EXISTS `article_i18n`;


CREATE TABLE `article_i18n`
(
	`id` INTEGER  NOT NULL,
	`culture` VARCHAR(7)  NOT NULL,
	`name` VARCHAR(255),
	`content` TEXT,
	PRIMARY KEY (`id`,`culture`),
	CONSTRAINT `article_i18n_FK_1`
		FOREIGN KEY (`id`)
		REFERENCES `article` (`id`)
		ON DELETE CASCADE
)Type=InnoDB;

# This restores the fkey checks, after having unset them earlier
SET FOREIGN_KEY_CHECKS = 1;
