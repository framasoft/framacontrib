<?php

/**
 * file components.
 *
 * @package    FramaCode
 * @subpackage file
 * @author     Simon Leblanc <contact@leblanc-simon.eu>
 * @version    SVN: $Id: components.class.php 12479 2008-10-31 10:54:40Z fabien $
 */

class fileComponents extends sfComponents
{
  /**
   * List the files associated with the project in the parameter
   *
   * @param   Project   $this->project    The project's object
   * @access  public
   */
  public function executeList()
  {
    if (!($this->project instanceof Project)) {
      throw new sfException('Error in the list component of the license module. $project must be an intance of Project');
    }
    
    $this->has_file = $this->project->hasFile();
    if ($this->has_file === true) {
      $this->files = $this->project->getFiles();
    }
  }
}