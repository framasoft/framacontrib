<?php

/**
 * file actions.
 *
 * @package    FramaCode
 * @subpackage file
 * @author     Simon Leblanc <contact@leblanc-simon.eu>
 * @version    SVN: $Id: actions.class.php 12479 2008-10-31 10:54:40Z fabien $
 */
class fileActions extends myActions
{
  /**
   * Show the details of the file
   *
   * @param   sfRequest   $request  A request object
   * @throws  sfException           If label isn't in the parameter, an exception is throw
   */
  public function executeShow(sfWebRequest $request)
  {
    $label = $request->getParameter('label', null);
    if ($label === null) {
      throw new sfException('File is required for show the detail');
    }
    
    $this->file = FilePeer::retrieveByLabel($label);
    $this->user = $this->getUser();
  }
}
