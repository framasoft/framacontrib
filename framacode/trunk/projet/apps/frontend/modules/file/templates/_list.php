<?php if ($has_file === true) { ?>
<div class="file">
  <h3><?php echo format_number_choice("[1]File|(1,+Inf]Files", array(), count($files)) ?></h3>
  <?php foreach ($files as $file) { ?>
  <div class="file_detail">
    <div class="filename">
      <a href="<?php echo url_for('file/show?label='.$file->getLabel()) ?>" title="<?php echo __("Show the page for the file %s", array('%s' => $file)) ?>"><?php echo $file ?></a>
    </div>
    <div class="download">
      <a href="<?php echo url_for('download/file?file='.$file->getLabel()) ?>" title="<?php echo __("Download the file '%s'", array('%s' => $file->getName())) ?>">
        <?php echo image_tag('file/download', array('alt' => 'download')) ?>
      </a>
    </div>
    <?php if ($file->hasNote() === true) { ?>
    <div class="note">
      <a href="<?php echo url_for('download/note?file='.$file->getLabel()) ?>" title="<?php echo __("Download the release note for the file '%s'", array('%s' => $file->getName())) ?>">
        <?php echo image_tag('file/release_note', array('alt' => 'release-note')) ?>
      </a>
    </div>
    <?php } ?>
    <?php if ($file->hasChangelog() === true) { ?>
    <div class="changelog">
      <a href="<?php echo url_for('download/changelog?file='.$file->getLabel()) ?>" title="<?php echo __("Download the changelog for the file '%s'", array('%s' => $file->getName())) ?>">
        <?php echo image_tag('file/changelog', array('alt' => 'changelog')) ?>
      </a>
    </div>
    <?php } ?>
  </div>
  <?php } ?>
</div>
<?php } ?>