<div class="file-details">
  <h2><?php echo $file ?></h2>
  <div class="date"><?php echo format_datetime($file->getCreateAt(), sfConfig::get('app_date_file', 'F'), $user->getCulture()) ?></div>
  <div class="size"><?php echo __("Size") ?> : <?php echo $file->getHumanSize($user->getCulture()) ?></div>
</div>