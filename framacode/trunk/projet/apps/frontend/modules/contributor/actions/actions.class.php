<?php

/**
 * contributor actions.
 *
 * @package    FramaCode
 * @subpackage contributor
 * @author     Simon Leblanc <contact@leblanc-simon.eu>
 * @version    SVN: $Id: actions.class.php 12479 2008-10-31 10:54:40Z fabien $
 */
class contributorActions extends myActions
{
  const HTML = 'html';
  const FOAF = 'foaf';
  
  /**
   * Show the list of contributors with them projects
   *
   * @param sfRequest $request A request object
   */
  public function executeIndex(sfWebRequest $request)
  {
    // Initialize the meta
    $this->meta_title = "List of the contributor";
    
    // Get the projects
    $criteria = new Criteria();
    
    // Add the necessary criteria
    Search::addCriteria($criteria, $request, $this->getUser());
    
    // Init the pagination
    $this->pager = new sfPropelPager('Contributor', sfConfig::get('app_pagination_contributor', 15));
    $this->pager->setCriteria($criteria);
    $this->pager->setPage(Search::$page);
    $this->pager->init();
    
    if ($this->pager->getNbResults() === 0) {
      return sfView::ERROR;
    } else {
      return sfView::SUCCESS;
    }
  }
  
  
  /**
   * Show the page of the contributor
   *
   * @param sfRequest $request A request object
   */
  public function executeShow(sfWebRequest $request)
  {
    $type   = $request->getParameter('type', contributorActions::HTML);
    $this->pseudo = $request->getParameter('pseudo', null);
    
    if ($type !== contributorActions::HTML && $type !== contributorActions::FOAF) {
      throw new sfException('The type of page must be contributorActions::HTML or contributorActions::FOAF');
    }
    
    if ($this->pseudo === null) {
      $this->meta_title = "Error : no contributor";
      $this->view_error = true;
      return sfView::ERROR;
    }
    
    $this->contributor = ContributorPeer::retrieveByPseudo($this->pseudo);
    if ($this->contributor === null) {
      $this->meta_title = "Error : no contributor";
      $this->view_error = true;
      return sfView::ERROR;
    }
    
    $this->meta_title = "Contributor : %s";
    $this->meta_title_args = array('%s' => $this->contributor->getName());
  }
}
