<?php

/**
 * contributor components.
 *
 * @package    FramaCode
 * @subpackage project
 * @author     Simon Leblanc <contact@leblanc-simon.eu>
 * @version    SVN: $Id: components.class.php 12479 2008-10-31 10:54:40Z fabien $
 */

class contributorComponents extends sfComponents
{
  /**
   * List the contributors associated with the project in the parameter
   *
   * @param   Project   $this->project    The project's object
   * @access  public
   */
  public function executeList()
  {
    if (!($this->project instanceof Project)) {
      throw new sfException('Error in the list component of the contributor module. $project must be an intance of Project');
    }
    
    $this->has_contributor = $this->project->hasContributor();
    if ($this->has_contributor === true) {
      $this->contributors = $this->project->getContributors();
    }
  }
  
  
  /**
   * List the projects associated with the contributor in the parameter
   *
   * @param   Contributor   $this->contributor    The contributor's object
   * @access  public
   */
  public function executeProjectsByContributor()
  {
    if (!($this->contributor instanceof Contributor)) {
      throw new sfException('Error in the projectsByContributor component of the contributor module. $contributor must be an intance of Contributor');
    }
    
    $this->criteria = new Criteria();
    
    Search::addCriteriaComponent($this->criteria, $this->getUser());
    
    $this->criteria->add(ContributorPeer::ID, $this->contributor->getId());
  }
  
  
  /**
   * Show the search engine
   *
   * @access  public
   * @todo    order contributor by either pseudo either first_name switch show_real_name
   */
  public function executeSearch()
  {
    $user = $this->getUser();
    
    $this->name         = $user->getAttribute('name', '', Search::NAMESPACE);
    $this->type_select  = $user->getAttribute('type', 0, Search::NAMESPACE);
    
    $criteria = new Criteria();
    $criteria->addAscendingOrderByColumn(ContributorTypeI18nPeer::NAME);
    $this->contributor_types = ContributorTypePeer::doSelectWithI18n($criteria);
  }
}