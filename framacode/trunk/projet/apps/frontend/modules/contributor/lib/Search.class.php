<?php
class Search extends AbstractSearch
{
  const NAMESPACE = 'search_contributor';
  
  /**
   * Add the criteria with the request wanted by the user
   *
   * @param   Criteria        $criteria   The criteria's object
   * @param   sfWebRequest    $request    The request's object
   * @param   myUser          $user       The user's object
   * @throws  SearchException             If the user send a bad value for an argument, a SearchException is throw
   * @access  public
   * @static
   */
  public static function addCriteria(Criteria $criteria, sfWebRequest $request, myUser $user)
  {
    // See only the visible project
    $criteria->addJoin(ProjectHasContributorPeer::PROJECT_ID, ProjectPeer::ID);
    $criteria->addJoin(ProjectHasContributorPeer::CONTRIBUTOR_ID, ContributorPeer::ID);
    $criteria->add(ProjectPeer::VISIBLE, true);
    
    // The type of contributor
    $type = (int) $request->getParameter('type', $user->getAttribute('type', 0, self::NAMESPACE));
    if ($type === 0 && $user->getAttribute('type', 0, self::NAMESPACE) !== 0) {
      self::saveCriteria($user, 'type', 0, self::NAMESPACE);
    } elseif ($type !== 0) {
      $criteria->add(ProjectHasContributorPeer::CONTRIBUTOR_TYPE_ID, $type);
      self::saveCriteria($user, 'type', $type, self::NAMESPACE);
    }
    
    // The name
    $name = $request->getParameter('name', $user->getAttribute('name', null, self::NAMESPACE));
    if ($name === '' && $user->getAttribute('name', '', self::NAMESPACE) !== '') {
      self::saveCriteria($user, 'name', '', self::NAMESPACE);
    } elseif ($name !== null && $name !== '') {
      $criterion = $criteria->getNewCriterion(ContributorPeer::PSEUDO, '%'.$name.'%', Criteria::LIKE);
      $criterion->addOr($criteria->getNewCriterion(ContributorPeer::FIRST_NAME, '%'.$name.'%', Criteria::LIKE));
      $criterion->addOr($criteria->getNewCriterion(ContributorPeer::LAST_NAME, '%'.$name.'%', Criteria::LIKE));
      $criteria->addAnd($criterion);
      self::saveCriteria($user, 'name', $name, self::NAMESPACE);
    }
    
    // Order by the name to show
    ContributorPeer::addOrder($criteria, 'asc');
    
    // The page
    self::$page = (int) $request->getParameter('page', $user->getAttribute('page', 0, self::NAMESPACE));
    self::saveCriteria($user, 'page', self::$page, self::NAMESPACE);
  }
  
  
  /**
   * Add the criteria for the component
   *
   * @param   Criteria        $criteria   The criteria's object
   * @param   myUser          $user       The user's object
   * @access  public
   * @static
   */
  public static function addCriteriaComponent(Criteria $criteria, myUser $user)
  {
    // See only the visible project
    $criteria->addJoin(ProjectHasContributorPeer::PROJECT_ID, ProjectPeer::ID);
    $criteria->addJoin(ProjectHasContributorPeer::CONTRIBUTOR_ID, ContributorPeer::ID);
    $criteria->add(ProjectPeer::VISIBLE, true);
    
    $criteria->addAscendingOrderByColumn(ProjectPeer::NAME);
  }
}