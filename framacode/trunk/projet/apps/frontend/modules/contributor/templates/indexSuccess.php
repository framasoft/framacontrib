<?php include_component('index', 'breadcrumb', array(myActions::LIST_CONTRIBUTORS)) ?>
<?php include_component('contributor', 'search'); ?>
<div class="list-contributor">
  <?php foreach ($pager->getResults() as $contributor) { ?>
  <div class="contributor">
    <div class="name"><a href="<?php echo url_for('contributor/show?pseudo='.$contributor->getPseudo()) ?>"><?php echo $contributor ?></a></div>
    <?php include_component('contributor', 'projectsByContributor', array('contributor' => $contributor)) ?>
  </div>
  <?php } ?>
</div>
<?php include_partial('global/pagination', array('pager' => $pager, 'link' => 'contributor/index?')) ?>