<?php include_component('index', 'breadcrumb', array(myActions::LIST_CONTRIBUTORS)) ?>
<?php include_component('contributor', 'search'); ?>
<div id="contributor-list-error">
  <?php echo __("There is no contributor with your criteria.") ?>
</div>