<?php include_component('index', 'breadcrumb', array(myActions::LIST_CONTRIBUTORS, $contributor)) ?>
<div class="contributor">
  <h2><?php echo $contributor ?></h2>
  <div class="info">
    <div class="email"><?php echo __("E-mail") ?> : <?php echo $contributor->getEmail() ?></div>
    <div class="website">
      <?php echo __("Website") ?> :
      <a href="<?php echo url_for($contributor->getWebsite()) ?>" title="<?php echo __("Visit the website of %s", array('%s' => $contributor)) ?>"><?php echo $contributor->getWebsite() ?></a>
    </div>
  </div>
  <div class="project">
    <h3><?php echo __("His projects") ?> :</h3>
    <?php include_component('contributor', 'projectsByContributor', array('contributor' => $contributor)) ?>
  </div>
</div>