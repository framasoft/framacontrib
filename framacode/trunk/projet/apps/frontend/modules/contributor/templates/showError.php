<?php include_component('index', 'breadcrumb', array(myActions::LIST_CONTRIBUTORS, 'last_link' => true)) ?>
<div id="contributor-error">
  <?php echo __("The contributor '%s' doesn't exist.", array('%s' => strip_tags($pseudo))) ?>
</div>