<?php if ($has_contributor === true) { ?>
<div class="contributor">
  <?php echo format_number_choice("[1]Contributor|(1,+Inf]Contributors", array(), count($contributors)) ?> :
  <?php
  $i = 0;
  foreach ($contributors as $contributor) {
    if ($i > 0) { echo ', '; }
  ?>
  <a href="<?php echo url_for('contributor/show?pseudo='.$contributor->getPseudo()) ?>" title="<?php echo __("Show the page for the contributor %s", array('%s' => $contributor)) ?>"><?php echo $contributor->getName() ?></a><?php
    $i++;
  }
  ?>
</div>
<?php } ?>