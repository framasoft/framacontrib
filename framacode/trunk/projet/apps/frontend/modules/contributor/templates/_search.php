<div class="search">
  <form action="<?php url_for('search/index') ?>" method="post">
    <div class="field">
      <label for="search_contributor_name"><?php echo __("Name of the contributor") ?></label>
      <input type="text" name="name" id="search_contributor_name" value="<?php echo $name ?>" />
    </div>
    <div class="field">
      <label for="search_contributor_type"><?php echo __("License") ?></label>
      <select name="type" id="search_contributor_type">
        <option value="0"><?php echo __("Select a type") ?></option>
        <?php foreach ($contributor_types as $contributor_type) { ?>
        <option value="<?php echo $contributor_type->getId() ?>"<?php if ($contributor_type->getId() == $type_select) { ?> selected="selected"<?php } ?>><?php echo $contributor_type ?></option>
        <?php } ?>
      </select>
    </div>
    <div class="submit"><input type="submit" name="submit" value="<?php echo __("Search") ?>" /></div>
  </form>
</div>