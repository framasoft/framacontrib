<?php include_component('index', 'breadcrumb', array(myActions::LIST_PROJECTS)) ?>
<?php include_component('project', 'search'); ?>
<div id="project-list">
  <?php foreach ($pager->getResults() as $project) { ?>
  <div class="project">
    <div class="name"><a href="<?php echo url_for('project/show?label='.$project->getLabel()) ?>"><?php echo $project ?></a></div>
    <div class="description"><?php echo $project->getDescription() ?></div>
    <div class="info">
      <?php include_component('project', 'license', array('project' => $project)) ?>
      <?php if ($project->hasWebsite() === true) { ?>
      <div class="website"><a href="<?php echo $project->getWebsite() ?>" title="<?php echo __("Go to the official website of %s", array('%s' => $project)) ?>"><?php echo __("Show the official website") ?></a>
      <?php } ?>
    </div>
  </div>
  <?php } ?>
</div>
<?php include_partial('global/pagination', array('pager' => $pager, 'link' => 'project/index?')) ?>