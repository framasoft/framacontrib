<?php include_component('index', 'breadcrumb', array(myActions::LIST_PROJECTS, $project)) ?>
<div id="project">
  <h2><?php echo $project ?></h2>
  <div class="description">
    <h3><?php echo __("Description") ?></h3>
    <?php echo $project->getDescription() ?>
  </div>
  <div class="info">
    <h3><?php echo __("Informations") ?></h3>
    
    <?php if ($project->hasWebsite() === true) { ?>
    <!-- The official website -->
    <div class="website"><?php echo __("Official website") ?> : <a href="<?php echo $project->getWebsite() ?>"><?php echo $project->getWebsite() ?></a></div>
    <?php } ?>
    
    <!-- The date of last update of the project -->
    <div class="update"><?php echo __("Last update") ?> : <?php echo format_datetime($project->getUpdateAt(), sfConfig::get('app_date_project', 'F'), $user->getCulture()) ?></div>
    
    <?php if ($project->hasProjectUrl() === true) { ?>
    <!-- The website of the devel's porject -->
    <div class="devel"><?php echo __("Development website") ?> : <a href="<?php echo $project->getProjectUrl() ?>" title="<?php echo __("Browse the %s's development website", array('%s' => $project)) ?>"><?php echo $project->getProjectUrl() ?></a></div>
    <?php } ?>
    <?php if ($project->hasVersonning() === true) { ?>
    <!-- The website of the versionning's porject -->
    <div class="versionning"><?php echo __("Browse source code") ?> : <a href="<?php echo $project->getVersionningUrl() ?>" title="<?php echo __("Browse the %s's source code", array('%s' => $project)) ?>"><?php echo $project->getVersionningUrl() ?></a></div>
    <?php } ?>
    
    <!-- The licenses -->
    <?php include_component('license', 'list', array('project' => $project)) ?>
    
    <!-- the contributors -->
    <?php include_component('contributor', 'list', array('project' => $project)) ?>
  </div>
  
  <!-- files list -->
  <?php include_component('file', 'list', array('project' => $project)) ?>
  
  <!-- documentations list -->
  <?php include_component('documentation', 'list', array('project' => $project)) ?>
</div>