<ul class="license-list">
  <?php foreach ($licenses as $license) { ?>
  <li><a href="<?php echo url_for('license/show?label='.$license->getLabel()) ?>"><?php echo $license ?></a></li>
  <?php } ?>
</ul>