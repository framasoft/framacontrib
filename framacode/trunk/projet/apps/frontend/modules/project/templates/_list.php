<?php foreach ($projects as $project) { ?>
<div class="project-list">
  <h3><?php echo $project ?></h3>
  <div class="description"><?php echo $project->getDescription() ?></div>
  <div class="link"><a href="<?php echo url_for('project/show?label='.$project->getLabel()) ?>" title="<?php echo __("View the details of the project '%s'", array('%s' => $project)) ?>"><?php echo __("Show this project") ?></a></div>
</div>
<?php } ?>