<div class="search">
  <form action="<?php url_for('search/index') ?>" method="post">
    <div class="field">
      <label for="search_project_name"><?php echo __("Name of the project") ?></label>
      <input type="text" name="name" id="search_project_name" value="<?php echo $name ?>" />
    </div>
    <div class="field">
      <label for="search_project_license"><?php echo __("License") ?></label>
      <select name="license" id="search_project_license">
        <option value="0"><?php echo __("Select a license") ?></option>
        <?php foreach ($licenses as $license) { ?>
        <option value="<?php echo $license->getId() ?>"<?php if ($license->getId() == $license_select) { ?> selected="selected"<?php } ?>><?php echo $license ?></option>
        <?php } ?>
      </select>
    </div>
    <div class="field">
      <label for="search_project_contributor"><?php echo __("Contributor") ?></label>
      <select name="contributor" id="search_project_contributor">
        <option value="0"><?php echo __("Select a contributor") ?></option>
        <?php foreach ($contributors as $contributor) { ?>
        <option value="<?php echo $contributor->getId() ?>"<?php if ($contributor->getId() == $contributor_select) { ?> selected="selected"<?php } ?>><?php echo $contributor ?></option>
        <?php } ?>
      </select>
    </div>
    <div class="submit"><input type="submit" name="submit" value="<?php echo __("Search") ?>" /></div>
  </form>
</div>