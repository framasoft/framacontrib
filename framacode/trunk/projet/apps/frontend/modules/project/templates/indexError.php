<?php include_component('index', 'breadcrumb', array(myActions::LIST_PROJECTS)) ?>
<?php include_component('project', 'search'); ?>
<div id="project-list-error">
  <?php echo __("There is no project with your criteria.") ?>
</div>