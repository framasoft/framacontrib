<?php include_component('index', 'breadcrumb', array(myActions::LIST_PROJECTS, 'last_link' => true)) ?>
<div id="project-error">
  <?php echo __("The project '%s' doesn't exist.", array('%s' => strip_tags($label))) ?>
</div>