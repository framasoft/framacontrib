<?php

/**
 * project components.
 *
 * @package    FramaCode
 * @subpackage project
 * @author     Simon Leblanc <contact@leblanc-simon.eu>
 * @version    SVN: $Id: components.class.php 12479 2008-10-31 10:54:40Z fabien $
 */

class projectComponents extends sfComponents
{
  /**
   * Show the search engine
   *
   * @access  public
   */
  public function executeSearch()
  {
    $user = $this->getUser();
    
    $this->name               = $user->getAttribute('name', '', Search::NAMESPACE);
    $this->license_select     = $user->getAttribute('license', 0, Search::NAMESPACE);
    $this->contributor_select = $user->getAttribute('contributor', 0, Search::NAMESPACE);
    
    $criteria = new Criteria();
    $criteria->addAscendingOrderByColumn(LicensePeer::NAME);
    $this->licenses = LicensePeer::doSelect($criteria);
    
    // TODO : order by either pseudo either first_name switch show_real_name
    $criteria = new Criteria();
    ContributorPeer::addOrder($criteria, 'asc');
    $this->contributors = ContributorPeer::doSelect($criteria);
  }
  
  
  /**
   * Show the list of the projects
   *
   * @param   Criteria    $this->criteria   The criteria's object
   * @throws  sfException                   If $this->criteria isn't an instance of Criteria, an exception is throw
   * @access  public
   */
  public function executeList()
  {
    if (isset($this->criteria) === false) {
      $this->criteria = new Criteria();
    } elseif (!($this->criteria instanceof Criteria)) {
      throw new sfException('Error in the list component of the project module. $criteria must be an intance of Criteria');
    }
    
    $this->projects = ProjectPeer::doSelectWithI18n($this->criteria);
  }
  
  
  /**
   * Show the licenses associated with the project
   *
   * @param   Project   $this->project    The project's object
   * @throws  sfException                 If $this->project isn't an instance of Project, an exception is throw
   * @access  public
   */
  public function executeLicense()
  {
    if (!($this->project instanceof Project)) {
      throw new sfException('Error in the license component of the project module. $project must be an intance of Project');
    }
    
    $this->licenses = $this->project->getLicenses();
  }
}