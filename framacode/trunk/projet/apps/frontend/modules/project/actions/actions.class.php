<?php

/**
 * project actions.
 *
 * @package    FramaCode
 * @subpackage project
 * @author     Simon Leblanc <contact@leblanc-simon.eu>
 * @version    SVN: $Id: actions.class.php 12479 2008-10-31 10:54:40Z fabien $
 */
class projectActions extends myActions
{
  /**
   * Show the list of the projects available
   *
   * @param sfRequest $request A request object
   */
  public function executeIndex(sfWebRequest $request)
  {
    // Initialize the meta
    $this->meta_title = "List of the project";
    
    // Get the projects
    $criteria = new Criteria();
    
    // Add the necessary criteria
    Search::addCriteria($criteria, $request, $this->getUser());
    
    // Init the pagination
    $this->pager = new sfPropelPager('Project', sfConfig::get('app_pagination_project', 15));
    $this->pager->setCriteria($criteria);
    $this->pager->setPage(Search::$page);
    // For the culture, define the peer method
    $this->pager->setPeerMethod('doSelectWithI18n');
    $this->pager->setPeerCountMethod('doCountWithI18n');
    $this->pager->init();
    
    if ($this->pager->getNbResults() === 0) {
      return sfView::ERROR;
    } else {
      return sfView::SUCCESS;
    }
  }
  
  
  /**
   * Show the project page
   *
   * @param sfRequest $request A request object
   */
  public function executeShow(sfWebRequest $request)
  {
    $this->label  = $request->getParameter('label', null);
    $this->user   = $this->getUser();
    
    if ($this->label === null) {
      $this->meta_title = "Error : no project";
      $this->view_error = true;
      return sfView::ERROR;
    }
    
    $this->project = ProjectPeer::retrieveByLabel($this->label);
    if ($this->project === null) {
      $this->meta_title = "Error : no project";
      $this->view_error = true;
      return sfView::ERROR;
    }
    
    if ($this->project->getVisible() === false) {
      $this->meta_title = "Error : no project";
      $this->view_error = true;
      return sfView::ERROR;
    }
    
    $this->meta_title = "Project : %s";
    $this->meta_title_args = array('%s' => $this->project->getName());
  }
}
