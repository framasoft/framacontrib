<?php
class Search extends AbstractSearch
{
  const NAMESPACE = 'search_project';
  
  /**
   * Add the criteria with the request wanted by the user
   *
   * @param   Criteria        $criteria   The criteria's object
   * @param   sfWebRequest    $request    The request's object
   * @param   myUser          $user       The user's object
   * @throws  SearchException             If the user send a bad value for an argument, a SearchException is throw
   * @access  public
   * @static
   */
  public static function addCriteria(Criteria $criteria, sfWebRequest $request, myUser $user)
  {
    // See only the visible project
    $criteria->add(ProjectPeer::VISIBLE, true);
    
    // The ordering
    $order = (string) $request->getParameter('order', $user->getAttribute('order', 'name', self::NAMESPACE));
    $direction = (string) $request->getParameter('direction', $user->getAttribute('direction', 'asc', self::NAMESPACE));
    
    if ($direction === 'asc') {
      $func_direction = 'addAscendingOrderByColumn';
    } elseif ($direction === 'desc') {
      $func_direction = 'addDescendingOrderByColumn';
    } else {
      throw new SearchException('Direction must be "asc" or "desc"');
    }
    
    if ($order === 'name') {
      $criteria->$func_direction(ProjectPeer::NAME);
    } elseif ($order === 'update') {
      $criteria->$func_direction(ProjectPeer::UPDATE_AT);
    } elseif ($order === 'create') {
      $criteria->$func_direction(ProjectPeer::CREATE_AT);
    } else {
      throw new SearchException('Direction must be "name" or "update" or "create"');
    }
    
    // The name
    $name = $request->getParameter('name', $user->getAttribute('name', null, self::NAMESPACE));
    if ($name === '' && $user->getAttribute('name', '', self::NAMESPACE) !== '') {
      self::saveCriteria($user, 'name', '', self::NAMESPACE);
    } elseif ($name !== null && $name !== '') {
      $criteria->add(ProjectPeer::NAME, '%'.$name.'%', Criteria::LIKE);
      self::saveCriteria($user, 'name', $name, self::NAMESPACE);
    }
    
    // License
    $license = $request->getParameter('license', $user->getAttribute('license', null, self::NAMESPACE));
    if ($license !== null && empty($license) === false) {
      if (is_numeric($license) === false) {
        throw new SearchException('The license must be an integer');
      }
      $criteria->addJoin(ProjectPeer::ID, ProjectHasLicensePeer::PROJECT_ID);
      $criteria->add(ProjectHasLicensePeer::LICENSE_ID, (int) $license);
      self::saveCriteria($user, 'license', (int) $license, self::NAMESPACE);
    } elseif ($license == 0) {
      self::saveCriteria($user, 'license', 0, self::NAMESPACE);
    }
    
    // Contributor
    $contributor = $request->getParameter('contributor', $user->getAttribute('contributor', null, self::NAMESPACE));
    if ($contributor !== null && empty($contributor) === false) {
      if (is_numeric($contributor) === false) {
        throw new SearchException('The contributor must be an integer');
      }
      $criteria->addJoin(ProjectPeer::ID, ProjectHasContributorPeer::PROJECT_ID);
      $criteria->add(ProjectHasContributorPeer::CONTRIBUTOR_ID, (int) $contributor);
      self::saveCriteria($user, 'contributor', (int) $contributor, self::NAMESPACE);
    } elseif ($contributor == 0) {
      self::saveCriteria($user, 'contributor', 0, self::NAMESPACE);
    }
    
    // The page
    self::$page = (int) $request->getParameter('page', $user->getAttribute('page', 0, self::NAMESPACE));
    self::saveCriteria($user, 'page', self::$page, self::NAMESPACE);
  }
}