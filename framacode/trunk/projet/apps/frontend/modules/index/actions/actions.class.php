<?php

/**
 * index actions.
 *
 * @package    FramaCode
 * @subpackage index
 * @author     Simon Leblanc <contact@leblanc-simon.eu>
 * @version    SVN: $Id: actions.class.php 12479 2008-10-31 10:54:40Z fabien $
 */
class indexActions extends myActions
{
 /**
  * Show the homepage
  *
  * @param sfRequest $request A request object
  */
  public function executeIndex(sfWebRequest $request)
  {
    $this->criteria = new Criteria();
    
    $this->criteria->add(ProjectPeer::VISIBLE, true);
    $this->criteria->addAscendingOrderByColumn(ProjectPeer::UPDATE_AT);
    $this->criteria->setLimit(sfConfig::get('app_pagination_index', 5));
    
    $this->article = ArticlePeer::retrieveByLabel(sfConfig::get('app_article_index'));
    
    $this->meta_title = "FramaCode";
  }
  
  
  /**
   * Show an article
   *
   * @param sfRequest $request A request object
   */
  public function executeShow(sfWebRequest $request)
  {
    $this->label = $request->getParameter('label', null);
    if ($this->label === null) {
      $this->meta_title = "Error : no article";
      $this->view_error = true;
      return sfView::ERROR;
    }
    
    $this->article = ArticlePeer::retrieveByLabel($this->label);
    if ($this->article === null) {
      $this->meta_title = "Error : no article";
      $this->view_error = true;
      return sfView::ERROR;
    }
    
    $this->meta_title = $this->article->getName();
  }
}
