<?php

/**
 * index components.
 *
 * @package    FramaCode
 * @subpackage index
 * @author     Simon Leblanc <contact@leblanc-simon.eu>
 * @version    SVN: $Id: components.class.php 12479 2008-10-31 10:54:40Z fabien $
 */

class indexComponents extends sfComponents
{
  /**
   * Show the breadcrumb
   *
   * @param   bool    $this->last_link        True if you want have link to the last link
   * @param   array   $this->getVarHolder()   Contain all link you want
   */
  public function executeBreadcrumb()
  {
    $vars = $this->getVarHolder()->getAll();
    array_unshift($vars, myActions::INDEX);
    
    // If last_link is defined, delete to $vars
    if (isset($vars['last_link']) === true) {
      unset($vars['last_link']);
    }
    
    $this->links = array();
    
    $nb_items = count($vars);
    for ($i = 0; $i < $nb_items; $i++) {
      if ($vars[$i] instanceof Contributor) {
        $this->links[$i]['link'] = 'contributor/show?pseudo='.$vars[$i]->getPseudo();
        $this->links[$i]['name'] = $vars[$i]->getName();
      }
      
      elseif ($vars[$i] instanceof Documentation) {
        $this->links[$i]['link'] = 'documentation/show?label='.$vars[$i]->getLabel();
        $this->links[$i]['name'] = $vars[$i]->getName();
      }
      
      elseif ($vars[$i] instanceof License) {
        $this->links[$i]['link'] = 'license/show?label='.$vars[$i]->getLabel();
        $this->links[$i]['name'] = $vars[$i]->getName();
      }
      
      elseif ($vars[$i] instanceof Project) {
        $this->links[$i]['link'] = 'project/show?label='.$vars[$i]->getLabel();
        $this->links[$i]['name'] = $vars[$i]->getName();
      }
      
      elseif ($vars[$i] instanceof File) {
        $this->links[$i]['link'] = 'file/show?label='.$vars[$i]->getLabel();
        $this->links[$i]['name'] = $vars[$i]->getName();
      }
      
      elseif ($vars[$i] instanceof DocumentationPage) {
        $this->links[$i]['link'] = 'documentation/page?label='.$vars[$i]->getLabel();
        $this->links[$i]['name'] = $vars[$i]->getName();
      }
      
      elseif ($vars[$i] === myActions::LIST_CONTRIBUTORS) {
        $this->links[$i]['link'] = 'contributor/index';
        $this->links[$i]['name'] = $this->getContext()->getI18N()->__("Contributors");
      }
      
      elseif ($vars[$i] === myActions::LIST_DOCUMENTATIONS) {
        $this->links[$i]['link'] = 'documentation/index';
        $this->links[$i]['name'] = $this->getContext()->getI18N()->__("Documentations");
      }
      
      elseif ($vars[$i] === myActions::LIST_LICENSES) {
        $this->links[$i]['link'] = 'license/index';
        $this->links[$i]['name'] = $this->getContext()->getI18N()->__("Licenses");
      }
      
      elseif ($vars[$i] === myActions::LIST_PROJECTS) {
        $this->links[$i]['link'] = 'project/index';
        $this->links[$i]['name'] = $this->getContext()->getI18N()->__("Projects");
      }
      
      elseif ($vars[$i] === myActions::INDEX) {
        $this->links[$i]['link'] = 'index/index';
        $this->links[$i]['name'] = $this->getContext()->getI18N()->__("Homepage");
      }
    }
    
    // the last link have no url
    if (isset($this->last_link) === false) {
      $this->links[count($this->links) - 1]['link'] = null;
    }
  }
  
  
  /**
   * Show an article
   *
   * @param   Article   $this->article            The article's object
   * @param   bool      $this->dont_show_title    True if you want don't show the title of the article
   * @throws  sfException                         If $this-article isn't an instance of Article
   * @access  public
   */
  public function executeArticle()
  {
    if (!($this->article instanceof Article)) {
      throw new sfException('Error in the article component of the index module. $article must be an intance of Article');
    }
    
    $this->dont_show_title = (isset($this->dont_show_title) === true && $this->dont_show_title === true);
  }
  
  
  /**
   * Show a RSS feed
   *
   * @param   string    $this->feed       The URL of the feed
   */
  public function executeRss()
  {
    
  }
}
