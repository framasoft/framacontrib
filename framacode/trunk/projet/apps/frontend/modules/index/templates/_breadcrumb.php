<ul id="breadcrumb">
  <?php foreach ($links as $link) { ?>
    <?php if ($link['link'] !== null) { ?>
  <li><a href="<?php echo url_for($link['link']) ?>"><?php echo $link['name'] ?></a></li>
    <?php } else { ?>
  <li><?php echo $link['name'] ?></li>
    <?php } ?>
  <?php } ?>
</ul>