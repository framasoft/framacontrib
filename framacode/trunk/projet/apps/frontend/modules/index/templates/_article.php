<?php if ($article !== null) { ?>
<div class="article">
  <?php if ($dont_show_title === false) { ?><h2><?php echo $article->getName() ?></h2><?php } ?>
  <div class="content"><?php echo $article->getContent() ?></div>
</div>
<?php } ?>