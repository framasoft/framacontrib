<?php

/**
 * download actions.
 *
 * @package    FramaCode
 * @subpackage download
 * @author     Simon Leblanc <contact@leblanc-simon.eu>
 * @version    SVN: $Id: actions.class.php 12479 2008-10-31 10:54:40Z fabien $
 */
class downloadActions extends myActions
{
  /**
   * The content to download
   * @var     string      The content to download
   * @access  protected
   */
  protected static $content = null;
  
  /**
   * The path of the file to download
   * @var     string      The path of the file to download
   * @access  protected
   */
  protected static $file_content = null;
  
  /**
   * The filename for the user (see it when he download the file)
   * @var     string      The filename of the file to download
   * @access  protected
   */
  protected static $filename = null;
  
  
  /**
   * Download the release note for a file
   *
   * @throws  sfException   If content or filename is null, an exception is throw
   * @throws  sfException   If file_content doesn't exist, an exception is throw
   */
  public function executeDownload()
  {
    // Check if essential parameter exist
    if ((self::$content === null && self::$file_content === null) || self::$filename === null) {
      throw new sfException('You can\'t execute this method in first ! Call another method before.');
    }
    
    // If we want download a real file, check if it exist
    if (self::$file_content !== null && file_exists(self::$file_content) === false) {
      throw new sfException('The file doesn\'t exist !');
    }
    
    if (self::$content === null) {
      $file_size = filesize(self::$file_content);
    } else {
      $file_size = strlen(self::$content);
    }
    
    // The header to send
    $response = $this->getResponse();
    $response->setContentType('application/force-download');
    $response->setHttpHeader('Content-disposition', 'attachment; filename='.self::$filename);
    $response->setHttpHeader('Content-Transfer-Encoding', 'application/octet-stream'."\n");
    $response->setHttpHeader('Content-Length', $file_size);
    $response->setHttpHeader('Pragma', 'no-cache');
    $response->setHttpHeader('Cache-Control', 'must-revalidate, post-check=0, pre-check=0, public');
    $response->setHttpHeader('Expires', '0');
    
    // The content to send
    if (self::$content === null) {
      $response->setContent(file_get_contents(self::$file_content));
    } else {
      $response->setContent(self::$content);
    }
    
    // Send all
    $response->sendHttpHeaders();
    $response->sendContent();
    
    return sfView::RENDER_NONE;
  }
  
  
  /**
   * Download a file of the project
   *
   * @param   sfRequest   $request  A request object
   * @throws  sfException           If file isn't in the parameter, an exception is throw
   * @throws  sfException           If the file doesn't exist, an exception is throw
   * @throws  sfException           If the file have no release note associated, an exception is throw
   */
  public function executeFile(sfWebRequest $request)
  {
    $label = $request->getParameter('file', null);
    if ($label === null) {
      throw new sfException('File is required for this download !');
    }
    
    $file = FilePeer::retrieveByLabel($label);
    if ($file === null) {
      throw new sfException('There are no file with this label.');
    }
    
    self::$filename     = $file->getName();
    self::$file_content = $file->getFilePath();
    
    $this->forward('download', 'download');
  }
  
  
  /**
   * Download the release note for a file
   *
   * @param   sfRequest   $request  A request object
   * @throws  sfException           If file isn't in the parameter, an exception is throw
   * @throws  sfException           If the file doesn't exist, an exception is throw
   * @throws  sfException           If the file have no release note associated, an exception is throw
   */
  public function executeNote(sfWebRequest $request)
  {
    $label = $request->getParameter('file', null);
    if ($label === null) {
      throw new sfException('File is required for this download !');
    }
    
    $file = FilePeer::retrieveByLabel($label);
    if ($file === null) {
      throw new sfException('There are no file with this label.');
    }
    
    if ($file->hasNote() === false) {
      throw new sfException('This file has no release note associated.');
    }
    
    self::$filename = 'release-note-'.$file->getLabel().'.txt';
    self::$content  = $file->getNote();
    
    $this->forward('download', 'download');
  }
  
  
  /**
   * Download the changelog for a file
   *
   * @param   sfRequest   $request  A request object
   * @throws  sfException           If file isn't in the parameter, an exception is throw
   * @throws  sfException           If the file doesn't exist, an exception is throw
   * @throws  sfException           If the file have no changelog associated, an exception is throw
   */
  public function executeChangelog(sfWebRequest $request)
  {
    $label = $request->getParameter('file', null);
    if ($label === null) {
      throw new sfException('File is required for this download !');
    }
    
    $file = FilePeer::retrieveByLabel($label);
    if ($file === null) {
      throw new sfException('There are no file with this label.');
    }
    
    if ($file->hasChangelog() === false) {
      throw new sfException('This file has no changelog associated.');
    }
    
    self::$filename = 'changelog-'.$file->getLabel().'.txt';
    self::$content  = $file->getChangelog();
    
    $this->forward('download', 'download');
  }
  
  
  /**
   * Download a documentation
   *
   * @param   sfRequest   $request  A request object
   * @throws  sfException           If label isn't in the parameter, an exception is throw
   * @throws  sfException           If the documentation doesn't exist, an exception is throw
   * @throws  sfException           If the documentation have no file, an exception is throw
   */
  public function executeDocumentation(sfWebRequest $request)
  {
    $label = $request->getParameter('label', null);
    if ($label === null) {
      throw new sfException('Documentation is required for this download !');
    }
    
    $documentation = DocumentationPeer::retrieveByLabel($label);
    if ($file === null) {
      throw new sfException('There are no documentation with this label.');
    }
    
    if ($documentation->hasFile() === false) {
      throw new sfException('This documentation has no file to download !');
    } else {
      self::$filename     = $documentation->getFilename();
      self::$file_content = $documentation->getFilePath();
    }
    
    $this->forward('download', 'download');
  }
}
