<?php if ($has_license === true) { ?>
<div class="license">
  <?php echo format_number_choice("[1]License|(1,+Inf]Licenses", array(), count($licenses)) ?> :
  <?php
  $i = 0;
  foreach ($licenses as $license) {
    if ($i > 0) { echo ', '; }
  ?>
  <a href="<?php echo url_for('license/show?label='.$license->getLabel()) ?>" title="<?php echo __("Show the page for the license %s", array('%s' => $license)) ?>"><?php echo $license->getName() ?></a><?php
    $i++;
  }
  ?>
</div>
<?php } ?>