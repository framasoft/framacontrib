<?php

/**
 * license components.
 *
 * @package    FramaCode
 * @subpackage project
 * @author     Simon Leblanc <contact@leblanc-simon.eu>
 * @version    SVN: $Id: components.class.php 12479 2008-10-31 10:54:40Z fabien $
 */

class licenseComponents extends sfComponents
{
  /**
   * List the licenses associated with the project in the parameter
   *
   * @param   Project   $this->project    The project's object
   * @access  public
   */
  public function executeList()
  {
    if (!($this->project instanceof Project)) {
      throw new sfException('Error in the list component of the license module. $project must be an intance of Project');
    }
    
    $this->has_license = $this->project->hasLicense();
    if ($this->has_license === true) {
      $this->licenses = $this->project->getLicenses();
    }
  }
}