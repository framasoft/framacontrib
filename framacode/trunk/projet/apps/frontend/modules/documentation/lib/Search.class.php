<?php
class Search extends AbstractSearch
{
  const NAMESPACE = 'search_documentation';
  
  /**
   * Add the criteria with the request wanted by the user
   *
   * @param   Criteria        $criteria   The criteria's object
   * @param   sfWebRequest    $request    The request's object
   * @param   myUser          $user       The user's object
   * @throws  SearchException             If the user send a bad value for an argument, a SearchException is throw
   * @access  public
   * @static
   */
  public static function addCriteria(Criteria $criteria, sfWebRequest $request, myUser $user)
  {
    // See only the visible project
    $criteria->addJoin(DocumentationPeer::PROJECT_ID, ProjectPeer::ID);
    $criteria->add(ProjectPeer::VISIBLE, true);
    
    // The page
    self::$page = (int) $request->getParameter('page', $user->getAttribute('page', 0, self::NAMESPACE));
    self::saveCriteria($user, 'page', self::$page, self::NAMESPACE);
  }
}