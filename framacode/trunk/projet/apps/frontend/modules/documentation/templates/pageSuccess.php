<?php include_component('index', 'breadcrumb', array($project, $documentation, $page)) ?>
<div class="documentation-page">
  <h2><?php echo $page ?></h2>
  <div class="content"><?php echo $page->getContent() ?></div>
</div>