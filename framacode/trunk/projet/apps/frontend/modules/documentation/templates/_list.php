<?php if ($has_documentation === true) { ?>
<div class="documentation">
  <?php if ($list_only === false) { ?>
  <h3><?php echo format_number_choice("[1]Documentation|(1,+Inf]Documentations", array(), count($documentations)) ?></h3>
  <?php } ?>
  <?php foreach ($documentations as $documentation) { ?>
  <div class="documentation_detail">
    <div class="name">
      <a href="<?php echo url_for('documentation/show?label='.$documentation->getLabel()) ?>" title="<?php echo __("Show the documentation %s", array('%s' => $documentation)) ?>"><?php echo $documentation ?></a>
    </div>
    <?php if ($list_only === false && $documentation->hasFile() === true) { ?>
    <div class="download">
      <a href="<?php echo url_for('download/documentation?label='.$documentation->getLabel()) ?>" title="<?php echo __("Download the documentation '%s'", array('%s' => $documentation)) ?>">
        <?php echo image_tag('file/download', array('alt' => 'download')) ?>
      </a>
    </div>
    <?php } ?>
  </div>
  <?php } ?>
</div>
<?php } ?>