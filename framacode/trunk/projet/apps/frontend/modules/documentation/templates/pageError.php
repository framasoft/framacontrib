<?php include_component('index', 'breadcrumb', array(myActions::LIST_DOCUMENTATIONS, 'last_link' => true)) ?>
<div id="documentation-page-list-error">
  <?php echo __("The documentation page '%s' doesn't exist.", array('%s' => strip_tags($label))) ?>
</div>