<?php include_component('index', 'breadcrumb', array(myActions::LIST_DOCUMENTATIONS, 'last_link' => true)) ?>
<div class="documentation-error">
  <?php echo __("The documentation '%s' doesn't exist.", array('%s' => strip_tags($label))) ?>
</div>