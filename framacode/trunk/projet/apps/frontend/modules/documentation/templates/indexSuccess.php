<?php include_component('index', 'breadcrumb', array(myActions::LIST_DOCUMENTATIONS)) ?>
<div class="documentation-list">
  <?php foreach ($pager->getResults() as $project) { ?>
  <div class="project">
    <h2><?php echo $project ?></h2>
    <?php include_component('documentation', 'list', array('project' => $project, 'list_only' => true)) ?>
  </div>
  <?php } ?>
</div>
<?php include_partial('global/pagination', array('pager' => $pager, 'link' => 'documentation/index?')) ?>