<?php include_component('index', 'breadcrumb', array(myActions::LIST_DOCUMENTATIONS)) ?>
<div id="documentation-list-error">
  <?php echo __("There is no documentation with your criteria.") ?>
</div>