<?php include_component('index', 'breadcrumb', array($project, $documentation)) ?>
<div class="documentation">
  <h2><?php echo $documentation ?></h2>
  <?php include_component('documentation', 'pages', array('documentation' => $documentation)) ?>
</div>