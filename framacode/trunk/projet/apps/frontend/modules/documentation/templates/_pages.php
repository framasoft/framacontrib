<?php if ($has_page === false) { ?>
<div class="documentation-page-error"><?php echo __("This documentation haven't page !") ?></div>
<?php } else { ?>
<div class="documentation-page">
  <ul>
  <?php foreach ($pages as $page) { ?>
    <li><a href="<?php echo url_for('documentation/page?label='.$page->getLabel()) ?>" title="<?php echo __("Show the page of documentation '%s'", array('%s' => $page)) ?>"><?php echo $page ?></a></li>
  <?php } ?>
  </ul>
</div>
<?php } ?>