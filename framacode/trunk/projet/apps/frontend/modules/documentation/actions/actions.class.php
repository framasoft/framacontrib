<?php

/**
 * documentation actions.
 *
 * @package    FramaCode
 * @subpackage documentation
 * @author     Simon Leblanc <contact@leblanc-simon.eu>
 * @version    SVN: $Id: actions.class.php 12479 2008-10-31 10:54:40Z fabien $
 */
class documentationActions extends myActions
{
  /**
   * Show the list of documentation by project
   *
   * @param sfRequest $request A request object
   */
  public function executeIndex(sfWebRequest $request)
  {
    $this->meta_title = "List of documentations by project";
    
    $criteria = new Criteria();
    
    // Add the necessary criteria
    Search::addCriteria($criteria, $request, $this->getUser());
    
    // Init the pagination
    $this->pager = new sfPropelPager('Project', sfConfig::get('app_pagination_documentation', 15));
    $this->pager->setCriteria($criteria);
    $this->pager->setPage(Search::$page);
    // For the culture, define the peer method
    $this->pager->setPeerMethod('doSelectWithI18n');
    $this->pager->setPeerCountMethod('doCountWithI18n');
    $this->pager->init();
    
    if ($this->pager->getNbResults() === 0) {
      return sfView::ERROR;
    } else {
      return sfView::SUCCESS;
    }
  }
  
  
  /**
   * Show a documentation
   *
   * @param sfRequest $request A request object
   */
  public function executeShow(sfWebRequest $request)
  {
    $this->label = $request->getParameter('label', null);
    if ($this->label === null) {
      $this->meta_title = "Error : no documentation";
      $this->view_error = true;
      return sfView::ERROR;
    }
    
    $this->documentation = DocumentationPeer::retrieveByLabel($this->label);
    if ($this->documentation === null) {
      $this->meta_title = "Error : no documentation";
      $this->view_error = true;
      return sfView::ERROR;
    }
    
    $this->project = $this->documentation->getProject();
    if ($this->project === null || $this->project->getVisible() === false) {
      $this->meta_title = "Error : no documentation";
      $this->view_error = true;
      return sfView::ERROR;
    }
    
    $this->meta_title = "Documentation : %s";
    $this->meta_title_args = array('%s' => $this->documentation->getName());
  }
  
  
  /**
   * Show a page of the documentation
   *
   * @param sfRequest $request A request object
   */
  public function executePage(sfWebRequest $request)
  {
    $this->label = $request->getParameter('label', null);
    if ($this->label === null) {
      $this->meta_title = "Error : no documentation page";
      $this->view_error = true;
      return sfView::ERROR;
    }
    
    $this->page = DocumentationPagePeer::retrieveByLabel($this->label);
    if ($this->page === null) {
      $this->meta_title = "Error : no documentation page";
      $this->view_error = true;
      return sfView::ERROR;
    }
    
    $this->documentation = $this->page->getDocumentation();
    if ($this->documentation === null) {
      $this->meta_title = "Error : no documentation page";
      $this->view_error = true;
      return sfView::ERROR;
    }
    
    $this->project = $this->documentation->getProject();
    if ($this->project === null || $this->project->getVisible() === false) {
      $this->meta_title = "Error : no documentation page";
      $this->view_error = true;
      return sfView::ERROR;
    }
    
    $this->meta_title = "Documentation : %s";
    $this->meta_title_args = array('%s' => $this->page->getName());
  }
}
