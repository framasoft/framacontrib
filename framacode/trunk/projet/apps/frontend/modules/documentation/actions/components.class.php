<?php

/**
 * documentation components.
 *
 * @package    FramaCode
 * @subpackage documentation
 * @author     Simon Leblanc <contact@leblanc-simon.eu>
 * @version    SVN: $Id: components.class.php 12479 2008-10-31 10:54:40Z fabien $
 */

class documentationComponents extends sfComponents
{
  /**
   * List the documentation associated with the project in the parameter
   *
   * @param   Project   $this->project    The project's object
   * @param   bool      $this->list_only  True, if you want show only the list (no download and no label)
   * @access  public
   */
  public function executeList()
  {
    if (!($this->project instanceof Project)) {
      throw new sfException('Error in the list component of the documentation module. $project must be an intance of Project');
    }
    
    if (isset($this->list_only) === false) {
      $this->list_only = false;
    }
    
    $this->has_documentation = $this->project->hasDocumentation();
    if ($this->has_documentation === true) {
      $this->documentations = $this->project->getDocumentations();
    }
  }
  
  
  /**
   * List all page associated with the documentation in the parameter
   *
   * @param   Documentation   $this->documentation    The documentation's object
   * @access  public
   */
  public function executePages()
  {
    if (!($this->documentation instanceof Documentation)) {
      throw new sfException('Error in the pages components of the documentation module. $documentation must be an instance of Documentation');
    }
    
    $this->has_page = $this->documentation->hasPage();
    if ($this->has_page === true) {
      $criteria = new Criteria();
      $criteria->addAscendingOrderByColumn(DocumentationPagePeer::POSITION);
      $this->pages = $this->documentation->getDocumentationPages($criteria);
    }
  }
}