<?php
class myActions extends sfActions
{
  const CATALOGUE             = 'messages';
  protected $meta_title       = null;
  protected $meta_title_args  = array();
  
  protected $view_error = false;
  
  const INDEX               = 'index';
  const LIST_PROJECTS       = 'list_projects';
  const LIST_CONTRIBUTORS   = 'list_contributors';
  const LIST_DOCUMENTATIONS = 'list_documentations';
  const LIST_LICENSES       = 'list_licenses';
  
  
  /**
   * Execute this method before the execute method
   */
  public function preExecute()
  {
    if (sfConfig::get('app_html5', false) === true) {
      $this->setLayout('layout_html5');
      $this->setTemplate('html5/'.$this->actionName.$this->getTemplate());
    }
  }
  
  /**
   * Execute this method after the execute method
   */
  public function postExecute()
  {
    if ($this->meta_title !== null) {
      $this->getResponse()->setTitle($this->getContext()->getI18N()->__($this->meta_title, $this->meta_title_args, self::CATALOGUE));
    }
    
    if ($this->view_error === true) {
      $this->getResponse()->setStatusCode(404);
    }
  }
}
?>