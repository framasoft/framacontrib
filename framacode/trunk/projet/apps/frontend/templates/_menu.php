<div id="menu">
  <ul>
    <li><a href="<?php echo url_for('project/index') ?>"><?php echo __("All projects") ?></a></li>
    <li><a href="<?php echo url_for('documentation/index') ?>"><?php echo __("All documentations") ?></a></li>
    <li><a href="<?php echo url_for('license/index') ?>"><?php echo __("List of licenses") ?></a></li>
    <li><a href="<?php echo url_for('contributor/index') ?>"><?php echo __("List of contributors") ?></a></li>
  </ul>
</div>