<!DOCTYPE html PUBLIC "-//W3C//DTD XHTML 1.0 Transitional//EN" "http://www.w3.org/TR/xhtml1/DTD/xhtml1-transitional.dtd">
<html xmlns="http://www.w3.org/1999/xhtml" xml:lang="en" lang="en">
  <head>
    <?php include_http_metas() ?>
    <?php include_metas() ?>
    <?php include_title() ?>
    <link rel="shortcut icon" href="<?php echo image_path('favicon.png') ?>" />
  </head>
  <body>
    <div id="header">
      <?php include_partial('global/header') ?>
      <?php include_partial('global/menu') ?>
    </div>
    <div id="container">
      <div id="right"></div>
      <div id="content"><?php echo $sf_content ?></div>
    </div>
    <div id="footer"></div>
  </body>
</html>
