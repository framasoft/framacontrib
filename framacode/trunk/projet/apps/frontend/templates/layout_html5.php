<!DOCTYPE html>
<html xml:lang="fr" lang="fr">
  <head>
    <?php include_http_metas() ?>
    <?php include_metas() ?>
    <?php include_title() ?>
    <link rel="shortcut icon" href="<?php echo image_path('favicon.png') ?>" />
  </head>
  <body>
    <header>
      <?php include_partial('global/header') ?>
      <?php include_partial('global/menu') ?>
    </header>
    <div id="container">
      <div id="right"></div>
      <section id="content"><?php echo $sf_content ?></section>
    </div>
    <footer></footer>
  </body>
</html>