<?php if ($pager->haveToPaginate() === true) { ?>
<div class="pagination">
  <div class="page"><?php echo link_to('&laquo;', $link.'page='.$pager->getFirstPage()) ?></div>
  <div class="page"><?php echo link_to('&lt;', $link.'page='.$pager->getPreviousPage()) ?></div>
  <?php
  $links = $pager->getLinks(sfConfig::get('app_pagination_max_links', 5));
  foreach ($links as $page) {
    if ($page === $pager->getPage()) {
  ?>
  <div class="page"><?php echo $page ?></div>
  <?php
    } else {
  ?>
  <div class="page"><?php echo link_to($page, $link.'page='.$pager->getPreviousPage()) ?></div>
  <?php
    }
  }
  ?>
  <div class="page"><?php echo link_to('&gt;', $link.'page='.$pager->getNextPage()) ?></div>
  <div class="page"><?php echo link_to('&raquo;', $link.'page='.$pager->getLastPage()) ?></div>
</div>
<?php } ?>