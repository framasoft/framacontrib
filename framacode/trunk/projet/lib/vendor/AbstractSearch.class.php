<?php
abstract class AbstractSearch
{
  public static $page = 0;
  
  public static function addCriteria(Criteria $criteria, sfWebRequest $request, myUser $user) {}
  
  /**
   * Save the parameter of the search in the user session
   *
   * @param   myUser  $user       The user's object
   * @param   string  $name       The name of the parameter
   * @param   mixed   $value      The value of the parameter
   * @param   string  $namespace  The value of the namespace
   * @access  protected
   * @static
   */
  protected static function saveCriteria(myUser $user, $name, $value, $namespace)
  {
    $user->setAttribute($name, $value, $namespace);
  }
}