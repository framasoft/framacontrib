<?php

/**
 * ProjectHasContributor form.
 *
 * @package    FramaCode
 * @subpackage form
 * @author     Simon Leblanc <contact@leblanc-simon.eu>
 * @version    SVN: $Id: sfPropelFormTemplate.php 10377 2008-07-21 07:10:32Z dwhittle $
 */
class ProjectHasContributorForm extends BaseProjectHasContributorForm
{
  public function configure()
  {
  }
}
