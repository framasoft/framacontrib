<?php

/**
 * ContributorType form base class.
 *
 * @package    FramaCode
 * @subpackage form
 * @author     Simon Leblanc <contact@leblanc-simon.eu>
 * @version    SVN: $Id: sfPropelFormGeneratedTemplate.php 16976 2009-04-04 12:47:44Z fabien $
 */
class BaseContributorTypeForm extends BaseFormPropel
{
  public function setup()
  {
    $this->setWidgets(array(
      'id'    => new sfWidgetFormInputHidden(),
      'label' => new sfWidgetFormInput(),
    ));

    $this->setValidators(array(
      'id'    => new sfValidatorPropelChoice(array('model' => 'ContributorType', 'column' => 'id', 'required' => false)),
      'label' => new sfValidatorString(array('max_length' => 255, 'required' => false)),
    ));

    $this->validatorSchema->setPostValidator(
      new sfValidatorPropelUnique(array('model' => 'ContributorType', 'column' => array('label')))
    );

    $this->widgetSchema->setNameFormat('contributor_type[%s]');

    $this->errorSchema = new sfValidatorErrorSchema($this->validatorSchema);

    parent::setup();
  }

  public function getModelName()
  {
    return 'ContributorType';
  }

  public function getI18nModelName()
  {
    return 'ContributorTypeI18n';
  }

  public function getI18nFormClass()
  {
    return 'ContributorTypeI18nForm';
  }

}
