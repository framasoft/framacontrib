<?php

/**
 * Project form base class.
 *
 * @package    FramaCode
 * @subpackage form
 * @author     Simon Leblanc <contact@leblanc-simon.eu>
 * @version    SVN: $Id: sfPropelFormGeneratedTemplate.php 16976 2009-04-04 12:47:44Z fabien $
 */
class BaseProjectForm extends BaseFormPropel
{
  public function setup()
  {
    $this->setWidgets(array(
      'id'                           => new sfWidgetFormInputHidden(),
      'name'                         => new sfWidgetFormInput(),
      'label'                        => new sfWidgetFormInput(),
      'create_at'                    => new sfWidgetFormDateTime(),
      'update_at'                    => new sfWidgetFormDateTime(),
      'versionning_url'              => new sfWidgetFormInput(),
      'project_url'                  => new sfWidgetFormInput(),
      'website'                      => new sfWidgetFormInput(),
      'visible'                      => new sfWidgetFormInputCheckbox(),
      'position'                     => new sfWidgetFormInput(),
      'project_has_contributor_list' => new sfWidgetFormPropelChoiceMany(array('model' => 'Contributor')),
      'project_has_license_list'     => new sfWidgetFormPropelChoiceMany(array('model' => 'License')),
    ));

    $this->setValidators(array(
      'id'                           => new sfValidatorPropelChoice(array('model' => 'Project', 'column' => 'id', 'required' => false)),
      'name'                         => new sfValidatorString(array('max_length' => 255, 'required' => false)),
      'label'                        => new sfValidatorString(array('max_length' => 255, 'required' => false)),
      'create_at'                    => new sfValidatorDateTime(array('required' => false)),
      'update_at'                    => new sfValidatorDateTime(array('required' => false)),
      'versionning_url'              => new sfValidatorString(array('max_length' => 255, 'required' => false)),
      'project_url'                  => new sfValidatorString(array('max_length' => 255, 'required' => false)),
      'website'                      => new sfValidatorString(array('max_length' => 255, 'required' => false)),
      'visible'                      => new sfValidatorBoolean(array('required' => false)),
      'position'                     => new sfValidatorInteger(array('required' => false)),
      'project_has_contributor_list' => new sfValidatorPropelChoiceMany(array('model' => 'Contributor', 'required' => false)),
      'project_has_license_list'     => new sfValidatorPropelChoiceMany(array('model' => 'License', 'required' => false)),
    ));

    $this->validatorSchema->setPostValidator(
      new sfValidatorAnd(array(
        new sfValidatorPropelUnique(array('model' => 'Project', 'column' => array('name'))),
        new sfValidatorPropelUnique(array('model' => 'Project', 'column' => array('label'))),
      ))
    );

    $this->widgetSchema->setNameFormat('project[%s]');

    $this->errorSchema = new sfValidatorErrorSchema($this->validatorSchema);

    parent::setup();
  }

  public function getModelName()
  {
    return 'Project';
  }

  public function getI18nModelName()
  {
    return 'ProjectI18n';
  }

  public function getI18nFormClass()
  {
    return 'ProjectI18nForm';
  }

  public function updateDefaultsFromObject()
  {
    parent::updateDefaultsFromObject();

    if (isset($this->widgetSchema['project_has_contributor_list']))
    {
      $values = array();
      foreach ($this->object->getProjectHasContributors() as $obj)
      {
        $values[] = $obj->getContributorId();
      }

      $this->setDefault('project_has_contributor_list', $values);
    }

    if (isset($this->widgetSchema['project_has_license_list']))
    {
      $values = array();
      foreach ($this->object->getProjectHasLicenses() as $obj)
      {
        $values[] = $obj->getLicenseId();
      }

      $this->setDefault('project_has_license_list', $values);
    }

  }

  protected function doSave($con = null)
  {
    parent::doSave($con);

    $this->saveProjectHasContributorList($con);
    $this->saveProjectHasLicenseList($con);
  }

  public function saveProjectHasContributorList($con = null)
  {
    if (!$this->isValid())
    {
      throw $this->getErrorSchema();
    }

    if (!isset($this->widgetSchema['project_has_contributor_list']))
    {
      // somebody has unset this widget
      return;
    }

    if (is_null($con))
    {
      $con = $this->getConnection();
    }

    $c = new Criteria();
    $c->add(ProjectHasContributorPeer::PROJECT_ID, $this->object->getPrimaryKey());
    ProjectHasContributorPeer::doDelete($c, $con);

    $values = $this->getValue('project_has_contributor_list');
    if (is_array($values))
    {
      foreach ($values as $value)
      {
        $obj = new ProjectHasContributor();
        $obj->setProjectId($this->object->getPrimaryKey());
        $obj->setContributorId($value);
        $obj->save();
      }
    }
  }

  public function saveProjectHasLicenseList($con = null)
  {
    if (!$this->isValid())
    {
      throw $this->getErrorSchema();
    }

    if (!isset($this->widgetSchema['project_has_license_list']))
    {
      // somebody has unset this widget
      return;
    }

    if (is_null($con))
    {
      $con = $this->getConnection();
    }

    $c = new Criteria();
    $c->add(ProjectHasLicensePeer::PROJECT_ID, $this->object->getPrimaryKey());
    ProjectHasLicensePeer::doDelete($c, $con);

    $values = $this->getValue('project_has_license_list');
    if (is_array($values))
    {
      foreach ($values as $value)
      {
        $obj = new ProjectHasLicense();
        $obj->setProjectId($this->object->getPrimaryKey());
        $obj->setLicenseId($value);
        $obj->save();
      }
    }
  }

}
