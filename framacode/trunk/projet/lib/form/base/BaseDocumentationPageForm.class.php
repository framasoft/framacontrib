<?php

/**
 * DocumentationPage form base class.
 *
 * @package    FramaCode
 * @subpackage form
 * @author     Simon Leblanc <contact@leblanc-simon.eu>
 * @version    SVN: $Id: sfPropelFormGeneratedTemplate.php 16976 2009-04-04 12:47:44Z fabien $
 */
class BaseDocumentationPageForm extends BaseFormPropel
{
  public function setup()
  {
    $this->setWidgets(array(
      'id'               => new sfWidgetFormInputHidden(),
      'documentation_id' => new sfWidgetFormPropelChoice(array('model' => 'Documentation', 'add_empty' => false)),
      'label'            => new sfWidgetFormInput(),
      'position'         => new sfWidgetFormInput(),
    ));

    $this->setValidators(array(
      'id'               => new sfValidatorPropelChoice(array('model' => 'DocumentationPage', 'column' => 'id', 'required' => false)),
      'documentation_id' => new sfValidatorPropelChoice(array('model' => 'Documentation', 'column' => 'id')),
      'label'            => new sfValidatorString(array('max_length' => 255, 'required' => false)),
      'position'         => new sfValidatorInteger(array('required' => false)),
    ));

    $this->validatorSchema->setPostValidator(
      new sfValidatorPropelUnique(array('model' => 'DocumentationPage', 'column' => array('label')))
    );

    $this->widgetSchema->setNameFormat('documentation_page[%s]');

    $this->errorSchema = new sfValidatorErrorSchema($this->validatorSchema);

    parent::setup();
  }

  public function getModelName()
  {
    return 'DocumentationPage';
  }

  public function getI18nModelName()
  {
    return 'DocumentationPageI18n';
  }

  public function getI18nFormClass()
  {
    return 'DocumentationPageI18nForm';
  }

}
