<?php

/**
 * ProjectHasContributor form base class.
 *
 * @package    FramaCode
 * @subpackage form
 * @author     Simon Leblanc <contact@leblanc-simon.eu>
 * @version    SVN: $Id: sfPropelFormGeneratedTemplate.php 16976 2009-04-04 12:47:44Z fabien $
 */
class BaseProjectHasContributorForm extends BaseFormPropel
{
  public function setup()
  {
    $this->setWidgets(array(
      'project_id'          => new sfWidgetFormInputHidden(),
      'contributor_id'      => new sfWidgetFormInputHidden(),
      'contributor_type_id' => new sfWidgetFormPropelChoice(array('model' => 'ContributorType', 'add_empty' => false)),
    ));

    $this->setValidators(array(
      'project_id'          => new sfValidatorPropelChoice(array('model' => 'Project', 'column' => 'id', 'required' => false)),
      'contributor_id'      => new sfValidatorPropelChoice(array('model' => 'Contributor', 'column' => 'id', 'required' => false)),
      'contributor_type_id' => new sfValidatorPropelChoice(array('model' => 'ContributorType', 'column' => 'id')),
    ));

    $this->widgetSchema->setNameFormat('project_has_contributor[%s]');

    $this->errorSchema = new sfValidatorErrorSchema($this->validatorSchema);

    parent::setup();
  }

  public function getModelName()
  {
    return 'ProjectHasContributor';
  }


}
