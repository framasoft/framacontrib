<?php

/**
 * Documentation form base class.
 *
 * @package    FramaCode
 * @subpackage form
 * @author     Simon Leblanc <contact@leblanc-simon.eu>
 * @version    SVN: $Id: sfPropelFormGeneratedTemplate.php 16976 2009-04-04 12:47:44Z fabien $
 */
class BaseDocumentationForm extends BaseFormPropel
{
  public function setup()
  {
    $this->setWidgets(array(
      'id'         => new sfWidgetFormInputHidden(),
      'project_id' => new sfWidgetFormPropelChoice(array('model' => 'Project', 'add_empty' => false)),
      'label'      => new sfWidgetFormInput(),
      'filename'   => new sfWidgetFormInput(),
      'position'   => new sfWidgetFormInput(),
    ));

    $this->setValidators(array(
      'id'         => new sfValidatorPropelChoice(array('model' => 'Documentation', 'column' => 'id', 'required' => false)),
      'project_id' => new sfValidatorPropelChoice(array('model' => 'Project', 'column' => 'id')),
      'label'      => new sfValidatorString(array('max_length' => 255, 'required' => false)),
      'filename'   => new sfValidatorString(array('max_length' => 255, 'required' => false)),
      'position'   => new sfValidatorInteger(array('required' => false)),
    ));

    $this->validatorSchema->setPostValidator(
      new sfValidatorPropelUnique(array('model' => 'Documentation', 'column' => array('label')))
    );

    $this->widgetSchema->setNameFormat('documentation[%s]');

    $this->errorSchema = new sfValidatorErrorSchema($this->validatorSchema);

    parent::setup();
  }

  public function getModelName()
  {
    return 'Documentation';
  }

  public function getI18nModelName()
  {
    return 'DocumentationI18n';
  }

  public function getI18nFormClass()
  {
    return 'DocumentationI18nForm';
  }

}
