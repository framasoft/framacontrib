<?php

/**
 * License form base class.
 *
 * @package    FramaCode
 * @subpackage form
 * @author     Simon Leblanc <contact@leblanc-simon.eu>
 * @version    SVN: $Id: sfPropelFormGeneratedTemplate.php 16976 2009-04-04 12:47:44Z fabien $
 */
class BaseLicenseForm extends BaseFormPropel
{
  public function setup()
  {
    $this->setWidgets(array(
      'id'                       => new sfWidgetFormInputHidden(),
      'name'                     => new sfWidgetFormInput(),
      'label'                    => new sfWidgetFormInput(),
      'url'                      => new sfWidgetFormInput(),
      'logo'                     => new sfWidgetFormInput(),
      'project_has_license_list' => new sfWidgetFormPropelChoiceMany(array('model' => 'Project')),
    ));

    $this->setValidators(array(
      'id'                       => new sfValidatorPropelChoice(array('model' => 'License', 'column' => 'id', 'required' => false)),
      'name'                     => new sfValidatorString(array('max_length' => 255, 'required' => false)),
      'label'                    => new sfValidatorString(array('max_length' => 255, 'required' => false)),
      'url'                      => new sfValidatorString(array('max_length' => 255, 'required' => false)),
      'logo'                     => new sfValidatorString(array('max_length' => 255, 'required' => false)),
      'project_has_license_list' => new sfValidatorPropelChoiceMany(array('model' => 'Project', 'required' => false)),
    ));

    $this->validatorSchema->setPostValidator(
      new sfValidatorAnd(array(
        new sfValidatorPropelUnique(array('model' => 'License', 'column' => array('name'))),
        new sfValidatorPropelUnique(array('model' => 'License', 'column' => array('label'))),
      ))
    );

    $this->widgetSchema->setNameFormat('license[%s]');

    $this->errorSchema = new sfValidatorErrorSchema($this->validatorSchema);

    parent::setup();
  }

  public function getModelName()
  {
    return 'License';
  }


  public function updateDefaultsFromObject()
  {
    parent::updateDefaultsFromObject();

    if (isset($this->widgetSchema['project_has_license_list']))
    {
      $values = array();
      foreach ($this->object->getProjectHasLicenses() as $obj)
      {
        $values[] = $obj->getProjectId();
      }

      $this->setDefault('project_has_license_list', $values);
    }

  }

  protected function doSave($con = null)
  {
    parent::doSave($con);

    $this->saveProjectHasLicenseList($con);
  }

  public function saveProjectHasLicenseList($con = null)
  {
    if (!$this->isValid())
    {
      throw $this->getErrorSchema();
    }

    if (!isset($this->widgetSchema['project_has_license_list']))
    {
      // somebody has unset this widget
      return;
    }

    if (is_null($con))
    {
      $con = $this->getConnection();
    }

    $c = new Criteria();
    $c->add(ProjectHasLicensePeer::LICENSE_ID, $this->object->getPrimaryKey());
    ProjectHasLicensePeer::doDelete($c, $con);

    $values = $this->getValue('project_has_license_list');
    if (is_array($values))
    {
      foreach ($values as $value)
      {
        $obj = new ProjectHasLicense();
        $obj->setLicenseId($this->object->getPrimaryKey());
        $obj->setProjectId($value);
        $obj->save();
      }
    }
  }

}
