<?php

/**
 * Contributor form base class.
 *
 * @package    FramaCode
 * @subpackage form
 * @author     Simon Leblanc <contact@leblanc-simon.eu>
 * @version    SVN: $Id: sfPropelFormGeneratedTemplate.php 16976 2009-04-04 12:47:44Z fabien $
 */
class BaseContributorForm extends BaseFormPropel
{
  public function setup()
  {
    $this->setWidgets(array(
      'id'                           => new sfWidgetFormInputHidden(),
      'pseudo'                       => new sfWidgetFormInput(),
      'email'                        => new sfWidgetFormInput(),
      'first_name'                   => new sfWidgetFormInput(),
      'last_name'                    => new sfWidgetFormInput(),
      'website'                      => new sfWidgetFormInput(),
      'show_real_name'               => new sfWidgetFormInputCheckbox(),
      'project_has_contributor_list' => new sfWidgetFormPropelChoiceMany(array('model' => 'Project')),
    ));

    $this->setValidators(array(
      'id'                           => new sfValidatorPropelChoice(array('model' => 'Contributor', 'column' => 'id', 'required' => false)),
      'pseudo'                       => new sfValidatorString(array('max_length' => 255, 'required' => false)),
      'email'                        => new sfValidatorString(array('max_length' => 255, 'required' => false)),
      'first_name'                   => new sfValidatorString(array('max_length' => 255, 'required' => false)),
      'last_name'                    => new sfValidatorString(array('max_length' => 255, 'required' => false)),
      'website'                      => new sfValidatorString(array('max_length' => 255, 'required' => false)),
      'show_real_name'               => new sfValidatorBoolean(array('required' => false)),
      'project_has_contributor_list' => new sfValidatorPropelChoiceMany(array('model' => 'Project', 'required' => false)),
    ));

    $this->validatorSchema->setPostValidator(
      new sfValidatorPropelUnique(array('model' => 'Contributor', 'column' => array('pseudo')))
    );

    $this->widgetSchema->setNameFormat('contributor[%s]');

    $this->errorSchema = new sfValidatorErrorSchema($this->validatorSchema);

    parent::setup();
  }

  public function getModelName()
  {
    return 'Contributor';
  }


  public function updateDefaultsFromObject()
  {
    parent::updateDefaultsFromObject();

    if (isset($this->widgetSchema['project_has_contributor_list']))
    {
      $values = array();
      foreach ($this->object->getProjectHasContributors() as $obj)
      {
        $values[] = $obj->getProjectId();
      }

      $this->setDefault('project_has_contributor_list', $values);
    }

  }

  protected function doSave($con = null)
  {
    parent::doSave($con);

    $this->saveProjectHasContributorList($con);
  }

  public function saveProjectHasContributorList($con = null)
  {
    if (!$this->isValid())
    {
      throw $this->getErrorSchema();
    }

    if (!isset($this->widgetSchema['project_has_contributor_list']))
    {
      // somebody has unset this widget
      return;
    }

    if (is_null($con))
    {
      $con = $this->getConnection();
    }

    $c = new Criteria();
    $c->add(ProjectHasContributorPeer::CONTRIBUTOR_ID, $this->object->getPrimaryKey());
    ProjectHasContributorPeer::doDelete($c, $con);

    $values = $this->getValue('project_has_contributor_list');
    if (is_array($values))
    {
      foreach ($values as $value)
      {
        $obj = new ProjectHasContributor();
        $obj->setContributorId($this->object->getPrimaryKey());
        $obj->setProjectId($value);
        $obj->save();
      }
    }
  }

}
