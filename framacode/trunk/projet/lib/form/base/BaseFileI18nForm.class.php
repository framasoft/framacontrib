<?php

/**
 * FileI18n form base class.
 *
 * @package    FramaCode
 * @subpackage form
 * @author     Simon Leblanc <contact@leblanc-simon.eu>
 * @version    SVN: $Id: sfPropelFormGeneratedTemplate.php 16976 2009-04-04 12:47:44Z fabien $
 */
class BaseFileI18nForm extends BaseFormPropel
{
  public function setup()
  {
    $this->setWidgets(array(
      'id'        => new sfWidgetFormInputHidden(),
      'culture'   => new sfWidgetFormInputHidden(),
      'note'      => new sfWidgetFormTextarea(),
      'changelog' => new sfWidgetFormTextarea(),
    ));

    $this->setValidators(array(
      'id'        => new sfValidatorPropelChoice(array('model' => 'File', 'column' => 'id', 'required' => false)),
      'culture'   => new sfValidatorPropelChoice(array('model' => 'FileI18n', 'column' => 'culture', 'required' => false)),
      'note'      => new sfValidatorString(array('required' => false)),
      'changelog' => new sfValidatorString(array('required' => false)),
    ));

    $this->widgetSchema->setNameFormat('file_i18n[%s]');

    $this->errorSchema = new sfValidatorErrorSchema($this->validatorSchema);

    parent::setup();
  }

  public function getModelName()
  {
    return 'FileI18n';
  }


}
