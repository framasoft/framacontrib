<?php

/**
 * File form base class.
 *
 * @package    FramaCode
 * @subpackage form
 * @author     Simon Leblanc <contact@leblanc-simon.eu>
 * @version    SVN: $Id: sfPropelFormGeneratedTemplate.php 16976 2009-04-04 12:47:44Z fabien $
 */
class BaseFileForm extends BaseFormPropel
{
  public function setup()
  {
    $this->setWidgets(array(
      'id'          => new sfWidgetFormInputHidden(),
      'project_id'  => new sfWidgetFormPropelChoice(array('model' => 'Project', 'add_empty' => false)),
      'name'        => new sfWidgetFormInput(),
      'label'       => new sfWidgetFormInput(),
      'size'        => new sfWidgetFormInput(),
      'create_at'   => new sfWidgetFormDateTime(),
      'nb_download' => new sfWidgetFormInput(),
      'version'     => new sfWidgetFormInput(),
    ));

    $this->setValidators(array(
      'id'          => new sfValidatorPropelChoice(array('model' => 'File', 'column' => 'id', 'required' => false)),
      'project_id'  => new sfValidatorPropelChoice(array('model' => 'Project', 'column' => 'id')),
      'name'        => new sfValidatorString(array('max_length' => 255, 'required' => false)),
      'label'       => new sfValidatorString(array('max_length' => 255, 'required' => false)),
      'size'        => new sfValidatorNumber(array('required' => false)),
      'create_at'   => new sfValidatorDateTime(array('required' => false)),
      'nb_download' => new sfValidatorInteger(array('required' => false)),
      'version'     => new sfValidatorString(array('max_length' => 20, 'required' => false)),
    ));

    $this->validatorSchema->setPostValidator(
      new sfValidatorAnd(array(
        new sfValidatorPropelUnique(array('model' => 'File', 'column' => array('name'))),
        new sfValidatorPropelUnique(array('model' => 'File', 'column' => array('label'))),
      ))
    );

    $this->widgetSchema->setNameFormat('file[%s]');

    $this->errorSchema = new sfValidatorErrorSchema($this->validatorSchema);

    parent::setup();
  }

  public function getModelName()
  {
    return 'File';
  }

  public function getI18nModelName()
  {
    return 'FileI18n';
  }

  public function getI18nFormClass()
  {
    return 'FileI18nForm';
  }

}
