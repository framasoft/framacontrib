<?php

/**
 * DocumentationPageI18n form.
 *
 * @package    FramaCode
 * @subpackage form
 * @author     Simon Leblanc <contact@leblanc-simon.eu>
 * @version    SVN: $Id: sfPropelFormTemplate.php 10377 2008-07-21 07:10:32Z dwhittle $
 */
class DocumentationPageI18nForm extends BaseDocumentationPageI18nForm
{
  public function configure()
  {
  }
}
