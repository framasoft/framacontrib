<?php

class License extends BaseLicense
{
  /**
   * Overload the __toString method
   *
   * @return  string    The name of the license
   * @access  public
   */
  public function __toString()
  {
    return $this->getName();
  }
}
