<?php

class Documentation extends BaseDocumentation
{
  /**
   * Overload the __toString method to return the name of the documentation
   *
   * @return  string      The name of the documentation
   * @access  public
   */
  public function __toString()
  {
    return $this->getName();
  }
  
  
  /**
   * Check if documentation has a file to download
   *
   * @return  bool    True if the documentation has a file to download, False else
   * @access  public
   */
  public function hasFile()
  {
    return !empty($this->filename);
  }
  
  
  /**
   * Get the path of the file
   *
   * @return  string        The path of the file (or null if there is no file)
   * @throws  sfException   If the folder of the file isn't defined, an exception is throw
   * @throws  sfException   If the file doesn't exist, an exception is throw
   * @access  public
   */
  public function getFilePath()
  {
    if ($this->hasFile() === false) {
      return null;
    }
    
    $folder = sfConfig::get('app_folders_documentation', null);
    if ($folder === null) {
      throw new sfException('The folder for documentation isn\'t defined !');
    }
    
    $file = $folder.DIRECTORY_SEPARATOR.$this->getProjectLabel().DIRECTORY_SEPARATOR.$this->getFilename();
    if (file_exists($file) === false) {
      throw new sfException('The file "'.$this->getFilename().'" doesn\'t exist !');
    }
    
    return $file;
  }
  
  
  /**
   * Return the label of the project's documentation
   *
   * @return  string      The label of the project associated with this documentation
   * @throws  sfException If the documentation isn't associated with a project, an exception is throw
   * @access  protected
   */
  protected function getProjectLabel()
  {
    $project = $this->getProject();
    if ($project === null) {
      throw new sfException('Documentation must be associated with one project.');
    }
    
    return $project->getLabel();
  }
  
  
  /**
   * Check if documentation has one or more pages
   *
   *@param    Criteria  $criteria   A criteria object for restrict the search
   * @return  bool                  True if the documentation has at least a page, False else
   * @access  public
   */
  public function hasPage(Criteria $criteria = null)
  {
    return (bool) $this->countDocumentationPages($criteria);
  }


  /**
   * Fetch the i18n object for this object culture.
   *
   * @param   string          $culture    The culture to set
   * @return  mixed                       A i18n object
   * @throws  PropelException             Any exceptions caught during processing will be rethrown wrapped into a PropelException.
   * @link    http://snippets.symfony-project.org/snippet/237 -- modified for Symfony 1.2
   */
  public function getCurrentDocumentationI18n($culture = null)
  {
    if (is_null($culture)) {
      $culture = is_null($this->culture) ? sfPropel::getDefaultCulture() : $this->culture;
    }
 
    if (!isset($this->current_i18n[$culture]))
    {
      $obj = DocumentationI18nPeer::retrieveByPK($this->getId(), $culture);
      if ($obj !== null) { // Test if there is a translation for current culture
        $this->setDocumentationI18nForCulture($obj, $culture);
      } else { // Create a translation for this culture
        $new_i18n = new DocumentationI18n();
 
        $default_culture = sfConfig::get('sf_default_culture');
 
        // We try to fetch the default culture translation to initialise the new culture.
        if (!isset($this->current_i18n[$default_culture])) {
          $obj = DocumentationI18nPeer::retrieveByPK($this->getId(), $default_culture);
          if ($obj !== null) { // Test if there is a translation for current culture
            $this->setDocumentationI18nForCulture($obj, $default_culture);
          }
        } else {
          $obj = $this->current_i18n[$default_culture];
        }
 
        if ($obj !== null) {
          $obj->copyInto($new_i18n);
        }
 
        $new_i18n->setId($this->getId());
        $new_i18n->setCulture($culture);
 
        $this->setDocumentationI18nForCulture($new_i18n, $culture);
      }
    }
 
    return $this->current_i18n[$culture];
  }


  /**
   * Stores the object in the database while setting default culture if necessary.
   *
   * If the object is new, it inserts it; otherwise an update is performed.
   * All related objects are also updated in this method.
   *
   * @param      Connection      $con The database connection
   * @return     int             The number of rows affected by this insert/update and any referring fk objects' save() operations.
   * @throws     PropelException Any exceptions caught during processing will be rethrown wrapped into a PropelException.
   * @see        save()
   * @link       http://snippets.symfony-project.org/snippet/237 -- modified for Symfony 1.2
   */
  protected function doSave(PropelPDO $con)
  {
    $default_culture = sfConfig::get('sf_default_culture');
    $current_culture = is_null($this->culture) ? sfPropel::getDefaultCulture() : $this->culture;
    $obj = null;
 
    // We try to fetch the default culture translation to initialise the new culture.
    if (!isset($this->current_i18n[$default_culture])) {
      $obj = DocumentationI18nPeer::retrieveByPK($this->getId(), $default_culture, $con);
      if ($obj !== null) { // Test if there is a translation for current culture
        $this->setDocumentationI18nForCulture($obj, $default_culture);
      }
    } else {
      $obj = $this->current_i18n[$default_culture];
    }
 
    if($obj === null && isset($this->current_i18n[$current_culture])) {
      $new_i18n = new DocumentationI18n();
      $this->current_i18n[$current_culture]->copyInto($new_i18n);
 
      $new_i18n->setId($this->getId());
      $new_i18n->setCulture($default_culture);
 
      $this->setDocumentationI18nForCulture($new_i18n, $default_culture);
    }
 
    return parent::doSave($con);
  }

}