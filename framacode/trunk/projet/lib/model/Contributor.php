<?php

class Contributor extends BaseContributor
{
  /**
   * Overload the __toString method
   *
   * @return  string    The name of the contributor
   * @access  public
   */
  public function __toString()
  {
    return $this->getName();
  }
  
  /**
   * Return the name to show switch the value of show_real_name
   *
   * @return  string      The value of the name to show
   * @access  public
   */
  public function getName()
  {
    if ($this->getShowRealName() === false || (empty($this->first_name) === true && empty($this->last_name) === true)) {
      return $this->getPseudo();
    } else {
      $name = $this->getFirstName();
      if (empty($name) === false) {
        $name .= ' ';
      }
      $name .= $this->getLastName();
      
      return $name;
    }
  }
}
