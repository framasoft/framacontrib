<?php

class File extends BaseFile
{
  /**
   * Overload the toString function
   *
   * @return  string    The name of the file
   * @access  public
   */
  public function __toString()
  {
    return $this->name;
  }
  
  
  /**
   * Return the size in human readable
   *
   * @param   string  $culture    The culture of the user
   * @return  string              The size in human readable
   * @access  public
   * @see     http://www.php.net/manual/en/function.filesize.php
   */
  public function getHumanSize($culture)
  {
    $size = $this->size;
    
    if ($culture === 'fr') {
      $units = explode(' ','o Kio Mio Gio Tio Pio');
    } else {
      $units = explode(' ','B KiB MiB GiB TiB PiB');
    }
    
    $mod = 1024;

    for ($i = 0; $size > $mod; $i++) {
      $size = $size / $mod;
    }
    
    return round($size, 2).$units[$i];
  }
  
  
  /**
   * Overload the save method for initialize the label
   *
   * @param   PropelPDO   $con      The propelpdo's object
   * @return  bool                  True if the object is save, false else
   * @access  public
   */
  public function save(PropelPDO $con = null)
  {
    $this->label = $this->name.'-'.$this->version;
    $this->label = str_replace(array('.', ' ', ), '', $this->label);
    
    return parent::save($con);
  }
  
  
  /**
   * Check if there is one or more note associated to the file
   *
   * @return  bool    True if there is at least one note associated to the file
   * @access  public
   */
  public function hasNote()
  {
    $criteria = new Criteria();
    $criterion = $criteria->getNewCriterion(FileI18nPeer::NOTE, null, Criteria::ISNOTNULL);
    $criterion->addAnd($criteria->getNewCriterion(FileI18nPeer::NOTE, '', Criteria::NOT_EQUAL));
    $criteria->addAnd($criterion);
    $criteria->add(FileI18nPeer::ID, $this->id);
    
    return (bool) FilePeer::doCountWithI18n($criteria);
  }
  
  
  /**
   * Check if there is one or more note associated to the file
   *
   * @return  bool    True if there is at least one note associated to the file
   * @access  public
   */
  public function hasChangelog()
  {
    $criteria = new Criteria();
    $criterion = $criteria->getNewCriterion(FileI18nPeer::CHANGELOG, null, Criteria::ISNOTNULL);
    $criterion->addAnd($criteria->getNewCriterion(FileI18nPeer::CHANGELOG, '', Criteria::NOT_EQUAL));
    $criteria->addAnd($criterion);
    $criteria->add(FileI18nPeer::ID, $this->id);
    
    return (bool) FilePeer::doCountWithI18n($criteria);
  }
  
  
  /**
   * Return the filename with path of the file
   *
   * @return  string        The path with filename for the file
   * @throws  sfException   If the folder of the file isn't defined, an exception is throw
   * @throws  sfException   If the file doesn't exist, an exception is throw
   * @access  public
   */
  public function getFilePath()
  {
    $folder = sfConfig::get('app_folders_file', null);
    if ($folder === null) {
      throw new sfException('The folder for file isn\'t defined !');
    }
    
    $file = $folder.DIRECTORY_SEPARATOR.$this->getProjectLabel().DIRECTORY_SEPARATOR.$this->getName();
    if (file_exists($file) === false) {
      throw new sfException('The file "'.$this->getName().'" doesn\'t exist !');
    }
    
    return $file;
  }
  
  
  /**
   * Return the label of the project's file
   *
   * @return  string      The label of the project associated with this file
   * @throws  sfException If the file isn't associated with a project, an exception is throw
   * @access  protected
   */
  protected function getProjectLabel()
  {
    $project = $this->getProject();
    if ($project === null) {
      throw new sfException('File must be associated with one project.');
    }
    
    return $project->getLabel();
  }


  /**
   * Fetch the i18n object for this object culture.
   *
   * @param   string          $culture    The culture to set
   * @return  mixed                       A i18n object
   * @throws  PropelException             Any exceptions caught during processing will be rethrown wrapped into a PropelException.
   * @link    http://snippets.symfony-project.org/snippet/237 -- modified for Symfony 1.2
   */
  public function getCurrentFileI18n($culture = null)
  {
    if (is_null($culture)) {
      $culture = is_null($this->culture) ? sfPropel::getDefaultCulture() : $this->culture;
    }
 
    if (!isset($this->current_i18n[$culture]))
    {
      $obj = FileI18nPeer::retrieveByPK($this->getId(), $culture);
      if ($obj !== null) { // Test if there is a translation for current culture
        $this->setFileI18nForCulture($obj, $culture);
      } else { // Create a translation for this culture
        $new_i18n = new FileI18n();
 
        $default_culture = sfConfig::get('sf_default_culture');
 
        // We try to fetch the default culture translation to initialise the new culture.
        if (!isset($this->current_i18n[$default_culture])) {
          $obj = FileI18nPeer::retrieveByPK($this->getId(), $default_culture);
          if ($obj !== null) { // Test if there is a translation for current culture
            $this->setFileI18nForCulture($obj, $default_culture);
          }
        } else {
          $obj = $this->current_i18n[$default_culture];
        }
 
        if ($obj !== null) {
          $obj->copyInto($new_i18n);
        }
 
        $new_i18n->setId($this->getId());
        $new_i18n->setCulture($culture);
 
        $this->setFileI18nForCulture($new_i18n, $culture);
      }
    }
 
    return $this->current_i18n[$culture];
  }


  /**
   * Stores the object in the database while setting default culture if necessary.
   *
   * If the object is new, it inserts it; otherwise an update is performed.
   * All related objects are also updated in this method.
   *
   * @param      Connection      $con The database connection
   * @return     int             The number of rows affected by this insert/update and any referring fk objects' save() operations.
   * @throws     PropelException Any exceptions caught during processing will be rethrown wrapped into a PropelException.
   * @see        save()
   * @link       http://snippets.symfony-project.org/snippet/237 -- modified for Symfony 1.2
   */
  protected function doSave(PropelPDO $con)
  {
    $default_culture = sfConfig::get('sf_default_culture');
    $current_culture = is_null($this->culture) ? sfPropel::getDefaultCulture() : $this->culture;
    $obj = null;
 
    // We try to fetch the default culture translation to initialise the new culture.
    if (!isset($this->current_i18n[$default_culture])) {
      $obj = FileI18nPeer::retrieveByPK($this->getId(), $default_culture, $con);
      if ($obj !== null) { // Test if there is a translation for current culture
        $this->setFileI18nForCulture($obj, $default_culture);
      }
    } else {
      $obj = $this->current_i18n[$default_culture];
    }
 
    if($obj === null && isset($this->current_i18n[$current_culture])) {
      $new_i18n = new FileI18n();
      $this->current_i18n[$current_culture]->copyInto($new_i18n);
 
      $new_i18n->setId($this->getId());
      $new_i18n->setCulture($default_culture);
 
      $this->setFileI18nForCulture($new_i18n, $default_culture);
    }
 
    return parent::doSave($con);
  }

}