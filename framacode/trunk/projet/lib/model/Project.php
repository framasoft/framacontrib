<?php

class Project extends BaseProject
{
  /**
   * Overload the __toString method
   *
   * @return  string    The name of the project
   * @access  public
   */
  public function __toString()
  {
    return $this->name;
  }
  
  
  /**
   * Check if the project have an official website
   *
   * @return  bool    True if the website exist, false else
   * @access  public
   */
  public function hasWebsite()
  {
    return !empty($this->website);
  }
  
  
  /**
   * Check if the project have a development website
   *
   * @return  bool    True if the website exist, false else
   * @access  public
   */
  public function hasProjectUrl()
  {
    return !empty($this->project_url);
  }
  
  
  /**
   * Check if the project have a versionning website
   *
   * @return  bool    True if the website exist, false else
   * @access  public
   */
  public function hasVersonning()
  {
    return !empty($this->versionning_url);
  }
  
  
  /**
   * Check if the project have one or more licenses
   *
   * @return  bool    True if the project have one license at least, false else
   * @access  public
   */
  public function hasLicense()
  {
    $criteria = new Criteria();
    
    $criteria->addJoin(ProjectPeer::ID, ProjectHasLicensePeer::PROJECT_ID);
    $criteria->addJoin(ProjectHasLicensePeer::LICENSE_ID, LicensePeer::ID);
    $criteria->add(ProjectPeer::ID, $this->id);
    
    return (bool) LicensePeer::doCount($criteria);
  }
  
  
  /**
   * Return the licenses associated with the project
   *
   * @param   Criteria    $criteria     The criteria's object
   * @param   PropelPDO   $con          The connection's object
   * @return  array                     An array of license object associated with project
   * @access  public
   */
  public function getLicenses(Criteria $criteria = null, PropelPDO $con = null)
  {
    if ($criteria === null) {
      $criteria = new Criteria();
    }
    
    $criteria->addJoin(ProjectPeer::ID, ProjectHasLicensePeer::PROJECT_ID);
    $criteria->addJoin(ProjectHasLicensePeer::LICENSE_ID, LicensePeer::ID);
    $criteria->add(ProjectPeer::ID, $this->id);
    
    return LicensePeer::doSelect($criteria, $con);
  }
  
  
  /**
   * Check if the project have one or more contributors
   *
   * @return  bool    True if the project have one contributor at least, false else
   * @access  public
   */
  public function hasContributor()
  {
    $criteria = new Criteria();
    
    $criteria->addJoin(ProjectPeer::ID, ProjectHasContributorPeer::PROJECT_ID);
    $criteria->addJoin(ProjectHasContributorPeer::CONTRIBUTOR_ID, ContributorPeer::ID);
    $criteria->add(ProjectPeer::ID, $this->id);
    
    return (bool) ContributorPeer::doCount($criteria);
  }
  
  
  /**
   * Return the contributors associated with the project
   *
   * @param   Criteria    $criteria     The criteria's object
   * @param   PropelPDO   $con          The connection's object
   * @return  array                     An array of contributor object associated with project
   * @access  public
   */
  public function getContributors(Criteria $criteria = null, PropelPDO $con = null)
  {
    if ($criteria === null) {
      $criteria = new Criteria();
    }
    
    $criteria->addJoin(ProjectPeer::ID, ProjectHasContributorPeer::PROJECT_ID);
    $criteria->addJoin(ProjectHasContributorPeer::CONTRIBUTOR_ID, ContributorPeer::ID);
    $criteria->add(ProjectPeer::ID, $this->id);
    
    return ContributorPeer::doSelect($criteria, $con);
  }
  
  
  /**
   * Check if the project have one or more files
   *
   * @return  bool    True if the project have one file at least, false else
   * @access  public
   */
  public function hasFile()
  {
    return (bool) $this->countFiles();
  }
  
  
  /**
   * Check if the project have one or more documetations
   *
   * @return  bool    True if the project have one documetation at least, false else
   * @access  public
   */
  public function hasDocumentation()
  {
    return (bool) $this->countDocumentations();
  }


  /**
   * Fetch the i18n object for this object culture.
   *
   * @param   string          $culture    The culture to set
   * @return  mixed                       A i18n object
   * @throws  PropelException             Any exceptions caught during processing will be rethrown wrapped into a PropelException.
   * @link    http://snippets.symfony-project.org/snippet/237 -- modified for Symfony 1.2
   */
  public function getCurrentProjectI18n($culture = null)
  {
    if (is_null($culture)) {
      $culture = is_null($this->culture) ? sfPropel::getDefaultCulture() : $this->culture;
    }
 
    if (!isset($this->current_i18n[$culture]))
    {
      $obj = ProjectI18nPeer::retrieveByPK($this->getId(), $culture);
      if ($obj !== null) { // Test if there is a translation for current culture
        $this->setProjectI18nForCulture($obj, $culture);
      } else { // Create a translation for this culture
        $new_i18n = new ProjectI18n();
 
        $default_culture = sfConfig::get('sf_default_culture');
 
        // We try to fetch the default culture translation to initialise the new culture.
        if (!isset($this->current_i18n[$default_culture])) {
          $obj = ProjectI18nPeer::retrieveByPK($this->getId(), $default_culture);
          if ($obj !== null) { // Test if there is a translation for current culture
            $this->setProjectI18nForCulture($obj, $default_culture);
          }
        } else {
          $obj = $this->current_i18n[$default_culture];
        }
 
        if ($obj !== null) {
          $obj->copyInto($new_i18n);
        }
 
        $new_i18n->setId($this->getId());
        $new_i18n->setCulture($culture);
 
        $this->setProjectI18nForCulture($new_i18n, $culture);
      }
    }
 
    return $this->current_i18n[$culture];
  }


  /**
   * Stores the object in the database while setting default culture if necessary.
   *
   * If the object is new, it inserts it; otherwise an update is performed.
   * All related objects are also updated in this method.
   *
   * @param      Connection      $con The database connection
   * @return     int             The number of rows affected by this insert/update and any referring fk objects' save() operations.
   * @throws     PropelException Any exceptions caught during processing will be rethrown wrapped into a PropelException.
   * @see        save()
   * @link       http://snippets.symfony-project.org/snippet/237 -- modified for Symfony 1.2
   */
  protected function doSave(PropelPDO $con)
  {
    $default_culture = sfConfig::get('sf_default_culture');
    $current_culture = is_null($this->culture) ? sfPropel::getDefaultCulture() : $this->culture;
    $obj = null;
 
    // We try to fetch the default culture translation to initialise the new culture.
    if (!isset($this->current_i18n[$default_culture])) {
      $obj = ProjectI18nPeer::retrieveByPK($this->getId(), $default_culture, $con);
      if ($obj !== null) { // Test if there is a translation for current culture
        $this->setProjectI18nForCulture($obj, $default_culture);
      }
    } else {
      $obj = $this->current_i18n[$default_culture];
    }
 
    if($obj === null && isset($this->current_i18n[$current_culture])) {
      $new_i18n = new ProjectI18n();
      $this->current_i18n[$current_culture]->copyInto($new_i18n);
 
      $new_i18n->setId($this->getId());
      $new_i18n->setCulture($default_culture);
 
      $this->setProjectI18nForCulture($new_i18n, $default_culture);
    }
 
    return parent::doSave($con);
  }

}