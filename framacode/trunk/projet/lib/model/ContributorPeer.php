<?php

class ContributorPeer extends BaseContributorPeer
{
  /**
   * Add an order for the contributor by pseudo or last name switch show_real_name
   *
   * @param   Criteria        $criteria     The criteria's object
   * @param   string          $sort         'asc' or 'desc' for the order of the sort
   * @throws  PropelException               If $sort isn't equal than asc or desc, an exception is throw
   * @access  public
   * @static
   */
  public static function addOrder(Criteria $criteria, $sort = 'asc')
  {
    ContributorPeer::addSelectColumns($criteria);
    $case = 'CASE '.ContributorPeer::SHOW_REAL_NAME.' WHEN 0 THEN '.ContributorPeer::PSEUDO.' WHEN 1 THEN '.ContributorPeer::FIRST_NAME.' END';
    $criteria->addAsColumn('NAME_SHOW', $case);
    
    if ($sort === 'asc') {
      $criteria->addAscendingOrderByColumn('NAME_SHOW');
    } elseif ($sort === 'desc') {
      $criteria->addDescendingOrderByColumn('NAME_SHOW');
    } else {
      throw new PropelException('$sort must be asc or desc !');
    }
  }
  
  
  /**
   * Get the contributor with this pseudo
   *
   * @param   string      $pseudo   The pseudo of the contributor you want
   * @return  Contributor           The contributor corresponding to the peusdo or null if there is no contributor with this peusdo
   * @throws  sfException           If the pseudo isn't a string an exception is throw
   * @access  public
   * @static
   */
  public static function retrieveByPseudo($pseudo)
  {
    if (is_string($pseudo) === false) {
      throw new sfException('pseudo must be a string !');
    }
    
    $criteria = new Criteria();
    $criteria->add(ContributorPeer::PSEUDO, (string) $pseudo);
    
    return ContributorPeer::doSelectOne($criteria);
  }
}
