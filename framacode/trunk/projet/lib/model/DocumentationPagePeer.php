<?php

class DocumentationPagePeer extends BaseDocumentationPagePeer
{
  /**
   * Retrieve a documentation page by this label
   *
   * @param   string            $label    The label of the documentation page we must retrieve
   * @return  DocumentationPage           The documentation page associated with the label, null if there is non documentation
   * @access  public
   * @static
   */
  public static function retrieveByLabel($label)
  {
    $criteria = new Criteria();
    $criteria->add(DocumentationPagePeer::LABEL, $label, Criteria::EQUAL);
    
    return DocumentationPagePeer::doSelectOne($criteria);
  }


  const COUNT_DISTINCT = 'COUNT(DISTINCT documentation_page.ID)';

  /**
   * Returns the number of rows matching criteria with I18N criteria.
   *
   * @param       Criteria    $criteria   The criteria's object
   * @param       boolean     $distinct   Whether to select only distinct columns (You can also set DISTINCT modifier in Criteria).
   * @param       Connection  $con        An optional database connection
   * @param       string      $culture    The selected culture.
   * @return      int                     Number of matching rows.
   * @link        http://snippets.symfony-project.org/snippet/237 -- modified for Symfony 1.2
   */
  public static function doCountWithI18n(Criteria $criteria = null, $distinct = false, PropelPDO $con = null, $culture = null)
  {
    // we're going to modify criteria, so copy it first
    if ($criteria === null){
      $criteria = new Criteria();
    } else {
      $criteria = clone $criteria;
    }
 
    $default_culture = sfConfig::get('sf_default_culture');
 
    if ($culture === null) {
      // We use current user culture.
      $culture = sfContext::getInstance()->getUser()->getCulture();
    }
 
    // clear out anything that might confuse the ORDER BY clause
    $criteria->clearSelectColumns()->clearOrderByColumns();
    $criteria->addSelectColumn(DocumentationPagePeer::COUNT_DISTINCT);
 
    // just in case we're grouping: add those columns to the select statement
    foreach($criteria->getGroupByColumns() as $column)
    {
      $criteria->addSelectColumn($column);
    }
 
    $criteria->addJoin(DocumentationPagePeer::ID, DocumentationPageI18nPeer::ID);
    $criterion = $criteria->getNewCriterion(DocumentationPageI18nPeer::CULTURE, $culture);
    $criterion->addOr($criteria->getNewCriterion(DocumentationPageI18nPeer::CULTURE, $default_culture));
    $criteria->add($criterion);
 
    $rs = DocumentationPagePeer::doSelectStmt($criteria, $con);
    if ($res = $rs->fetchColumn(0)) {
      return $res;
    } else {
      // no rows returned; we infer that means 0 matches.
      return 0;
    }
  }


  /**
   * Selects a collection of DocumentationPage objects pre-filled with their i18n objects.
   *
   * @param     Criteria         $criteria   The criteria's object
   * @param     string           $culture    The selected culture.
   * @param     PropelPDO        $con        An optional database connection
   * @return    array                        Array of DocumentationPage objects.
   * @throws    PropelException              Any exceptions caught during processing will be rethrown wrapped into a PropelException.
   * @link      http://snippets.symfony-project.org/snippet/237 -- modified for Symfony 1.2
   */
  public static function doSelectWithI18n(Criteria $criteria, $culture = null, PropelPDO $con = null)
  {
    $criteria = clone $criteria;
 
    if ($culture === null) {
      $culture = sfContext::getInstance()->getUser()->getCulture();
    }
 
    $default_culture = sfConfig::get('sf_default_culture');
 
    // Set the correct dbName if it has not been overridden
    if ($criteria->getDbName() == Propel::getDefaultDB()) {
      $criteria->setDbName(self::DATABASE_NAME);
    }
 
    DocumentationPagePeer::addSelectColumns($criteria);
    $startcol = (DocumentationPagePeer::NUM_COLUMNS - DocumentationPagePeer::NUM_LAZY_LOAD_COLUMNS);
 
    DocumentationPageI18nPeer::addSelectColumns($criteria);
 
    $criteria->addJoin(DocumentationPagePeer::ID, DocumentationPageI18nPeer::ID);
    $criterion = $criteria->getNewCriterion(DocumentationPageI18nPeer::CULTURE, $culture);
    $criterion->addOr($criteria->getNewCriterion(DocumentationPageI18nPeer::CULTURE, $default_culture));
    $criteria->add($criterion);
 
    $stmt = BasePeer::doSelect($criteria, $con);
    $results = array();
    $uncultured_results = array();
 
    while($row = $stmt->fetch(PDO::FETCH_NUM)) {

      $obj1 = new DocumentationPage();
      $obj1->hydrate($row);
      $obj1->setCulture($culture);
 
      if(isset($results[$obj1->getId()])) {
        $obj1 = $results[$obj1->getId()];
      }
 
      $omClass = DocumentationPageI18nPeer::getOMClass($row, $startcol);
 
      $cls = Propel::importClass($omClass);
      $obj2 = new $cls();
      $obj2->hydrate($row, $startcol);
 
      $obj1->setDocumentationPageI18nForCulture($obj2, $obj2->getCulture());
      $obj2->setDocumentationPage($obj1);
 
      if(!isset($uncultured_results[$obj1->getId()])) {
        $uncultured_results[$obj1->getId()] = $obj1;
      }
 
      if($obj2->getCulture() == $culture) {
        $uncultured_results[$obj1->getId()] = false;
      }
 
      if(!isset($results[$obj1->getId()])) {
        $results[$obj1->getId()] = $obj1;
      } elseif($obj2->getCulture() == $culture) {
        // Move result to the end of results array to fit eventual sort
        // criteria (ugly fix).
        unset($results[$obj1->getId()]);
        $results[$obj1->getId()] = $obj1;
      }
    }
 
    foreach ($uncultured_results as $obj1) {
      if ($obj1) {
        $obj1->setCulture($default_culture);
        $default_culture_object = $obj1->getCurrentDocumentationPageI18n();
        if ($default_culture_object) {
          $obj2 = new DocumentationPageI18n();
          $default_culture_object->copyInto($obj2);
          $obj2->setCulture($culture);
          $obj2->setDocumentationPage($obj1);
          $obj1->setDocumentationPageI18nForCulture($obj2, $obj2->getCulture());
        }
        $obj1->setCulture($culture);
      }
    }
 
    return array_values($results);
  }

}