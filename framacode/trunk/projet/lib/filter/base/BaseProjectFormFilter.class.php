<?php

require_once(sfConfig::get('sf_lib_dir').'/filter/base/BaseFormFilterPropel.class.php');

/**
 * Project filter form base class.
 *
 * @package    FramaCode
 * @subpackage filter
 * @author     Simon Leblanc <contact@leblanc-simon.eu>
 * @version    SVN: $Id: sfPropelFormFilterGeneratedTemplate.php 16976 2009-04-04 12:47:44Z fabien $
 */
class BaseProjectFormFilter extends BaseFormFilterPropel
{
  public function setup()
  {
    $this->setWidgets(array(
      'name'                         => new sfWidgetFormFilterInput(),
      'label'                        => new sfWidgetFormFilterInput(),
      'create_at'                    => new sfWidgetFormFilterDate(array('from_date' => new sfWidgetFormDate(), 'to_date' => new sfWidgetFormDate(), 'with_empty' => true)),
      'update_at'                    => new sfWidgetFormFilterDate(array('from_date' => new sfWidgetFormDate(), 'to_date' => new sfWidgetFormDate(), 'with_empty' => true)),
      'versionning_url'              => new sfWidgetFormFilterInput(),
      'project_url'                  => new sfWidgetFormFilterInput(),
      'website'                      => new sfWidgetFormFilterInput(),
      'visible'                      => new sfWidgetFormChoice(array('choices' => array('' => 'yes or no', 1 => 'yes', 0 => 'no'))),
      'position'                     => new sfWidgetFormFilterInput(),
      'project_has_contributor_list' => new sfWidgetFormPropelChoice(array('model' => 'Contributor', 'add_empty' => true)),
      'project_has_license_list'     => new sfWidgetFormPropelChoice(array('model' => 'License', 'add_empty' => true)),
    ));

    $this->setValidators(array(
      'name'                         => new sfValidatorPass(array('required' => false)),
      'label'                        => new sfValidatorPass(array('required' => false)),
      'create_at'                    => new sfValidatorDateRange(array('required' => false, 'from_date' => new sfValidatorDate(array('required' => false)), 'to_date' => new sfValidatorDate(array('required' => false)))),
      'update_at'                    => new sfValidatorDateRange(array('required' => false, 'from_date' => new sfValidatorDate(array('required' => false)), 'to_date' => new sfValidatorDate(array('required' => false)))),
      'versionning_url'              => new sfValidatorPass(array('required' => false)),
      'project_url'                  => new sfValidatorPass(array('required' => false)),
      'website'                      => new sfValidatorPass(array('required' => false)),
      'visible'                      => new sfValidatorChoice(array('required' => false, 'choices' => array('', 1, 0))),
      'position'                     => new sfValidatorSchemaFilter('text', new sfValidatorInteger(array('required' => false))),
      'project_has_contributor_list' => new sfValidatorPropelChoice(array('model' => 'Contributor', 'required' => false)),
      'project_has_license_list'     => new sfValidatorPropelChoice(array('model' => 'License', 'required' => false)),
    ));

    $this->widgetSchema->setNameFormat('project_filters[%s]');

    $this->errorSchema = new sfValidatorErrorSchema($this->validatorSchema);

    parent::setup();
  }

  public function addProjectHasContributorListColumnCriteria(Criteria $criteria, $field, $values)
  {
    if (!is_array($values))
    {
      $values = array($values);
    }

    if (!count($values))
    {
      return;
    }

    $criteria->addJoin(ProjectHasContributorPeer::PROJECT_ID, ProjectPeer::ID);

    $value = array_pop($values);
    $criterion = $criteria->getNewCriterion(ProjectHasContributorPeer::CONTRIBUTOR_ID, $value);

    foreach ($values as $value)
    {
      $criterion->addOr($criteria->getNewCriterion(ProjectHasContributorPeer::CONTRIBUTOR_ID, $value));
    }

    $criteria->add($criterion);
  }

  public function addProjectHasLicenseListColumnCriteria(Criteria $criteria, $field, $values)
  {
    if (!is_array($values))
    {
      $values = array($values);
    }

    if (!count($values))
    {
      return;
    }

    $criteria->addJoin(ProjectHasLicensePeer::PROJECT_ID, ProjectPeer::ID);

    $value = array_pop($values);
    $criterion = $criteria->getNewCriterion(ProjectHasLicensePeer::LICENSE_ID, $value);

    foreach ($values as $value)
    {
      $criterion->addOr($criteria->getNewCriterion(ProjectHasLicensePeer::LICENSE_ID, $value));
    }

    $criteria->add($criterion);
  }

  public function getModelName()
  {
    return 'Project';
  }

  public function getFields()
  {
    return array(
      'id'                           => 'Number',
      'name'                         => 'Text',
      'label'                        => 'Text',
      'create_at'                    => 'Date',
      'update_at'                    => 'Date',
      'versionning_url'              => 'Text',
      'project_url'                  => 'Text',
      'website'                      => 'Text',
      'visible'                      => 'Boolean',
      'position'                     => 'Number',
      'project_has_contributor_list' => 'ManyKey',
      'project_has_license_list'     => 'ManyKey',
    );
  }
}
