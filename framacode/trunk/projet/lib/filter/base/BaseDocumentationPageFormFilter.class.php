<?php

require_once(sfConfig::get('sf_lib_dir').'/filter/base/BaseFormFilterPropel.class.php');

/**
 * DocumentationPage filter form base class.
 *
 * @package    FramaCode
 * @subpackage filter
 * @author     Simon Leblanc <contact@leblanc-simon.eu>
 * @version    SVN: $Id: sfPropelFormFilterGeneratedTemplate.php 16976 2009-04-04 12:47:44Z fabien $
 */
class BaseDocumentationPageFormFilter extends BaseFormFilterPropel
{
  public function setup()
  {
    $this->setWidgets(array(
      'documentation_id' => new sfWidgetFormPropelChoice(array('model' => 'Documentation', 'add_empty' => true)),
      'label'            => new sfWidgetFormFilterInput(),
      'position'         => new sfWidgetFormFilterInput(),
    ));

    $this->setValidators(array(
      'documentation_id' => new sfValidatorPropelChoice(array('required' => false, 'model' => 'Documentation', 'column' => 'id')),
      'label'            => new sfValidatorPass(array('required' => false)),
      'position'         => new sfValidatorSchemaFilter('text', new sfValidatorInteger(array('required' => false))),
    ));

    $this->widgetSchema->setNameFormat('documentation_page_filters[%s]');

    $this->errorSchema = new sfValidatorErrorSchema($this->validatorSchema);

    parent::setup();
  }

  public function getModelName()
  {
    return 'DocumentationPage';
  }

  public function getFields()
  {
    return array(
      'id'               => 'Number',
      'documentation_id' => 'ForeignKey',
      'label'            => 'Text',
      'position'         => 'Number',
    );
  }
}
