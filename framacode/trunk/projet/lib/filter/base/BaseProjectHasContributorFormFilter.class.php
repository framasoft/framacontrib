<?php

require_once(sfConfig::get('sf_lib_dir').'/filter/base/BaseFormFilterPropel.class.php');

/**
 * ProjectHasContributor filter form base class.
 *
 * @package    FramaCode
 * @subpackage filter
 * @author     Simon Leblanc <contact@leblanc-simon.eu>
 * @version    SVN: $Id: sfPropelFormFilterGeneratedTemplate.php 16976 2009-04-04 12:47:44Z fabien $
 */
class BaseProjectHasContributorFormFilter extends BaseFormFilterPropel
{
  public function setup()
  {
    $this->setWidgets(array(
      'contributor_type_id' => new sfWidgetFormPropelChoice(array('model' => 'ContributorType', 'add_empty' => true)),
    ));

    $this->setValidators(array(
      'contributor_type_id' => new sfValidatorPropelChoice(array('required' => false, 'model' => 'ContributorType', 'column' => 'id')),
    ));

    $this->widgetSchema->setNameFormat('project_has_contributor_filters[%s]');

    $this->errorSchema = new sfValidatorErrorSchema($this->validatorSchema);

    parent::setup();
  }

  public function getModelName()
  {
    return 'ProjectHasContributor';
  }

  public function getFields()
  {
    return array(
      'project_id'          => 'ForeignKey',
      'contributor_id'      => 'ForeignKey',
      'contributor_type_id' => 'ForeignKey',
    );
  }
}
