<?php

/**
 * Project filter form base class.
 *
 * @package    FramaCode
 * @subpackage filter
 * @author     Simon Leblanc <contact@leblanc-simon.eu>
 * @version    SVN: $Id: sfPropelFormFilterBaseTemplate.php 11675 2008-09-19 15:21:38Z fabien $
 */
abstract class BaseFormFilterPropel extends sfFormFilterPropel
{
  public function setup()
  {
  }
}
