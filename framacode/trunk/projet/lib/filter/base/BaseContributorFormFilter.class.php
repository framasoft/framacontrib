<?php

require_once(sfConfig::get('sf_lib_dir').'/filter/base/BaseFormFilterPropel.class.php');

/**
 * Contributor filter form base class.
 *
 * @package    FramaCode
 * @subpackage filter
 * @author     Simon Leblanc <contact@leblanc-simon.eu>
 * @version    SVN: $Id: sfPropelFormFilterGeneratedTemplate.php 16976 2009-04-04 12:47:44Z fabien $
 */
class BaseContributorFormFilter extends BaseFormFilterPropel
{
  public function setup()
  {
    $this->setWidgets(array(
      'pseudo'                       => new sfWidgetFormFilterInput(),
      'email'                        => new sfWidgetFormFilterInput(),
      'first_name'                   => new sfWidgetFormFilterInput(),
      'last_name'                    => new sfWidgetFormFilterInput(),
      'website'                      => new sfWidgetFormFilterInput(),
      'show_real_name'               => new sfWidgetFormChoice(array('choices' => array('' => 'yes or no', 1 => 'yes', 0 => 'no'))),
      'project_has_contributor_list' => new sfWidgetFormPropelChoice(array('model' => 'Project', 'add_empty' => true)),
    ));

    $this->setValidators(array(
      'pseudo'                       => new sfValidatorPass(array('required' => false)),
      'email'                        => new sfValidatorPass(array('required' => false)),
      'first_name'                   => new sfValidatorPass(array('required' => false)),
      'last_name'                    => new sfValidatorPass(array('required' => false)),
      'website'                      => new sfValidatorPass(array('required' => false)),
      'show_real_name'               => new sfValidatorChoice(array('required' => false, 'choices' => array('', 1, 0))),
      'project_has_contributor_list' => new sfValidatorPropelChoice(array('model' => 'Project', 'required' => false)),
    ));

    $this->widgetSchema->setNameFormat('contributor_filters[%s]');

    $this->errorSchema = new sfValidatorErrorSchema($this->validatorSchema);

    parent::setup();
  }

  public function addProjectHasContributorListColumnCriteria(Criteria $criteria, $field, $values)
  {
    if (!is_array($values))
    {
      $values = array($values);
    }

    if (!count($values))
    {
      return;
    }

    $criteria->addJoin(ProjectHasContributorPeer::CONTRIBUTOR_ID, ContributorPeer::ID);

    $value = array_pop($values);
    $criterion = $criteria->getNewCriterion(ProjectHasContributorPeer::PROJECT_ID, $value);

    foreach ($values as $value)
    {
      $criterion->addOr($criteria->getNewCriterion(ProjectHasContributorPeer::PROJECT_ID, $value));
    }

    $criteria->add($criterion);
  }

  public function getModelName()
  {
    return 'Contributor';
  }

  public function getFields()
  {
    return array(
      'id'                           => 'Number',
      'pseudo'                       => 'Text',
      'email'                        => 'Text',
      'first_name'                   => 'Text',
      'last_name'                    => 'Text',
      'website'                      => 'Text',
      'show_real_name'               => 'Boolean',
      'project_has_contributor_list' => 'ManyKey',
    );
  }
}
