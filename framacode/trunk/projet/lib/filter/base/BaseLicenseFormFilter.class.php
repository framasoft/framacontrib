<?php

require_once(sfConfig::get('sf_lib_dir').'/filter/base/BaseFormFilterPropel.class.php');

/**
 * License filter form base class.
 *
 * @package    FramaCode
 * @subpackage filter
 * @author     Simon Leblanc <contact@leblanc-simon.eu>
 * @version    SVN: $Id: sfPropelFormFilterGeneratedTemplate.php 16976 2009-04-04 12:47:44Z fabien $
 */
class BaseLicenseFormFilter extends BaseFormFilterPropel
{
  public function setup()
  {
    $this->setWidgets(array(
      'name'                     => new sfWidgetFormFilterInput(),
      'label'                    => new sfWidgetFormFilterInput(),
      'url'                      => new sfWidgetFormFilterInput(),
      'logo'                     => new sfWidgetFormFilterInput(),
      'project_has_license_list' => new sfWidgetFormPropelChoice(array('model' => 'Project', 'add_empty' => true)),
    ));

    $this->setValidators(array(
      'name'                     => new sfValidatorPass(array('required' => false)),
      'label'                    => new sfValidatorPass(array('required' => false)),
      'url'                      => new sfValidatorPass(array('required' => false)),
      'logo'                     => new sfValidatorPass(array('required' => false)),
      'project_has_license_list' => new sfValidatorPropelChoice(array('model' => 'Project', 'required' => false)),
    ));

    $this->widgetSchema->setNameFormat('license_filters[%s]');

    $this->errorSchema = new sfValidatorErrorSchema($this->validatorSchema);

    parent::setup();
  }

  public function addProjectHasLicenseListColumnCriteria(Criteria $criteria, $field, $values)
  {
    if (!is_array($values))
    {
      $values = array($values);
    }

    if (!count($values))
    {
      return;
    }

    $criteria->addJoin(ProjectHasLicensePeer::LICENSE_ID, LicensePeer::ID);

    $value = array_pop($values);
    $criterion = $criteria->getNewCriterion(ProjectHasLicensePeer::PROJECT_ID, $value);

    foreach ($values as $value)
    {
      $criterion->addOr($criteria->getNewCriterion(ProjectHasLicensePeer::PROJECT_ID, $value));
    }

    $criteria->add($criterion);
  }

  public function getModelName()
  {
    return 'License';
  }

  public function getFields()
  {
    return array(
      'id'                       => 'Number',
      'name'                     => 'Text',
      'label'                    => 'Text',
      'url'                      => 'Text',
      'logo'                     => 'Text',
      'project_has_license_list' => 'ManyKey',
    );
  }
}
