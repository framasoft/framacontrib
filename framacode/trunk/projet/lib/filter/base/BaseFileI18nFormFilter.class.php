<?php

require_once(sfConfig::get('sf_lib_dir').'/filter/base/BaseFormFilterPropel.class.php');

/**
 * FileI18n filter form base class.
 *
 * @package    FramaCode
 * @subpackage filter
 * @author     Simon Leblanc <contact@leblanc-simon.eu>
 * @version    SVN: $Id: sfPropelFormFilterGeneratedTemplate.php 16976 2009-04-04 12:47:44Z fabien $
 */
class BaseFileI18nFormFilter extends BaseFormFilterPropel
{
  public function setup()
  {
    $this->setWidgets(array(
      'note'      => new sfWidgetFormFilterInput(),
      'changelog' => new sfWidgetFormFilterInput(),
    ));

    $this->setValidators(array(
      'note'      => new sfValidatorPass(array('required' => false)),
      'changelog' => new sfValidatorPass(array('required' => false)),
    ));

    $this->widgetSchema->setNameFormat('file_i18n_filters[%s]');

    $this->errorSchema = new sfValidatorErrorSchema($this->validatorSchema);

    parent::setup();
  }

  public function getModelName()
  {
    return 'FileI18n';
  }

  public function getFields()
  {
    return array(
      'id'        => 'ForeignKey',
      'culture'   => 'Text',
      'note'      => 'Text',
      'changelog' => 'Text',
    );
  }
}
