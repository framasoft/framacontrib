<?php

require_once(sfConfig::get('sf_lib_dir').'/filter/base/BaseFormFilterPropel.class.php');

/**
 * File filter form base class.
 *
 * @package    FramaCode
 * @subpackage filter
 * @author     Simon Leblanc <contact@leblanc-simon.eu>
 * @version    SVN: $Id: sfPropelFormFilterGeneratedTemplate.php 16976 2009-04-04 12:47:44Z fabien $
 */
class BaseFileFormFilter extends BaseFormFilterPropel
{
  public function setup()
  {
    $this->setWidgets(array(
      'project_id'  => new sfWidgetFormPropelChoice(array('model' => 'Project', 'add_empty' => true)),
      'name'        => new sfWidgetFormFilterInput(),
      'label'       => new sfWidgetFormFilterInput(),
      'size'        => new sfWidgetFormFilterInput(),
      'create_at'   => new sfWidgetFormFilterDate(array('from_date' => new sfWidgetFormDate(), 'to_date' => new sfWidgetFormDate(), 'with_empty' => true)),
      'nb_download' => new sfWidgetFormFilterInput(),
      'version'     => new sfWidgetFormFilterInput(),
    ));

    $this->setValidators(array(
      'project_id'  => new sfValidatorPropelChoice(array('required' => false, 'model' => 'Project', 'column' => 'id')),
      'name'        => new sfValidatorPass(array('required' => false)),
      'label'       => new sfValidatorPass(array('required' => false)),
      'size'        => new sfValidatorSchemaFilter('text', new sfValidatorNumber(array('required' => false))),
      'create_at'   => new sfValidatorDateRange(array('required' => false, 'from_date' => new sfValidatorDate(array('required' => false)), 'to_date' => new sfValidatorDate(array('required' => false)))),
      'nb_download' => new sfValidatorSchemaFilter('text', new sfValidatorInteger(array('required' => false))),
      'version'     => new sfValidatorPass(array('required' => false)),
    ));

    $this->widgetSchema->setNameFormat('file_filters[%s]');

    $this->errorSchema = new sfValidatorErrorSchema($this->validatorSchema);

    parent::setup();
  }

  public function getModelName()
  {
    return 'File';
  }

  public function getFields()
  {
    return array(
      'id'          => 'Number',
      'project_id'  => 'ForeignKey',
      'name'        => 'Text',
      'label'       => 'Text',
      'size'        => 'Number',
      'create_at'   => 'Date',
      'nb_download' => 'Number',
      'version'     => 'Text',
    );
  }
}
