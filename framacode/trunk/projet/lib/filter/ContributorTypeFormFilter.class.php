<?php

/**
 * ContributorType filter form.
 *
 * @package    FramaCode
 * @subpackage filter
 * @author     Simon Leblanc <contact@leblanc-simon.eu>
 * @version    SVN: $Id: sfPropelFormFilterTemplate.php 11675 2008-09-19 15:21:38Z fabien $
 */
class ContributorTypeFormFilter extends BaseContributorTypeFormFilter
{
  public function configure()
  {
  }
}
