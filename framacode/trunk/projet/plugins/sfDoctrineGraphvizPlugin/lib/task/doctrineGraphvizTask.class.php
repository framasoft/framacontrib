<?php

/*
 * This file is part of sfDoctrineGraphvizPlugin
 * (c) 2009 David PHAM-VAN
 *
 * For the full copyright and license information, please view the LICENSE
 * file that was distributed with this source code.
 */

/**
 * @package    sfDoctrineGraphvizPlugin
 * @author     David PHAM-VAN
 * @version    SVN: $Id:  $
 */
class doctrineGraphvizTask extends sfBaseTask
{
  protected function configure()
  {
    $this->addOptions(array(
      new sfCommandOption('application', null, sfCommandOption::PARAMETER_REQUIRED, 'The application name'),
      new sfCommandOption('env', null, sfCommandOption::PARAMETER_REQUIRED, 'The environment', 'dev'),
      new sfCommandOption('connection', null, sfCommandOption::PARAMETER_REQUIRED, 'The connection name', 'doctrine'),
    ));

    $this->namespace        = 'doctrine';
    $this->name             = 'graphviz';
    $this->briefDescription = 'Doctrine database schema visualisation.';
    $this->detailedDescription = <<<EOF
The [doctrine:graphviz|INFO] task output database MCD and MLD schema using graphviz.
Call it with:

  [php symfony doctrine:graphviz|INFO]
EOF;
  }

  public function searchTables() {
    $tables = array();
    $config = new sfAutoloadConfigHandler();
    $mapping = $config->evaluate($this->configuration->getConfigPaths('config/autoload.yml'));
    foreach ($mapping as $class => $file)
    {
      if (substr($class, -5)=='Table' && is_subclass_of($class, 'Doctrine_Table'))
      {
        $tables[]=substr($class, 0, -5);
      }
    }
    return $tables;
  }

  private function listColumns($table) {
    $ret=Array();
    foreach ($table->getColumns() as $name=>$column) {
      if (!@$column['primary']) {
        $ajoute=True;
        foreach ($table->getRelations() as $relation) {
          if (is_a($relation, 'Doctrine_Relation_LocalKey') && $relation->getLocal()==$name) {
            $ajoute=False;
            break;
          }
        }
        if ($ajoute)
          $ret[]="$name ($column[type])";
      }
    }
    return $ret;
  }

  public function genMCD()
  {
    $tables = $this->searchTables();

    $relations = array();
    foreach($tables as $tableName)
    {
      $table = Doctrine::getTable($tableName);
      foreach ($table->getRelations() as $relation)
      {
        if ($relation->getType() === Doctrine_Relation::MANY && isset($relation['refTable']))
        {
          $relations[] = $relation['refTable']->name;
        }
      }
    }

    $entites = array();
    $assocs = array();
    $liens = array();
    $gens = array();

    foreach($tables as $tableName) {
      $table = Doctrine::getTable($tableName);
      $entites[$tableName]=array();
      $entites[$tableName]=$this->listColumns($table);
      if (count($entites[$tableName])==0) {
        unset($entites[$tableName]);
        $relations[]=$tableName;
      }
    }

    $relations = array_unique($relations);
    foreach($relations as $relation)
    {
      unset($tables[array_search($relation, $tables)]);
    }


    foreach($relations as $tableName) {
      $table = Doctrine::getTable($tableName);
      $assocs[$tableName]=array();
      $assocs[$tableName]=$this->listColumns($table);
    }

    foreach($relations as $tableName) {
      $table = Doctrine::getTable($tableName);
      foreach ($table->getRelations() as $name => $relation) {
        if (is_a($relation, 'Doctrine_Relation_LocalKey')) {
          $liens[]=Array($tableName, $relation->getTable()->name, '0,n', $relation->getAlias());
        }
      }
    }

    foreach($tables as $tableName) {
      $table = Doctrine::getTable($tableName);
      foreach ($table->getRelations() as $name => $relation) {
        if (!in_array($relation->getTable()->name, $relations)){
          if (is_a($relation, 'Doctrine_Relation_LocalKey')) {
            if (!$relation->getTable()->getRelation($tableName)->isOneToOne()) {
              $assocName=$tableName.$relation->getTable()->name;
              $liens[]=Array($assocName, $relation->getTable()->name, '0,n', $relation->getAlias());
            }
          } elseif (is_a($relation, 'Doctrine_Relation_ForeignKey')) {
            if ($relation->isOneToOne()) {
              $gens[]=Array($tableName, $relation->getTable()->name);
            } else {
              $assocName=$relation->getTable()->name.$tableName;
              $liens[]=Array($assocName, $relation->getTable()->name, '0,1', $relation->getAlias());
            }
          }
        }
      }
    }

    foreach($liens as $lien) {
      if (!@$assocs[$lien[0]]) {
        $assocs[$lien[0]]=Array();
      }
    }

    $digraph="graph G {\noverlap=false;splines=true\n";
    foreach($entites as $entite=>$champs) {
      $digraph.="node".$entite." [label=\"{<table>".$entite."|<cols>".implode("\l", $champs)."}\", shape=record];\n";
    }
    $digraph.="\n";
    foreach($assocs as $assoc=>$champs) {
      $digraph.="node".$assoc." [label=\"{<table>".$assoc."|<cols>".implode("\l", $champs)."}\", shape=Mrecord];\n";
    }
    $digraph.="\n";
    foreach($liens as $lien) {
      if ($lien[3]==$lien[1])
        $lien[3]='';
      else
        $lien[3]="($lien[3])";
      $digraph.="node$lien[0] -- node$lien[1] [headlabel=\"$lien[2]\",label=\"$lien[3]\",labeldistance=3];\n";
    }

    $digraph.="\n";
    foreach($gens as $gen) {
      $digraph.="node$gen[0] -- node$gen[1] [arrowhead=normal];\n";
    }

    $digraph.="}\n";
    return $digraph;
  }

  public function genMLD()
  {
    $tables = $this->searchTables();

    $digraph="digraph G {edge  [ len=2 labeldistance=2 ];overlap=false;splines=true;\n";
    foreach($tables as $tableName) {
      $table = Doctrine::getTable($tableName);
      $digraph.="node".$table->name." [label=\"{<table>".$table->tableName."|<cols>";
      foreach ($table->getColumns() as $name=>$column) {
        $digraph.="$name ($column[type])".(@$column['primary']?' [PK]':'')."\l";
      }
      $digraph.="}\", shape=record];\n";
    }

    $digraph.="\n";

    $rel=Array();
    foreach($tables as $tableName) {
      $table = Doctrine::getTable($tableName);
      foreach ($table->getRelations() as $name => $relation)
      {
        if (is_a($relation, 'Doctrine_Relation_LocalKey'))
        {
          $rel[]="node".$table->name.":cols -> node".$relation->getTable()->name.":table [label=\"".$relation->getLocal()."=".$relation->getForeign()." \"];";
        }
      }
    }

    $rel = array_unique($rel);
    $digraph.=implode("\n", $rel)."}\n";
    return $digraph;
  }

  protected function execute($arguments = array(), $options = array())
  {
    // initialize the database connection
    $databaseManager = new sfDatabaseManager($this->configuration);
    $connection = $databaseManager->getDatabase($options['connection'] ? $options['connection'] : null)->getConnection();

    $digraph = $this->genMCD();

    $f=fopen('graph/mcd.schema.dot', 'w');
    fwrite($f, $digraph);
    fclose($f);

    $fs = new sfFilesystem();
    $fs->sh('echo '.escapeshellarg($digraph).' | twopi -Tpng -ograph/mcd.schema.png');


    $digraph = $this->genMLD();

    $f=fopen('graph/mld.schema.dot', 'w');
    fwrite($f, $digraph);
    fclose($f);

    $fs = new sfFilesystem();
    $fs->sh('echo '.escapeshellarg($digraph).' | dot -Tpng -ograph/mld.schema.png');
  }
}
