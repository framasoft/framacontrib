<?php
/**
 * sfPropelBuildI18nDefaultCulture plugin task.
 *
 * @package    sfPropelBuildI18nDefaultCulture
 * @subpackage propel
 * @author     Simon Leblanc <contact@leblanc-simon.eu>
 * @license    http://www.opensource.org/licenses/bsd-license.php BSD
 */
class sfPropelBuildI18nDefaultCultureTask extends sfPropelBaseTask
{
  protected function configure()
  {
    $this->namespace        = 'propel';
    $this->name             = 'build-i18n';
    $this->briefDescription = 'Add the essential method for default culture with I18N and Propel';
    $this->detailedDescription = <<<EOF
The [PropelBuildI18n|INFO] task does things.
Call it with:

  [php symfony propel:build-i18n|INFO]
EOF;
  }

  /**
   * Execute the task : Add the essential method for default culture with I18N and Propel
   *
   * @param   array       $arguments
   * @param   array       $options
   * @throws  sfException             If there is no schema, an exception is throw
   * @access  protected
   * @todo    add table exception
   */
  protected function execute($arguments = array(), $options = array())
  {
    // Add essential path
    self::setIncludeAndPath();
    
    // Get the schemas
    $this->schemaToXML(self::DO_NOT_CHECK_SCHEMA, 'generated-');
    $this->copyXmlSchemaFromPlugins('generated-');
    
    $schemas = sfFinder::type('file')->name('*schema.xml')->follow_link()->in(sfConfig::get('sf_config_dir'));
    if (self::CHECK_SCHEMA === $checkSchema && !$schemas) {
      throw new sfCommandException('You must create a schema.yml or schema.xml file.');
    }
    
    // Initialize the $tables var
    $this->tables = array();
    
    // check all schema
    foreach ($schemas as $schema) {
      $this->checkTable($schema);
    }
    
    // Add i18n method
    $this->addI18n();
  }
  
  
  /**
   * Check all table of the schema and add the i18n table in the $table var
   *
   * @param   string      $schema   The schema file (with path) to check
   * @throws  sfException           If the schema file doesn't exist, an exception is throw
   * @access  protected
   */
  protected function checkTable($schema)
  {
    if (file_exists($schema) === false) {
      throw new sfException('the file "'.$schema.'" doesn\'t exist !');
    }
    
    $dom = new DOMDocument();
    $dom->load($schema);
    
    $tables = $dom->getElementsByTagName('table');
    foreach ($tables as $table) {
      if ($table->getAttribute('isI18N') === 'true') {
        $i = count($this->tables);
        $this->tables[$i]['name'] = $table->getAttribute('name');
        $this->tables[$i]['i18n'] = $table->getAttribute('i18nTable');
      }
    }
  }
  
  
  /**
   * Add I18n method in all table
   *
   * @access  protected
   */
  protected function addI18n()
  {
    foreach ($this->tables as $table) {
      $this->addI18nMethod($table);
      $this->addI18nPeerMethod($table);
    }
  }
  
  
  /**
   * Add 2 method in the TableClassName.php file for I18N
   *
   * @param   array       $table    The array with table name and table i18n name
   * @access  protected
   */
  protected function addI18nMethod($table)
  {
    $class_file = sfConfig::get('sf_lib_dir').DIRECTORY_SEPARATOR.'model'.DIRECTORY_SEPARATOR.self::getClassTableName($table['name']).'.php';
    
    // getCurrentTableClassNameI18n method
    $this->checkAndAddMethod($class_file, 'getCurrentTableClassNameI18n', $table);
    
    // doSave method
    $this->checkAndAddMethod($class_file, 'doSave', $table);
  }
  
  
  /**
   * Add 2 method in the TableClassNamePeer.php file for I18N
   *
   * @param   array       $table    The array with table name and table i18n name
   * @access  protected
   */
  protected function addI18nPeerMethod($table)
  {
    $class_file = sfConfig::get('sf_lib_dir').DIRECTORY_SEPARATOR.'model'.DIRECTORY_SEPARATOR.self::getClassTableName($table['name']).'Peer.php';
    
    // getCurrentTableClassNameI18n method
    $this->checkAndAddMethod($class_file, 'doCountWithI18n', $table);
    
    // doSave method
    $this->checkAndAddMethod($class_file, 'doSelectWithI18n', $table);
  }
  
  
  /**
   * Check and add, if necessary, the method in the class file
   *
   * @param   string      $file   The class where we must add the method
   * @param   string      $method The name of the method to add
   * @param   array       $table  The array with table name and table i18n name
   * @throws  sfException         If the template of the method doesn't exist, an exception is throw
   * @throws  sfException         If the write fail for add method, an exception is throw
   * @return  bool                True if the method is added, false else (caution : false !== error)
   * @access  protected
   */
  protected function checkAndAddMethod($file, $method, $table)
  {
    $method_template = $method;
    
    // An exception in the rules : getCurrentTableClassNameI18n is a generic
    // name, we must convert with the class name
    if ($method === 'getCurrentTableClassNameI18n') {
      $method = str_replace('TableClassNameI18n', self::getClassTableName($table['i18n']), $method);
    }
    
    if (self::checkMethod($file, $method) === false) { // The method doesn't exist
      $method_file = dirname(__FILE__).DIRECTORY_SEPARATOR.'..'.DIRECTORY_SEPARATOR.'..'.DIRECTORY_SEPARATOR.'data'.DIRECTORY_SEPARATOR.$method_template.'.php';
      if (file_exists($method_file) === false) {
        throw new sfException('The template for '.$method.' function doesn\'t exist !');
      }
      
      // Get the template of method
      $content = file_get_contents($method_file);
      
      // And replace the vars
      $content = str_replace('TableClassNameI18n', self::getClassTableName($table['i18n']), $content);
      $content = str_replace('TableClassName', self::getClassTableName($table['name']), $content);
      $content = str_replace('TableName', $table['name'], $content);
      
      // Save the new class file
      if (self::addMethod($file, $content) === false) {
        throw new sfException('Fail to add "'.$method.'" in '.$file);
      }
      
      $this->logSection('schema', sprintf('add "%s" to %s', $method, $file));
      
      return true;
    } else {
      return false;
    }
  }
  
  
  /**
   * Check if the method name exist in the class file
   *
   * @param   string      $file         The filename (with path) for the user model class
   * @param   string      $method_name  The name of the method to check
   * @throws  sfException               If file doesn't exist, an exception is throw
   * @return  bool                      True if the method exist in the class file, false else
   * @access  protected
   * @static
   */
  protected static function checkMethod($file, $method_name)
  {
    if (file_exists($file) === false) {
      throw new sfException('The file "'.$file.'" doesn\'t exist !');
    }
    
    $class_content = file_get_contents($file);
    
    // The function name can't be in the first position, so we can type the return
    // because the search return > 0 or false
    return (bool) strpos($class_content, 'function '.$method_name);
  }
  
  
  /**
   * Add the method in the class file
   *
   * @param   string      $file     The filename (with path) for the user model class
   * @param   string      $content  The content of the method to add
   * @throws  sfException           If file doesn't exist, an exception is throw
   * @throws  sfException           If file haven't '}', an exception is throw
   * @return  bool                  True if the class is modified, false else
   * @access  protected
   * @static
   */
  protected static function addMethod($file, $content)
  {
    if (file_exists($file) === false) {
      throw new sfException('The file "'.$file.'" doesn\'t exist !');
    }
    
    $class_content = file_get_contents($file);
    
    // search the last '}'
    $pos = strrpos($class_content, '}');
    if ($pos === false) {
      throw new sfException('There is no "}" in the class !');
    }
    
    // insert the content before the last '}'
    $class_content = substr($class_content, 0, $pos)."\n\n".$content."\n}";
    
    // save the file
    return file_put_contents($file, $class_content);
  }
  
  
  /**
   * Convert the table name to PHP name (class)
   *
   * @param   string  $table_name   The table name
   * @return  string                The PHP class name
   * @see     PhpNameGenerator
   * @access  protected
   * @static
   */
  protected static function getClassTableName($table_name)
  {
    $php_name = new PhpNameGenerator();
    
    return $php_name->generateName(array($table_name, PhpNameGenerator::CONV_METHOD_UNDERSCORE));
  }
  
  
  /**
   * Add essential path in the include_path and include the php file for convert table to php name
   *
   * @access  protected
   * @static
   */
  protected static function setIncludeAndPath()
  {
    $path = sfConfig::get('sf_symfony_lib_dir').DIRECTORY_SEPARATOR.'plugins'.DIRECTORY_SEPARATOR.'sfPropelPlugin'.DIRECTORY_SEPARATOR.'lib'.DIRECTORY_SEPARATOR.'vendor'.DIRECTORY_SEPARATOR.'propel-generator'.DIRECTORY_SEPARATOR.'classes';
    set_include_path(get_include_path().PATH_SEPARATOR.$path);
    
    require_once 'propel'.DIRECTORY_SEPARATOR.'engine'.DIRECTORY_SEPARATOR.'database'.DIRECTORY_SEPARATOR.'model'.DIRECTORY_SEPARATOR.'PhpNameGenerator.php';
  }
}