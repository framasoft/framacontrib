<?php
/**
 * The Abstract class to use the API (twitter compatible) of the microblogging service
 *
 * @author  Simon Leblanc <contact@leblanc-simon.eu>
 * @copyright Simon Leblanc 2009
 * @license http://www.opensource.org/licenses/bsd-license.php  BSD Licence
 * @package MicrobloggingLibrary
 * @abstract
 * @version 1.0
 */
abstract class Microblogging
{
  /**
   * @var     array       The array of relation between the error code and the error message
   * @access  protected
   */
  protected $return_status = array(304 => 'Not Modified: there was no new data to return.',
                                   400 => 'Bad Request: your request is invalid, did you exceed the rate limit ?',
                                   401 => 'Not Authorized: either you need to provide authentication credentials, or the credentials provided aren\'t valid.',
                                   403 => 'Forbidden: access denied to requested data.',
                                   404 => 'Not Found: either you\'re requesting an invalid URI or the resource in question doesn\'t exist (ex: no such user).',
                                   500 => 'Internal Server Error',
                                   502 => 'Bad Gateway: returned if the service is down or being upgraded.',
                                   503 => 'Service Unavailable: the servers are up, but are overloaded with requests. Try again later.',
                                  );
  
  /**
   * @var     string       The current error message
   * @access  protected
   */
  protected $error_msg  = null;
  
  /**
   * @var     string      The current error code
   * @access  protected
   */
  protected $error_code = null;
  
  
  /**
   * @var     string       The username used for connect in the microblogging service
   * @access  protected
   */
  protected $user       = null;
  
  /**
   * @var     string       The password used for connect in the microblogging service
   * @access  protected
   */
  protected $passwd     = null;
  
  
  /**
   * @var     bool       Indicate if you use SSL for connect in the microblogging service
   * @access  protected
   */
  protected $ssl        = false;
  
  /**
   * @var     string      The base URL of the microblogging service
   * @access  protected
   */
  protected $base_url;
  
  /**
   * @var     int         The timeout in second for the connection
   * @access  protected
   */
  protected $time_out   = 15;
  
  /**
   * @var     string      The type of the result you want (must be .json or .xml)
   * @access  protected
   */
  protected $type       = '.json';
  
  /**
   * @var     string      The user Agent send in the microblogging service
   * @access  protected
   */
  protected $user_agent = 'Elgg';
  
  /**
   * @var     string      The headers send in the microblogging service
   * @access  protected
   */
  protected $headers    = array();
  
  
  /**
   * Constructor of the class
   *
   * @throws  Exception   if cURL extension isn't loaded, an exception is throw
   * @access  public
   */
  public function __construct()
  {
    if (extension_loaded('curl') === false) {
      throw new Exception('cURL extension isn\'t loaded !');
    }
  }
  
  
  /**
   * Set the error message depending the error code
   *
   * @access  public
   */
  public function setError()
  {
    if (isset($this->return_status[$this->error_code]) === true) {
      $this->error_msg = $this->return_status[$this->error_code];
    } else {
      $this->error_msg = 'Unknown error !';
    }
  }
  
  
  /**
   * Get the error message
   *
   * @return  string    The error message
   * @access  public
   */
  public function getError()
  {
    return $this->error_msg;
  }
  
  /**
   * Check if there is an error
   *
   * @return  bool    True if there is an error, false else
   */
  public function hasError()
  {
    return !($this->error_code === null);
  }
  
  
  /**
   * Send a request in the service
   *
   * @param   string  $url        The URL where you want send your request
   * @param   bool    $use_auth   Indicate if you want use authentication
   * @param   string  $post       A string in urlencoded format with all variables
   * @return  mixed               The result of the request, or false if error
   * @access  protected
   */
  protected function sendHttpRequest($url, $use_auth = false, $post = null)
  {
    // new request, reset error
    $this->error_code = null;
    
    // Init the cURL instance
    $curl = curl_init();
    
    curl_setopt($curl, CURLOPT_URL, $url);
    curl_setopt($curl, CURLOPT_HEADER, false);
    curl_setopt($curl, CURLOPT_TIMEOUT, (int) $this->time_out);
    curl_setopt($curl, CURLOPT_FRESH_CONNECT, true);
    curl_setopt($curl, CURLOPT_RETURNTRANSFER, true);
    curl_setopt($curl, CURLOPT_USERAGENT, $this->user_agent); 
    
    // Send http post header
    if ($post !== null) {
      curl_setopt($curl, CURLOPT_POST, true);
      curl_setopt($curl, CURLOPT_POSTFIELDS, $post);
    }
    
    // Send http authentification
    if ($use_auth === true && $this->user !== null && $this->passwd !== null) {
      curl_setopt($curl, CURLOPT_USERPWD, $this->user.':'.$this->passwd);
    }
    
    // Send headers
    if (count($this->headers) > 0) {
      curl_setopt($ch, CURLOPT_HTTPHEADER, $this->headers);
    }
    
    // if you use ssl, send the options
    if ($this->ssl === true) {
      curl_setopt($curl, CURLOPT_SSL_VERIFYPEER, false);
      curl_setopt($curl, CURLOPT_SSL_VERIFYHOST, false);
    }
    
    $result = curl_exec($curl);
    
    $this->error_code = curl_getinfo($curl, CURLINFO_HTTP_CODE);
    
    if (curl_errno($curl) === 0 && $this->error_code === 200) {
      return $result;
    } else {
      $this->setError();
      return false;
    }
  }
  
  
  /**
   * Get the public timeline
   *
   * @param   int   $since    The ID of the status since you want the timeline
   * @return  mixed           JSON or XML with the status in the public timeline
   * @access  public
   */
  public function getPublicTimeline($since = null)
  {
    if ($since !== null) {
      $query = '?since='.(int)$since;
    } else {
      $query = '';
    }
    
    $url = $this->base_url.'statuses/public_timeline'.$this->type.$query;
    
    return $this->sendHttpRequest($url);
  }
  
  
  /**
   * Get your or a user's friend timeline
   *
   * @param   string  $friend   The user for which you want their friend timeline
   * @param   int     $since    The number of status you want in their friend timeline
   * @return  mixed             The timeline in the format you want (XML or JSON)
   * @access  public
   */
  public function getFriendTimeline($user = null, $since = null)
  {
    if ($since !== null) {
      $query = '?since='.(int)$since;
    } else {
      $query = '';
    }
    
    if ($user !== null) {
      $user = '/'.(string)$user;
    } else {
      $user = '';
    }
    
    $url = $this->base_url.'statuses/friends_timeline'.$user.$this->type.$query;
    
    return $this->sendHttpRequest($url, true);
  }
  
  
  /**
   * Get your or a user's timeline
   *
   * @param   string  $user     The user for which you want the timeline
   * @param   int     $count    The number of status you want in the timeline
   * @param   int     $since    The ID before you don't want the status
   * @return  mixed             The timeline in the format you want (XML or JSON)
   * @access  public
   */
  public function getUserTimeline($user = null, $count = 15, $since = null)
  {
    $query = '?count='.(int)$count;
    
    if ($since !== null) {
      $query .= '&since='.(int)$since;
    } 
    
    if ($user !== null) {
      $user = '/'.(string)$user;
    } else {
      $user = '';
    }
    
    $url = $this->base_url.'statuses/friends_timeline'.$user.$this->type.$query;
    
    return $this->sendHttpRequest($url, true);
  }
  
  
  /**
   * Get a status
   *
   * @param   int     $status_id    The ID of the status you want
   * @return  mixed                 The status wanted in the format you want (XML or JSON)
   * @access  public
   */
  public function getStatus($status_id)
  {
    $url = $this->base_url.'statuses/show/'.(int)$status_id.$this->type;
    
    return $this->sendHttpRequest($url, true);
  }
  
  
  /**
   * Add a new status
   *
   * @param   string     $status    The status to add
   * @return  mixed
   * @access  public
   */
  public function setStatus($status)
  {
    $url = $this->base_url.'statuses/update.xml';
    $arg = 'status='.urlencode($status);
    
    return $this->sendHttpRequest($url, true, $arg);
  }
  
  
  /**
   * Get your friend or a user's friends
   *
   * @param   string     $user    The name of the user for which you want the friends 
   * @return  mixed               Your or the user's friends in the format you want (XML or JSON)
   * @access  public
   */
  public function getFriends($user = null)
  {
    if ($user !== null) {
      $user = '/'.(string)$user;
    } else {
      $user = '';
    }
    
    $url = $this->base_url.'statuses/friends'.$user.$this->type;
    
    return $this->sendHttpRequest($url, true);
  }
  
  
  /**
   * Get your or a user's followers
   *
   * @param   string     $user    The name of the user for which you want the followers 
   * @return  mixed               Your or the user's follower in the format you want (XML or JSON)
   * @access  public
   */
  public function getFollowers($user = null)
  {
    
    if ($user !== null) {
      $user = '/'.(string)$user;
    } else {
      $user = '';
    }
    
    $url = $this->base_url.'statuses/followers'.$user.$this->type;
    
    return $this->sendHttpRequest($url, true);
  }
  
  
  /**
   * Get the features
   *
   * @return  mixed     The feature in the format you want (XML or JSON)
   * @access  public
   */
  public function getFeatured()
  {
    $url = $this->base_url.'statuses/featured'.$this->type;
    
    return $this->sendHttpRequest($url, false);
  }
  
  
  /**
   * Get the information about a user
   *
   * @param   string     $user    The user for which you want information
   * @return  mixed               The informations about the user selected in the format you want (XML or JSON)
   * @access  public
   */
  public function getUser($user)
  {
    $url = $this->base_url.'users/show/'.$user.$this->type;
    
    return $this->sendHttpRequest($url, true);
  }
  
  
  /**
   * Get all direct message of your account
   *
   * @param   int       $since    an id of message before you dont wan't receive your direct message
   * @return  mixed               Your direct messages in format you want (XML or JSON)
   * @access  public
   */
  public function getDirectMessages($since = null)
  {
    if ($since !== null) {
      $query = '?since'.(int)$since;
    } else {
      $query = '';
    }
    
    $url = $this->base_url.'direct_messages'.$this->type.$query;
    
    return $this->sendHttpRequest($url, true);
  }
  
  
  /**
   * Send a direct message to an other user
   *
   * @param   string      $user       The user which receive your message
   * @param   string      $message    The message to send
   * @return  mixed
   * @access  public
   */
  public function setDirectMessage($user, $message)
  {
    $url = $this->base_url.'direct_messages/new.xml';
    
    $arg = 'user='.urlencode((string)$user).'&text='.urlencode((string)$message);
    
    return $this->sendHttpRequest($url, true, $arg);
  }
}