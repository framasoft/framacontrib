<?php
require dirname(__FILE__).'/microblogging.class.php';


/**
 * The class to use the Identi.ca's API
 *
 * @author  Simon Leblanc <contact@leblanc-simon.eu>
 * @copyright Simon Leblanc 2009
 * @license http://www.opensource.org/licenses/bsd-license.php  BSD Licence
 * @package MicrobloggingLibrary
 * @version 1.0
 */
class Identica extends Microblogging
{
  public function __construct($user, $passwd)
  {
    parent::__construct();
    
    $this->user     = $user;
    $this->passwd   = $passwd;
    $this->base_url = 'https://identi.ca/api/';
    $this->ssl      = true;
  }
}