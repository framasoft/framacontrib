<?php
  /**
   * Elgg microblogging Service.
   * This plugin provides a wrapper around David Grudl's microblogging library and exposes some basic functionality.
   *
   * @package ElggSMS
   * @license http://www.gnu.org/licenses/old-licenses/gpl-2.0.html GNU Public License version 2
   * @author Curverider Ltd
   * @author Simon Leblanc <contact@leblanc-simon.eu>
   * @copyright Simon Leblanc 2009
   * @link http://elgg.com/
   */
?>
<p>
  <?php echo elgg_echo('microbloggingservice:name'); ?> <?php echo elgg_view('input/text', array('internalname' => 'params[microbloggingname]', 'value' => $vars['entity']->microbloggingname)); ?>
</p>
<p>
  <?php echo elgg_echo('microbloggingservice:pass'); ?> <?php echo elgg_view('input/password', array('internalname' => 'params[microbloggingpass]', 'value' => $vars['entity']->microbloggingpass)); ?>
</p>
<p>
  <?php echo elgg_echo('microbloggingservice:service'); ?>
  <select name="params[microbloggingservice]">
    <option value="identica" <?php if ($vars['entity']->microbloggingservice == 'identica') echo " selected=\"selected\" "; ?>><?php echo elgg_echo('Identi.ca'); ?></option>
    <option value="twitter" <?php if ($vars['entity']->microbloggingservice == 'twitter') echo " selected=\"selected\" "; ?>><?php echo elgg_echo('Twitter'); ?></option>
  </select>
</p>

<?php if (is_plugin_enabled('thewire')) { ?>
<p>
  <?php echo elgg_echo('microbloggingservice:postwire'); ?>
  <select name="params[sendtowire]">
    <option value="yes" <?php if ($vars['entity']->sendtowire == 'yes') echo " selected=\"yes\" "; ?>><?php echo elgg_echo('option:yes'); ?></option>
    <option value="no" <?php if ($vars['entity']->sendtowire != 'yes') echo " selected=\"yes\" "; ?>><?php echo elgg_echo('option:no'); ?></option>
  </select>
</p>
<?php } ?>