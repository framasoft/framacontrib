<?php
  $french = array(
                   'microbloggingservice' => 'Service de MicroBlogging',
                   'microbloggingservice:postwire' => 'En parametrant l\'option de suivi, tous les messages que vous posterez sur le fil seront envoyés sur votre compte de MicroBlogging. Souhaitez vous envoyer vos messages du fil sur votre compte de MicroBlogging ?',
                   'microbloggingservice:name' => 'Nom d\'utilisateur du service de MicroBlogging',
                   'microbloggingservice:pass' => 'Mot de passe du service de MicroBlogging',
                   'microbloggingservice:service' => 'Service à utiliser',
                  );
  
  add_translation("fr", $french);
?>