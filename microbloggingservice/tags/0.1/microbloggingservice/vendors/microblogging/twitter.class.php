<?php
require dirname(__FILE__).'/microblogging.class.php';

/**
 * The class to use the Twitter's API
 *
 * @author  Simon Leblanc <contact@leblanc-simon.eu>
 * @copyright Simon Leblanc 2009
 * @license http://www.opensource.org/licenses/bsd-license.php  BSD Licence
 * @package MicrobloggingLibrary
 * @version 1.0
 */
class Twitter extends Microblogging
{
  public function __construct($user, $passwd)
  {
    parent::__construct();
    
    $this->user     = $user;
    $this->passwd   = $passwd;
    $this->base_url = 'http://twitter.com/';
    $this->ssl      = false;
  }
}