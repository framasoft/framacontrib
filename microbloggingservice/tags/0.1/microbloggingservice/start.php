<?php
  /**
   * Elgg microblogging Service.
   * This plugin provides a wrapper around David Grudl's microblogging library and exposes some basic functionality.
   *
   * @package ElggSMS
   * @license http://www.gnu.org/licenses/old-licenses/gpl-2.0.html GNU Public License version 2
   * @author Curverider Ltd
   * @author Simon Leblanc <contact@leblanc-simon.eu>
   * @copyright Simon Leblanc 2009
   * @link http://elgg.com/
   */
  
  
  function microbloggingservice_init()
  {
    // Listen for wire create event
    register_elgg_event_handler('create','object','microbloggingservice_wire_listener');
  }
  
  
  /**
   * Post a message to a twitter feed.
   *
   * @param   string    $microbloggingname    The username for use the API of the microblogging
   * @param   string    $microbloggingpass    The password for use the API of the microblogging
   * @param   string    $microbloggingmessage The message to post in the microblogging
   * @param   string    $microbloggingservice The service to use (must be identica or twitter)
   * @return  bool                            True if ti's ok, false else
   */
  function microbloggingservice_send($microbloggingname, $microbloggingpass, $microbloggingmessage, $microbloggingservice)
  {
    global $CONFIG;
    
    if ($microbloggingservice === 'identica') { // use Identi.ca service
      require_once($CONFIG->pluginspath . "microbloggingservice/vendors/microblogging/identi.ca.class.php");
      $microblogging = new Identica($microbloggingname, $microbloggingpass);
      
    } elseif ($microbloggingservice === 'twitter') { // use Twitter service
      require_once($CONFIG->pluginspath . "microbloggingservice/vendors/microblogging/twitter.class.php");
      $microblogging = new Twitter($microbloggingname, $microbloggingpass);
      
    } else { // no service, error !
      return false;
    }
    
    return $microblogging->setStatus($microbloggingmessage);
  }
  
  
  /**
   * Listen for thewire and push messages accordingly.
   */
  function microbloggingservice_wire_listener($event, $object_type, $object)
  {
    if (($object) && ($object->subtype == get_subtype_id('object', 'thewire')) ) {
      if (get_plugin_usersetting('sendtowire', $object->owner_guid, 'microbloggingservice')=='yes') {
        $microbloggingname = get_plugin_usersetting('microbloggingname', $object->owner_guid, 'microbloggingservice');
        $microbloggingpass = get_plugin_usersetting('microbloggingpass', $object->owner_guid, 'microbloggingservice');
        $microbloggingservice = get_plugin_usersetting('microbloggingservice', $object->owner_guid, 'microbloggingservice');
        
        if (($microbloggingname) && ($microbloggingpass)) {
          microbloggingservice_send($microbloggingname, $microbloggingpass, $object->description, $microbloggingservice);
        }
      }
    }
  }
  
  
  register_elgg_event_handler('init','system','microbloggingservice_init');
?>