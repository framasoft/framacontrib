<?php
  $english = array(
                   'microbloggingservice' => 'microblogging Service',
                   'microbloggingservice:postwire' => 'By setting the following option all messages you post to The Wire will be sent to your microblogging account. Do you want to post your messages from The Wire to microblogging?',
                   'microbloggingservice:name' => 'microblogging username',
                   'microbloggingservice:pass' => 'microblogging password',
                   'microbloggingservice:service' => 'Service to use',
                  );
  
  add_translation("en",$english);
?>