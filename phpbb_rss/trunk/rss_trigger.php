<?php
/**
*
* @package phpBB3
* @version $Id: rss_trigger.php 8601 2009-05-18 15:48:19Z leviathan $
* @copyright (c) 2009 Leblanc Simon
* @license http://opensource.org/licenses/gpl-license.php GNU Public License
* @author Leblanc Simon <contact@leblanc-simon.eu>
*
*/

/*
# Table for RSS cache
CREATE TABLE IF NOT EXISTS `phpbb3_cache` (
  `id` bigint(20) NOT NULL auto_increment,
  `forum_id` bigint(20) NOT NULL,
  `topic_id` bigint(20) NOT NULL,
  `post_id` bigint(20) NOT NULL,
  `username_topic` varchar(255) NOT NULL,
  `username_post` varchar(255) NOT NULL,
  `forum_title` varchar(255) NOT NULL,
  `topic_title` varchar(255) NOT NULL,
  `post_title` varchar(255) NOT NULL,
  `post` blob NOT NULL,
  `bbcode_bitfield` varchar(255) NOT NULL,
  `bbcode_uid` varchar(8) NOT NULL,
  `date_post` bigint(20) NOT NULL,
  PRIMARY KEY  (`id`)
);

# Views use in a trigger
CREATE VIEW select_first_cache as SELECT min(id) as min_id FROM phpbb3_cache;

# Function use in a trigger
CREATE FUNCTION MUST_DELETE() RETURNS TINYINT
BEGIN
  DECLARE del TINYINT default 0;
  DECLARE nb INTEGER default 0;
  DECLARE curl CURSOR FOR SELECT count(id) as nb_item FROM phpbb3_cache;
  OPEN curl;
    FETCH curl INTO nb;
    IF nb > 50 THEN
      SET del = 1;
    END IF;
  CLOSE curl;
  RETURN del;
END

# Trigger which delete the lastest post in the table
CREATE TRIGGER del_cache_rss BEFORE INSERT ON phpbb3_posts
FOR EACH ROW
    DELETE FROM phpbb3_cache WHERE id = (SELECT min_id FROM select_first_cache) AND MUST_DELETE() = 1;

# Trigger which add a new post in the cache table
CREATE TRIGGER cache_rss AFTER INSERT ON phpbb3_posts
FOR EACH ROW
  INSERT INTO phpbb3_cache
  SET 
    forum_id = NEW.forum_id,
    topic_id = NEW.topic_id,
    post_id = NEW.post_id,
    username_topic = (SELECT username FROM phpbb3_users LEFT JOIN phpbb3_topics ON phpbb3_users.user_id = phpbb3_topics.topic_poster WHERE topic_id = NEW.topic_id),
    username_post = (SELECT username FROM phpbb3_users WHERE user_id = NEW.poster_id),
    forum_title = (SELECT forum_name FROM phpbb3_forums WHERE forum_id = NEW.forum_id),
    topic_title = (SELECT topic_title FROM phpbb3_topics WHERE topic_id = NEW.topic_id),
    post_title = NEW.post_subject,
    post = NEW.post_text,
    bbcode_bitfield = NEW.bbcode_bitfield,
    bbcode_uid = NEW.bbcode_uid,
    date_post = NEW.post_time;

*/

/**
* @ignore
*/
define('IN_PHPBB', true);
$phpbb_root_path = (defined('PHPBB_ROOT_PATH')) ? PHPBB_ROOT_PATH : './';
$phpEx = substr(strrchr(__FILE__, '.'), 1);
include($phpbb_root_path . 'common.' . $phpEx);
include($phpbb_root_path . 'includes/functions_display.' . $phpEx);
include($phpbb_root_path . 'includes/bbcode.' . $phpEx);

// Start session management
$user->session_begin();
$auth->acl($user->data);
$user->setup();
// End session management

$server_protocol = $config['server_protocol'];
$server_name = preg_replace('#^\/?(.*?)\/?$#', '\1', trim($config['server_name']));
$server_port = (isset($config['server_port']) && $config['server_port'] <> 80) ? ':' . trim($config['server_port']) : '';
$script_location = preg_replace('#^\/?(.*?)\/?$#', '\1', trim($config['script_path']));
$script_location = ($script_location == '') ? $script_location : '/' . $script_location;

// Define constant
define('FORUM_URL', $server_protocol.$server_name.$server_port.$script_location);
define('FORUM_TITLE', $config['sitename']);
define('FORUM_DESCRIPTION', $config['site_desc']);
define('LANG_FORUM', $config['default_lang']);
define('LIMIT', 50);

###############################################################################
#
#                       FUNCTIONS
#
###############################################################################
/**
 * Return the title of the post (or topic if post title is empty)
 *
 * @param   array   $row   The array which contain all data for the item
 * @return  string         The title for the post
 */
function getPostName($row)
{
  if(!empty($row['post_title'])){
    return $row['post_title'];
  } else {
    return $row['topic_title'];
  }
}


/**
 * Add the post in the RSS
 *
 * @param   array   $row   The array which contain all data for the item
 * @param   string  $rss   The RSS content
 */
function addInRss($row, &$rss)
{
  $bbcode_bitfield = '';
  $message = $row['post'];

  // Define the global bbcode bitfield, will be used to load bbcodes
  $bbcode_bitfield = $bbcode_bitfield | base64_decode($row['bbcode_bitfield']);

  // Instantiate BBCode if need be
  if ($bbcode_bitfield !== '') {
    $bbcode = new bbcode(base64_encode($bbcode_bitfield));
  }

  // Second parse bbcode here
  if ($row['bbcode_bitfield']) {
    $bbcode->bbcode_second_pass($message, $row['bbcode_uid'], $row['bbcode_bitfield']);
  }

  $message = bbcode_nl2br($message);
  $message = smiley_text($message);

  $rss .= '<item>';
  $rss .= '<title>'.getPostName($row).'</title>';
  $rss .= '<link>'.FORUM_URL.'/viewtopic.php?f='.$row['topic_id'].'&amp;p='.$row['post_id'].'#p'.$row['post_id'].'</link>';
  $rss .= '<description><![CDATA['.$message.']]></description>';
  $rss .= '<pubDate>'.date('r', $row['date_post']).'</pubDate>';
  $rss .= '<author>'.$row['username_post'].'</author>';
  $rss .= '<category>'.$row['forum_title'].'</category>';
  $rss .= '</item>';
}


/**
 * Add the header in the RSS
 *
 * @param   string  $rss   The RSS content
 */
function beginRss(&$rss)
{
  $header  = '<?xml version="1.0" encoding="utf-8"?>';
  $header .= '<rss version="2.0">';
  $header .= '<channel>';
  $header .= '<title>'.FORUM_TITLE.'</title>';
  $header .= '<description>'.FORUM_DESCRIPTION.'</description>';
  $header .= '<lastBuildDate>'.date('r').'</lastBuildDate>';
  $header .= '<language>'.LANG_FORUM.'</language>';
  $header .= '<link>'.FORUM_URL.'</link>';
  
  $rss = $header.$rss;
}


/**
 * Add the footer in the RSS
 *
 * @param   string  $rss   The RSS content
 */
function endRss(&$rss)
{
  $rss .= '</channel>';
  $rss .= '</rss>';
}

###############################################################################
#
#                       MAIN PROGRAM
#
###############################################################################
$sql = 'SELECT id, forum_id, topic_id, post_id, username_topic, username_post, forum_title, topic_title, post_title, post, bbcode_uid, bbcode_bitfield, date_post
        FROM phpbb3_cache
        ORDER BY date_post DESC
        LIMIT 0, '.LIMIT;

if(!($result = $db->sql_query($sql))){
  trigger_error('General Error : Could not obtain rss informations -> '.$sql);
}

$nb_topic = 0;
$rss      = '';

while($row = $db->sql_fetchrow($result)){
  // Check if user have enough right for read the forum
  if($auth->acl_get('f_read', $row['forum_id']) !== 0){
    addInRss($row, $rss);
  }
}

// Write the top of rss
beginRss($rss);

// Write the bottom of rss
endRss($rss);

// Send header
header('Content-Type: application/rss+xml; charset=UTF-8');

// Send RSS content
echo $rss;
