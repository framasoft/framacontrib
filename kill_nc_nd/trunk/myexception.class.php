<?php
/**
 * Class gerant les exceptions
 * 
 * @author Leblanc Simon <contact@leblanc-simon.eu>
 * @license http://www.opensource.org/licenses/bsd-license.php  BSD Licence
 * @version 1.0.0
 */
class MyException  extends Exception {
    public function __construct($msg)
    {
        parent::__construct($msg);
    }
    
    public function getError()
    {
        $info = $this->getTrace();
        
        $msg = "<h1>Error</h1>";
        $msg .= "Date: ".date("Y-m-d H:m:s")."<br />";
        $msg .= "<b>Exception: ".$this->getMessage()."</b><br />";
        $msg .= "URL: ".$_SERVER["REQUEST_URI"]."<br />";
        $msg .= "File: ".$info[0]["file"]."<br />";
        $msg .= "URL: ".$_SERVER["REQUEST_URI"]."<br />";
        $msg .= "Line: ".$info[0]["line"]."<br />";
        $msg .= "Function: ".((empty($info[0]["class"])) ? "" : $info[0]["class"]."::").$info[0]["function"]."<br />";
        if (count($info[0]["args"]) > 0) {
            $msg .= "Arguments:<br />";
            foreach ($info[0]["args"] as $arg) {
                $msg .= "- ".$arg."<br />";
            }
        }
        
        return $msg;
    }
}
?>
