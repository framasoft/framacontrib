<?php
/**
 * Classe permettant de verifier la licence d'une photo Flickr
 *
 * @author Leblanc Simon <contact@leblanc-simon.eu>
 * @license http://www.opensource.org/licenses/bsd-license.php  BSD Licence
 * @version 1.0.0.0
 */

require_once "myexception.class.php";

class LS_Flickr
{
    private $_idarticle;
    private $_url;
    
    private $_config;
    private $_conn;
    
    //Constructeur
    public function __construct()
    {
        $this->_conn = false;
        $this->_init();
        $this->_initConfig();
        $this->_checkConfig();
    }
    
    //Destructeur
    public function __destruct()
    {
        if ($this->_conn) {
            mysql_close($this->_conn);
        }
    }
    
    /**
     * Fonction permettant de tester les licences
     *
     * @param    Int        $id        L'identifiant de l'article a tester
     * @return    Int                0: Licence non libre, 1: Licence libre, -1: rien a tester (URL n'existe pas)
     */
    public function checkLicence($id)
    {
        $this->_init($id);
        
        $url = $this->_readData();
        if ($url === false) {
            return -1;
        }
        
        $licence = $this->_checkURL();
        if ($licence == "") {
            return -1;
        }
        
        foreach ($this->_config["free"] as $free) {
            if (strpos($licence, $free) !== false) {
                return 1;
            }
        }
        
        return 0;
    }
    
    /**
     * Fonction permettant d'initialiser l'identifiant et l'url
     *
     * @param    Int        $id        L'identifiant de l'article a tester
     */
    private function _init($id = 0)
    {
        if (!is_numeric($id)) {
            throw new MyException("The id must be numeric");
        }
        
        if ($id == 0) {
            $this->_idarticle     = 0;
            $this->_url            = "";
        } else {
            $this->_idarticle     = $id;
            $this->_url            = "";
        }
    }
    
    /**
     * Fonction permettant d'initialiser les parametres de configuration
     */
    private function _initConfig()
    {
        $this->_config["server"]    = "localhost";
        $this->_config["login"]        = "root";
        $this->_config["password"]    = "";
        $this->_config["database"]    = "spip";
        
        $this->_config["url_field"]    = "url_site";
        $this->_config["id_field"]    = "id_article";
        $this->_config["table"]        = "spip_articles";
        
        $this->_config["begin"]        = "<p class=\"Privacy\">";
        $this->_config["end"]        = "</p>";
        
        $this->_config["free"]        = array("http://creativecommons.org/licenses/by/", "http://creativecommons.org/licenses/by-sa/");
    }
    
    /**
     * Fonction permettant de verifier que la configuration est correcte (les champs requis sont remplis)
     */
    private function _checkConfig()
    {
        if (empty($this->_config["server"]) || empty($this->_config["login"]) || empty($this->_config["database"])
            || empty($this->_config["url_field"]) || empty($this->_config["id_field"]) || empty($this->_config["table"])) 
        {
            throw new MyException("Error in the configuration file, check this!");
        }
    }
    
    /**
     * Fonction permettant de se connecter a la base de donnees
     */
    private function _connect_db()
    {
        $this->_checkConfig();
        
        if (!$this->_conn) {
            $this->_conn = mysql_connect($this->_config["server"], $this->_config["login"], $this->_config["password"]);
            if ($this->_conn === false) {
                throw new MyException("Error in the connect MySQL server: '".mysql_error()."'");
            }
            
            $res = mysql_select_db($this->_config["database"], $this->_conn);
            if ($res === false) {
                throw new MyException("Error in the connect database '".$this->_config["database"]."': '".mysql_error($this->_conn)."'");
            }
        }
    }
    
    /**
     * Fonction permettant de recuperer l'url a tester
     */
    private function _readData()
    {
        $this->_connect_db();
        
        $sql = "SELECT ".$this->_config["url_field"]." 
                FROM ".$this->_config["table"]." 
                WHERE ".$this->_config["id_field"]."='".mysql_real_escape_string($this->_idarticle, $this->_conn)."'";
        $res = mysql_query($sql);
        if ($res === false) {
            throw new MyException("Error in the SQL request : '".$sql."'. Error : '".mysql_error($this->_conn)."'");
        }
        
        $row = mysql_fetch_assoc($res);
        if ($row === false) {
            return false;
        } else {
            $this->_url = $row[$this->_config["url_field"]];
            if ($this->_url == "") {
                return false;
            }
        }
        
        return true;
    }
    
    /**
     * Fonction recuperant le code source de la page a tester et renvoyant la partie concernant la licence
     *
     * @return    Str                    Le bout de code source ou est indique la licence
     */
    private function _checkURL()
    {
        if (empty($this->_url)) {
            throw new MyException("The URL can't be empty");
        }
        
        //on recupere le code source de la page
        $str = file_get_contents($this->_url);
        if ($str === false) {
            return false;
        }
        
        $licence = $this->_recupLicence($str);
        
        return $licence;
    }
    
    /**
     * Fonction permettant de recuperer la partie du code contenant la licence
     *
     * @param    Str        $str        Le code source de la page a tester
     * @return    Str                    Le bout de code source ou est indique la licence
     */
    private function _recupLicence($str)
    {
        //on supprime les retour a la ligne pour pouvoir etudier la page
        $str = str_replace("\r\n", "", $str);
        $str = str_replace("\r", "", $str);
        $str = str_replace("\n", "", $str);
        
        $pattern = "#".$this->_transformePattern($this->_config["begin"])."(.*)".$this->_transformePattern($this->_config["end"])."#Usi";
        
        preg_match($pattern, $str, $matches);
        if (count($matches) > 1) {
            return $matches[1];
        } else {
            return "";
        }
    }
    
    /**
     * Fonction permettant de mettre en forme un pattern
     *
     * @param    Str        $str        Le pattern a mettre en forme
     * @return    Str                    Le pattern mis en forme
     */
    private function _transformePattern($str) 
    {
        $str = str_replace("?", "\?", $str);
        $str = str_replace("^", "\^", $str);
        $str = str_replace("$", "\$", $str);
        $str = str_replace(".", "\.", $str);
        $str = str_replace("*", "\*", $str);
        $str = str_replace("+", "\+", $str);
        $str = str_replace("[", "\[", $str);
        $str = str_replace("]", "\]", $str);
        $str = str_replace("{", "\{", $str);
        $str = str_replace("}", "\}", $str);
        $str = str_replace("(", "\(", $str);
        $str = str_replace(")", "\)", $str);
        $str = str_replace("#", "\#", $str);
        
        return $str;
    }
}


##################################
# Fonction de connexion
function connect($server, $login, $password, $database)
{
    $conn = mysql_connect($server, $login, $password);
    if ($conn === false) {
        throw new MyException("Error in the connect MySQL server: '".mysql_error()."'");
    }
            
    $res = mysql_select_db($database, $conn);
    if ($res === false) {
        throw new MyException("Error in the connect database '".$database."': '".mysql_error($conn)."'");
    }
}


#############################################################################
#############################################################################
#                                                                            #
#                        Programme principal                                    #
#                                                                            #
#############################################################################
#############################################################################
try {
    //connexion à la base
    connect("localhost", "root", "", "spip");
    
    //on selectionne tous les articles de la rubrique Flickr qui sont publie
    $sql = "SELECT id_article FROM spip_articles WHERE id_rubrique=408 AND statut='publie' ORDER BY id_article LIMIT 0, 30";
    $res = mysql_query($sql);
    if (!$res) {
        throw new MyException("erreur requete : ".mysql_error());
    }
    
    //creation de la classe
    $flickr = new LS_Flickr();
    
    //pour chaque image on teste sa licence
    while ($row = mysql_fetch_assoc($res)) {
        $resultat = $flickr->checkLicence($row["id_article"]);
        switch ($resultat) {
            case -1:
                echo "<a href=\"http://www.framasoft.net/article".$row["id_article"].".html\">".$row["id_article"]."</a> n'a pas pu etre tester<br />";
                break;
            case 0:
                echo "<a href=\"http://www.framasoft.net/article".$row["id_article"].".html\">".$row["id_article"]."</a> n'est pas libre<br />";
                break;
            case 1:
                echo "<a href=\"http://www.framasoft.net/article".$row["id_article"].".html\">".$row["id_article"]."</a> est libre<br />";
                break;
        }
    }

    unset($flickr);
    
} catch (MyException $e) {
    echo $e->getError();
}

?> 
