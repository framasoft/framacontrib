<?php
/**
 * This file contains the captcha class and theirs globales variables
 *
 * @author Leblanc Simon <contact@leblanc-simon.eu>
 * @license http://www.opensource.org/licenses/bsd-license.php  BSD Licence
 * @version 1.0.2
 */
session_start();

/**
 * the variable which indicate that the class file is load
 * @name FS_CAPTCHA_CORE
 */
define("FS_CAPTCHA_CORE", true);

/**
 * If error, redirect the user?
 * @name FS_REDIRECT
 */
define("FS_REDIRECT", false);

/**
 * the web page to redirect the bot
 * @name FS_REDIRECT_HTML
 */
define("FS_REDIRECT_HTML", "/echec_captcha.html");

/**
 * use the blacklist
 * @name FS_BLACKLIST
 */
define("FS_BLACKLIST", false);

/**
 * time (in minute) of an ip is blacklisted
 * @name FS_BLACKLIST_TIME
 */
define("FS_BLACKLIST_TIME", 15);

/**
 * number of test before being blacklisted
 * @name FS_BLACKLIST_NULL
 */
define("FS_BLACKLIST_NULL", 10);

/**
 * the name of file which contain the list of blacklist
 * @name FS_BLACKLIST_FILE
 */
define("FS_BLACKLIST_FILE", _DIR_PLUGINS."/FS_captcha/captcha/FS_blacklist.txt");

/**
 * the name of file which contain the list of blacklist
 * @name FS_BLACKLIST_FILE
 */
define("FS_BLACKLIST_FILE_TOTAL", _DIR_PLUGINS."/FS_captcha/captcha/FS_blacklist_total.txt");


/**
 * The check of answer must be sensitive in case or not?
 * @name FS_SENSITIVE_CASE
 */
define("FS_SENSITIVE_CASE", false);

/**
 * write a log file
 * @name FS_LOG
 */
define("FS_LOG", false);

/**
 * The name of file which contain the log
 * @name FS_LOG_FILE
 */
define("FS_LOG_FILE", _DIR_PLUGINS."/FS_captcha/captcha/blacklist.log");


/**
 * This class contains the ensemble of function for create and check an accessible captcha
 *
 * @access  public
 */
class FS_captcha
{
    /**
     * @var     Array   _questions_list The array which contained all question and answer
     * @access  private
     */
    var $_questions_list;
    var $_bad_words = "/(\s(watch|watches|replica|jewelry)\s)/i";
    
    /**
     * constructor of the class
     *
     * @access  public
     */
    function FS_captcha()
    {
        $this->_questions_list = array();
        
        include_once _DIR_PLUGINS."/FS_captcha/captcha/question_list.php";
    }
    
    
    /**
     * create the new captcha
     *
     * @return  String          The question for the captcha
     * @access  public
     */
    function generate()
    {
        $num_question = array_rand($this->_questions_list);        
        $_SESSION["captcha_num"] = $num_question;        
        $return_value = $this->_questions_list[$num_question]["question"];
        
        return $return_value;
    }
    
    
    /**
     * check if the answer is good or not
     *
     * @param   String  $answer The user's answer in the question
     * @return  Boolean         TRUE if the answer is good and the user isn't blacklisted, false else
     * @access  public
     */
    function check($answer)
    {
        $ip = $_SERVER["REMOTE_ADDR"];
        $num_question = $_SESSION["captcha_num"];
        
        //check if the user-actor is blacklisted (user-actor is a user which participated in full wiki :-))))
        if ($this->_is_blacklist($ip)) {
            $this->_blacklist($ip, $this->_questions_list[$num_question]["question"], $answer);
            if (FS_REDIRECT) {
                header("Location: ".FS_REDIRECT_HTML);
            }
            return false;
        }
        
        //check if the captcha num is informed
        if (empty($_SESSION["captcha_num"]) && $_SESSION["captcha_num"] != 0) {
            $this->_blacklist($ip, $this->_questions_list[$num_question]["question"], $answer);
            if (FS_REDIRECT) {
                header("Location: ".FS_REDIRECT_HTML);
            }
            return false;
        }
        
        //check if the answer isn't null
        if ($answer == "") {
            $this->_blacklist($ip, $this->_questions_list[$num_question]["question"], $answer);
            if (FS_REDIRECT) {
                header("Location: ".FS_REDIRECT_HTML);
            }
            return false;
        }
        
        //recover the captcha num and the good answer
        $good_answer  = $this->_questions_list[$num_question]["answer"];
        
        //sensitive case
        if (!FS_SENSITIVE_CASE) {
            $good_answer = strtolower($good_answer);
            $answer      = strtolower($answer);
        }
        
        //clean the string
        $good_answer = trim($good_answer);
        $answer      = trim($answer);
        
        //check if the user's answer is good
        if (($answer == $good_answer) && (preg_match($this->_bad_words, $_POST["texte"])==0)) {
            return true;
        } else {
            $this->_blacklist($ip, $this->_questions_list[$num_question]["question"], $answer);
            if (FS_REDIRECT) {
                header("Location: ".FS_REDIRECT_HTML);
            }
            return false;
        }
    }
    
    
    /**
     * Add one question in the list
     *
     * @param   String  $question   The question to add in the captcha
     * @param   String  $answer     The answer of the question
     * @access  public
     */
    function add_question($question, $answer)
    {
        $this->_questions_list[] = array("question"  => $question,
                                         "answer"    => $answer);
    }


    /**
     * check if the user is blacklisted
     *
     * @param   String  $ip     The IP address of the user
     * @return  Boolean         TRUE if the user is blacklisted, FALSE else
     * @access  private
     */
    function _is_blacklist($ip)
    {
        
        // Patch pour un blacklistage total
        $file = FS_BLACKLIST_FILE_TOTAL;
        if (filesize($file) > 0) {
		$handle = fopen($file, "r");
		if ($handle) {
			$log = fread($handle, filesize ($file));

			//check the ip
			$result = substr_count($log, $ip."\n");
			if ($result > 0) {
				return true;
			}
		} 
        }
        // Fin du patch pour un blacklistage total
	
		if (FS_BLACKLIST) {
            $this->_unblacklist();
            
            //open the file
            $file = FS_BLACKLIST_FILE;
            if (filesize($file) > 0) {
                $handle = fopen($file, "r");
                if (!$handle) {
                    return false;
                }
                $log = fread($handle, filesize ($file));

                //check the ip
                $result = substr_count($log, $ip."\n");
                if ($result < FS_BLACKLIST_NULL) {
                    return false;
                } else {
                    return true;
                }
            } else {
                return false;
            }
        } else {
            return false;
        }
    }    
    
    /**
     * blacklist the user
     *
     * @param   String  $ip         The IP address of the user
     * @param   String  $question   The question of the captcha
     * @param   String  $answer     The answer of the user
     * @return  Boolean             TRUE if the user is blacklisted, FALSE else
     * @access  private
     */
    function _blacklist($ip, $question, $answer)
    {
        if (FS_LOG) {
            $this->_log($ip, $question, $answer);
        }
        if (FS_BLACKLIST) {
            $log = date('YmdHi')." ".$ip."\n";
            
            $file = FS_BLACKLIST_FILE;
            $handle = fopen($file, "a+");
            if (!$handle) {
                return false;
            }
            $result = fwrite($handle, $log);
            if ($result === false) {
                return false;
            }
            fclose($handle);
            
            return true;
        } else {
            return true;
        }
    }
    
    
    /**
     * _unblacklist the users (is function of the time)
     *
     * @return  Boolean         TRUE if the users is unlacklisted, FALSE else
     * @access  private
     */
    function _unblacklist()
    {
        if (FS_BLACKLIST) {
            $end = date('YmdHi', time() - FS_BLACKLIST_TIME * 60);
            
            //open the file
            $file = FS_BLACKLIST_FILE;
            if (filesize ($file) > 0) {
                $handle = fopen($file, "r");
                if (!$handle) {
                    return false;
                }
                $log = fread($handle, filesize ($file));
                if ($log === false) {
                    echo "pas lu";
                }
                
                //extract each IP in a table
                $lines = explode("\n", $log);
                $new_log = "";
                
                //check if the time is expirated
                foreach($lines as $line) {
                    $details = explode(" ", $line);
                    if ($details[0] > $end) {
                        $new_log .= $line."\n";
                    }
                }
                fclose($handle);
                
                //write the new file
                $handle = fopen($file, "w+");
                if (!$handle) {
                    return false;
                }
                
                $result = fwrite($handle, $new_log);
                if ($result === false) {
                    return false;
                }
                fclose($handle);
            }
            
            return true;
        } else {
            return true;
        }
    }
    
    
    /**
     * log the bad answer
     *
     * @param   String  $ip         The IP address of the user
     * @param   String  $question   The question of the captcha
     * @param   String  $answer     The answer of the user
     * @return  Boolean             TRUE if the log is writed, FALSE else
     * @access  private
     */
    function _log($ip, $question, $answer)
    {
        $log = date('YmdHi')." ".$ip." / ".gethostbyaddr($ip)." | ".$question." -- ".$answer." -- \n";
            
        $file = FS_LOG_FILE;
        $handle = fopen($file, "a+");
        if (!$handle) {
            return false;
        }
        $result = fwrite($handle, $log);
        if ($result === false) {
                return false;
        }
        fclose($handle);
            
        return true;
    }
}
?>