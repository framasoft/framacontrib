<?php
/**
 * this file contains the list of the questions and the answers
 *
 * @author Leblanc Simon <contact@leblanc-simon.eu>
 * @license http://www.opensource.org/licenses/bsd-license.php  BSD Licence
 * @version 1.0.2
 */
 
if (defined("FS_CAPTCHA_CORE")) {
    for ($i=0; $i < 10; $i++) {
        if ($i % 3 == 0) {
            $j = $i + 2;
            $this->add_question("Que donne la somme ".$i." + ".$j." ? (chiffres)", $i + $j);
        } else {
            $j = $i + 1;
            $this->add_question("Que donne la somme ".$i." + ".$j." ? (chiffres)", $i + $j);
        }
    }
} else {
    exit();
}
?>