#!/bin/bash

##########################################################################################
#Copyright (c) 2009, Leblanc Simon <contact@leblanc-simon.eu>
#
#All rights reserved.
#
#Redistribution and use in source and binary forms, with or without modification, 
#are permitted provided that the following conditions are met:
#
#    * Redistributions of source code must retain the above copyright notice, 
#      this list of conditions and the following disclaimer.
#    * Redistributions in binary form must reproduce the above copyright notice, 
#      this list of conditions and the following disclaimer in the documentation 
#      and/or other materials provided with the distribution.
#    * Neither the name of the Leblanc Simon nor the names of its contributors 
#      may be used to endorse or promote products derived from this software without
#      specific prior written permission.
#
#THIS SOFTWARE IS PROVIDED BY THE COPYRIGHT HOLDERS AND CONTRIBUTORS
#"AS IS" AND ANY EXPRESS OR IMPLIED WARRANTIES, INCLUDING, BUT NOT
#LIMITED TO, THE IMPLIED WARRANTIES OF MERCHANTABILITY AND FITNESS FOR
#A PARTICULAR PURPOSE ARE DISCLAIMED. IN NO EVENT SHALL THE COPYRIGHT OWNER OR
#CONTRIBUTORS BE LIABLE FOR ANY DIRECT, INDIRECT, INCIDENTAL, SPECIAL,
#EXEMPLARY, OR CONSEQUENTIAL DAMAGES (INCLUDING, BUT NOT LIMITED TO,
#PROCUREMENT OF SUBSTITUTE GOODS OR SERVICES; LOSS OF USE, DATA, OR
#PROFITS; OR BUSINESS INTERRUPTION) HOWEVER CAUSED AND ON ANY THEORY OF
#LIABILITY, WHETHER IN CONTRACT, STRICT LIABILITY, OR TORT (INCLUDING
#NEGLIGENCE OR OTHERWISE) ARISING IN ANY WAY OUT OF THE USE OF THIS
#SOFTWARE, EVEN IF ADVISED OF THE POSSIBILITY OF SUCH DAMAGE.
###########################################################################################

TODAY=`date +"%y%m%d"`

###################################################
#
#                  PARAMETERS
#
###################################################
# General Parameters
LOG_FILE="/var/log/backup_${TODAY}.log"
MAIL="technique@example.com"
SUBJECT_ERROR="[LOG] [serveur1] Erreur lors de la sauvegarde du serveur"
SUBJECT_OK="[LOG] [serveur1] Sauvegarde du serveur réalisée"

# MySQL Parameters 
MYSQLDUMP_BIN="/usr/bin/mysqldump"
MYSQL_LOGIN="root"
MYSQL_PASS="password"
DUMP_FILE="/home/mysql/dump_all_databases.sql.gz"

# RSYNC Parameters 
RSYNC_BIN="/usr/bin/rsync"
INCLUDE="/root/backup/include_rsync"
EXCLUDE="/root/backup/exclude_rsync"
SERVER="backup.server:/home/backup/server1"


###################################################
#
#                MAIN PROGRAM
#
###################################################

############
# MYSQL
#
# option opt équivalente à add-drop-table, add-locks, create-options, disable-keys, extended-insert, lock-tables, quick, set-charset

${MYSQLDUMP_BIN} -u ${MYSQL_LOGIN} -p${MYSQL_PASS} --opt --all-databases | gzip > ${DUMP_FILE}


############
# RSYNC
#
# option a equivalente à rlptgoD
# r : recursive
# l : conserve les liens symboliques en liens symbolique
# p : préserve les permissions
# t : préserve les dates
# g : préserve le groupe
# o : préserve l'utilisateur
# D : préserve les périphérique
# option v : plus loquace
# option n : montre ce qui aurait été transféré
# option z : compresse les fichiers pour le transfert

# On vérifie que le fichier d'incluson existe bien
if [ "${INCLUDE}" = "" ] ; then
  echo "Le fichier d'inclusion doit être renseigné!!!"
  exit 1
else
  if [ ! -f "${INCLUDE}" ] ; then
    echo "Le fichier d'inclusion '${INCLUDE}' n'existe pas!!!"
    exit 2
  fi
fi


# On vérifie que le fichier d'exclusion existe bien
if [ "${EXCLUDE}" != "" ] ; then
  if [ ! -f "${EXCLUDE}" ] ; then
    echo "Le fichier d'exclusion '${EXCLUDE}' n'existe pas!!!"
    exit 3
  fi
fi


# On vérifie le serveur, répertoire et l'utilisateur (aucun ne peut être vide)
if [ "${SERVER}" = "" ] ; then
  echo "L'adresse du serveur de backup doit être renseigné!!!"
  exit 4
fi

# Execute-t-on le script en test ?
if [ $# -eq 1 ] && [ "$1" = "debug" ] ; then
  # S'il n'y a pas de fichier d'exclusion, on n'indique pas d'exclusion : logique :-)
  if [ "${EXCLUDE}" = "" ] ; then
    ${RSYNC_BIN} -avrzn --files-from=${INCLUDE} --delete / ${SERVER}
  else
    ${RSYNC_BIN} -avrzn --files-from=${INCLUDE} --exclude-from=${EXCLUDE} --delete / ${SERVER}
  fi
else
  # S'il n'y a pas de fichier d'exclusion, on n'indique pas d'exclusion : logique :-)
  if [ "${EXCLUDE}" = "" ] ; then
    ${RSYNC_BIN} -avrz --files-from=${INCLUDE} --delete / ${SERVER} 2>&1 >> ${LOG_FILE}
  else
    ${RSYNC_BIN} -avrz --files-from=${INCLUDE} --exclude-from=${EXCLUDE} --delete / ${SERVER} 2>&1 >> ${LOG_FILE}
  fi

  # On vérifie que le script n'a connu aucune erreur
  if [ $? -ne 0 ]; then
    cat ${LOG_FILE} | mail -s "${SUBJECT_ERROR}" ${MAIL} -
  else
    cat ${LOG_FILE} | mail -s "${SUBJECT_OK}" ${MAIL} -
  fi

  # On supprime le fichier de log
  rm -f ${LOG_FILE}
fi
