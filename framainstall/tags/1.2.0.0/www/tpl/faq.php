<?php include TPL_FOLDER.'/header.php'; ?>

    <div id="header">
      <div class="bloc border">
        <div class="logo"><a href="http://www.framapack.org" title="Acc&eacute;der &agrave; l'installeur de logiciels libres : Framapack"><img src="images/logo_pack2.png" alt="Logo de Framapack" /></a></div>
        <div class="description">
          <ul>
            <li>1. S&eacute;lectionnez les applications.</li>
            <li>2. T&eacute;l&eacute;charger Framapack.</li>
            <li>3. Installer vos applications.</li>
          </ul>
        </div>
      </div>
      <div class="network border">
        <p>Notre r&eacute;seau :</p>
        <ul>
          <li><a href="http://fr.wikipedia.org/wiki/Framasoft" title="Acc&eacute;der &agrave; l'article Wikipedia propos de Framasoft">Framapedia</a></li>
          <li><a href="http://www.framasoft.org" title="Acc&eacute;der &agrave; l'annuaire de logiciels libres de Framasoft">Framasoft</a></li>
          <li><a href="http://www.framakey.org" title="Acc&eacute;der au site des logiciels portables : Framakey">Framakey</a></li>
          <li><a href="http://www.framabook.org" title="Acc&eacute;der au site des livres libres Framabook">Framabook</a></li>
          <li><a href="http://www.framablog.org/index.php/pages/framatube" title="Acc&eacute;der au site Framatube">Framatube</a></li>
          <li><a href="http://www.framablog.org" title="Acc&eacute;der au blog de Framasoft">Framablog</a></li>
          <li><a href="http://forum.framasoft.org" title="Acc&eacute;der aux forums de Framasoft">Framagora</a></li>
          <li><a href="http://www.framapack.org" title="Acc&eacute;der &agrave; l'installeur de logiciels libres : Framapack">Framapack</a></li>
        </ul>
      </div>
    </div>
    <div id="content" class="border">
      <ul class="list-faq">
        <li><a href="#qu_est_que_framapack">Qu'est ce que Framapack ?</a></li>
        <li><a href="#qui_propose_framapack">Qui propose Framapack ?</a></li>
        <li><a href="#pourquoi_mon_logiciel_favori_n_est_pas_disponible">Pourquoi mon logiciel favori n'est pas disponible ?</a></li>
        <li><a href="#j_ai_une_idee_pour_ameliorer_framapack">J'ai une id&eacute;e pour am&eacute;liorer Framapack...</a></li>
        <li><a href="#je_souhaite_faire_une_version_personnalise_de_framapack">Je souhaite faire une version personnalis&eacute; de Framapack</a></li>
        <li><a href="#comment_configurer_framapack_pour_activer_les_options">Comment configurer Framapack pour activer les options (cache, proxy) ?</a></li>
        <li><a href="#les_logiciels_installes_ont_il_ete_modifies">Les logiciels install&eacute;s ont-ils &eacute;t&eacute; modifi&eacute;s ?</a></li>
      </ul>
      
      <div class="question"><a name="qu_est_que_framapack"></a>
        <h2>Qu'est ce que Framapack ?</h2>
        <p>Framapack vous permet d'installer automatiquement sur votre ordinateur toute une collection de <a href="http://www.framasoft.net/article3338.html" title="Voir un article d&eacute;crivant les logiciels libres">logiciels libres</a> (pour Windows) de votre choix.</p>
        <p>Faites votre choix parmi les applications propos&eacute;es ici, t&eacute;l&eacute;chargez l'installeur Framapack en cliquant sur la fl&egrave;che verte, puis ex&eacute;cutez le programme afin d'installer les derni&egrave;res versions des logiciels libres que vous avez choisi.</p>
        <p>Framapack t&eacute;l&eacute;chargera et installera automatiquement les applications avec les options par d&eacute;faut.</p>
      </div>
      
      <div class="question"><a name="qui_propose_framapack"></a>
        <h2>Qui propose Framapack ?</h2>
        <p>Framapack a &eacute;t&eacute; d&eacute;velopp&eacute; par le r&eacute;seau <a href="http://www.framasoft.net" title="Acc&eacute;der au site de Framasoft">Framasoft</a>. Nous avons &eacute;galement fait le choix des applications propos&eacute;es ici.</p>
        <p>Si vous le souhaitez vous pouvez nous soumettre des id&eacute;es d'am&eacute;lioration &agrave; l'adresse contact _AT_ framapack.org.</p>
      </div>
      
      <div class="question"><a name="pourquoi_mon_logiciel_favori_n_est_pas_disponible"></a>
        <h2>Pourquoi mon logiciel favori n'est pas disponible ?</h2>
        <p>Framapack ne propose que des logiciels libres, peut-être ne l'est-il pas, dans ce cas nous ne proposerons pas ce logiciel.</p>
        <p>C'est un logiciel libre, dans ce cas, prenez contact avec nous afin que l'on puisse &eacute;valuer la pertinence de le proposer ici.</p>
      </div>
      
      <div class="question"><a name="j_ai_une_idee_pour_ameliorer_framapack"></a>
        <h2>J'ai une id&eacute;e pour am&eacute;liorer Framapack...</h2>
        <p>Framapack est un logiciel libre, tout le monde peut participer que ce soit en terme d'id&eacute;e ou de code.</p>
        <p>Pour cela, rien de plus simple, prenez contact avec l'&eacute;quipe du projet, ils vous indiqueront la marche &agrave; suivre.</p>
      </div>
      
      <div class="question"><a name="je_souhaite_faire_une_version_personnalise_de_framapack"></a>
        <h2>Je souhaite faire une version personnalis&eacute; de Framapack</h2>
        <p>Aucun probl&egrave;me. Framapack est un logiciel libre distribu&eacute; sous licence <a href="http://www.gnu.org/licenses/agpl-3.0.html" title="Voir le texte de la licence GNU/AGPL version 3">GNU/AGPL version 3</a> pour le site Internet et <a href="http://www.gnu.org/licenses/gpl-2.0.html" title="Voir le texte de la licence GNU/GPL version 2">GNU/GPL version 2</a> pour le programme.</p>
        <p>Vous pouvez r&eacute;cup&eacute;rer le code source du site Internet et du programme sur <a href="http://framacontrib.svn.sourceforge.net/viewvc/framacontrib/framainstall/trunk/" title="Acc&eacute;der au d&eacute;p&ocirc;t subversion du projet Framapack">Sourceforge</a>.</p>
        <p>Suivez les instructions du fichier INSTALL afin d'installer Framapack sur votre serveur.</p>
      </div>
      
      <div class="question"><a name="comment_configurer_framapack_pour_activer_les_options"></a>
        <h2>Comment configurer Framapack pour activer les options (cache, proxy) ?</h2>
        <p>Il suffit d'ajouter un fichier nomm&eacute; framapack.ini dans le m&ecirc;me r&eacute;pertoire que celui contenant l'ex&eacute;cutable framapack.exe que vous avez t&eacute;l&eacute;charg&eacute;.</p>
        <p>Vous pouvez t&eacute;l&eacute;charger un exemple contenant l'ensemble des options <a href="framapack.ini" title="T&eacute;l&eacute;charger le fichier framapack.ini exemple">ici.</a></p>
        <p>Voici les différentes options que vous pouvez ajouter via le fichier framapack.ini :</p>
        <h3>Le syst&egrave;me de proxy :</h3>
        <pre>
[proxy]
server=l'adresse de votre proxy
port=le num&eacute;ro du port de votre proxy (si manquant 1080)
username=votre nom d'utilisateur
password=votre mot de passe
        </pre>
        <h3>Le syst&egrave;me de cache :</h3>
        <pre>
[options]
cache=1
# 1 pour activer le cache, cela cr&eacute;era un r&eacute;pertoire framapack
# dans le r&eacute;pertoire de framapack.exe s'il n'existe pas d&eacute;j&agrave;, il
# stockera les fichiers d'installation &agrave; l'int&eacute;rieur
# 0 pour ne pas activer le cache (c'est la valeur par d&eacute;faut
# si le ficher ini n'existe pas ou si la valeur n'existe pas
# dans le fichier ini, alors il n'y aura pas d'utilisation du cache)

install=0
# 0 pour indiquer de ne pas installer le logiciel apr&egrave;s le t&eacute;l&eacute;chargement
# cela n'est utile que si cache=1 afin d'installer les applications
# plus tard ou sur un autre PC
# 1 pour indiquer d'installer le logiciel (c'est la valeur par d&eacute;faut
# si le ficher ini n'existe pas ou si la valeur n'existe pas
# dans le fichier ini, alors il n'y aura pas d'utilisation du cache)
        </pre>
      </div>
      
      <div class="question"><a name="les_logiciels_installes_ont_il_ete_modifies"></a>
        <h2>Les logiciels install&eacute;s ont-ils &eacute;t&eacute; modifi&eacute;s ?</h2>
        <p>Non ! Chaque logiciel est t&eacute;l&eacute;charg&eacute; &agrave; partir du site officiel de l'application ce qui permet de n'avoir que les derni&egrave;res versions officielles.</p>
        <p>Une exception &agrave; ce principe : CDex. L'installeur de ce logiciel n'&eacute;tant pas silencieux, nous avons refait l'installeur (et uniquement l'installeur). Le logiciel est rigoureusement identique &agrave; la version officielle.</p>
      </div>
    </div>

<?php include TPL_FOLDER.'/footer.php'; ?>