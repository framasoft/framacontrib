<?php
##########################################################################################
#Copyright (c) 2009, Leblanc Simon <contact@leblanc-simon.eu>
#
#All rights reserved.
#
#Redistribution and use in source and binary forms, with or without modification, 
#are permitted provided that the following conditions are met:
#
#    * Redistributions of source code must retain the above copyright notice, 
#      this list of conditions and the following disclaimer.
#    * Redistributions in binary form must reproduce the above copyright notice, 
#      this list of conditions and the following disclaimer in the documentation 
#      and/or other materials provided with the distribution.
#    * Neither the name of the Leblanc Simon nor the names of its contributors 
#      may be used to endorse or promote products derived from this software without
#      specific prior written permission.
#
#THIS SOFTWARE IS PROVIDED BY THE COPYRIGHT HOLDERS AND CONTRIBUTORS
#"AS IS" AND ANY EXPRESS OR IMPLIED WARRANTIES, INCLUDING, BUT NOT
#LIMITED TO, THE IMPLIED WARRANTIES OF MERCHANTABILITY AND FITNESS FOR
#A PARTICULAR PURPOSE ARE DISCLAIMED. IN NO EVENT SHALL THE COPYRIGHT OWNER OR
#CONTRIBUTORS BE LIABLE FOR ANY DIRECT, INDIRECT, INCIDENTAL, SPECIAL,
#EXEMPLARY, OR CONSEQUENTIAL DAMAGES (INCLUDING, BUT NOT LIMITED TO,
#PROCUREMENT OF SUBSTITUTE GOODS OR SERVICES; LOSS OF USE, DATA, OR
#PROFITS; OR BUSINESS INTERRUPTION) HOWEVER CAUSED AND ON ANY THEORY OF
#LIABILITY, WHETHER IN CONTRACT, STRICT LIABILITY, OR TORT (INCLUDING
#NEGLIGENCE OR OTHERWISE) ARISING IN ANY WAY OUT OF THE USE OF THIS
#SOFTWARE, EVEN IF ADVISED OF THE POSSIBILITY OF SUCH DAMAGE.
###########################################################################################

########################################
#
# PARAMETRES DE CONFIGURATION
#
########################################
define('MYSQL_HOST', '');
define('MYSQL_USER', '');
define('MYSQL_PASS', '');
define('MYSQL_DATABASE', '');
define('BAN_FILE', './plugins/FS_captcha/captcha/FS_blacklist_total.txt');

########################################
#
# FONCTIONS
#
########################################
/**
 * Check if IP is OK and return 'not_ip_address' else
 *
 * @param	String	$ip_address	The IP address to check
 */
function clear($ip_address)
{
	// basic pattern for check the IP address
	$pattern = '/([0-9]{1,3}).([0-9]{1,3}).([0-9]{1,3}).([0-9]{1,3})/';
	if (preg_match($pattern, $ip_address) === 0) {
		// it's not a IP address, return 'not_ip_address'
		return 'not_ip_address';
	} else {
		return $ip_address;
	}
}


/**
 * Count the number of post in SPIP forum for the IP address
 *
 * @param	String	$ip_address	The IP address to check
 */
function countNbPosts($ip_address)
{
	connectDB();
	$sql = 'SELECT COUNT(*) as nb_post FROM spip_forum WHERE ip = \''.mysql_real_escape_string($ip_address).'\'';
	if ($row = execSQL($sql, true)) {
		return $row['nb_post'];
	} else {
		return 0;
	}
}

function deletePosts($ip_address)
{
	connectDB();
	$sql = 'DELETE FROM spip_forum WHERE ip = \''.mysql_real_escape_string($ip_address).'\'';
	execSQL($sql);
}

function banIP($ip_address)
{
	$handle = fopen(BAN_FILE, 'a');
	if ($handle === false) {
		die('The file '.BAN_FILE.' can\'t be open. The IP address isn\'t ban');
	}
	
	$res = fwrite($handle, "\n".$ip_address);
	if ($res === false) {
		die('The file '.BAN_FILE.' can\'t be write. The IP address isn\'t ban');
	}
	
	fclose($handle);
}

function connectDB()
{
	$conn = mysql_connect(MYSQL_HOST, MYSQL_USER, MYSQL_PASS);
	if ($conn === false) {
		die('Error in the MySQL connect');
	}
	
	if (mysql_select_db(MYSQL_DATABASE) === false) {
		die('Error in the database\'s select');
	}
	
	return true;
}

function execSQL($sql, $result = false)
{
	$res = mysql_query($sql);
	if ($res === false) {
		die('Error in the SQL request : "'.$sql.'" <br />Error : "'.mysql_error());
	}
	if ($result === true) {
		if (mysql_num_rows($res) > 0) {
			$row = mysql_fetch_assoc($res);
			return $row;
		} else {
			return false;
		}
	}
	
	return true;
}

########################################
#
# PROGRAMME PRINCIPAL
#
########################################
$first_request = true;
$check_ip = false;
$confirm = false;
if ((isset($_POST['check']) && !empty($_POST['check'])) || (isset($_POST['ban']) && !empty($_POST['ban'])) || (isset($_POST['cancel']) && !empty($_POST['cancel']))) {
	if (isset($_POST['check']) && !empty($_POST['check'])) {
		// première demande, on compte le nombre de message et on demande confirmation
		$first_request = false;
		$check_ip = true;
		$confirm = false;
		$ip_address = clear($_POST['ip_address']);
		$nb_message = countNbPosts($ip_address);
	} elseif (isset($_POST['ban']) && !empty($_POST['ban'])) {
		// confirmation, on supprime les message et on banni l'adresse IP
		$first_request = false;
		$check_ip = false;
		$confirm = true;
		$ip_address = clear($_POST['ip_address']);
		deletePosts($ip_address);
		banIP($ip_address);
	} elseif (isset($_POST['cancel']) && !empty($_POST['cancel'])) {
		$first_request = true;
		$check_ip = false;
		$confirm = false;
	} else {
		die('Hum..., tu as cliqu&eacute; sur quel bouton ?');
	}
}
?>
<html>
	<head>
		<title>Suppression de messages et bannisement d'adresse IP</title>
		<meta http-equiv="Content-Type" content="text/html; charset=utf-8" />
	</head>
	<body>
		<div class="formulaire">
			<?php if ($first_request === true) { ?>
			<form action="kill_spam.php" method="post">
				<label for="ip_address">Adresse IP : </label>
				<input type="text" name="ip_address" id="ip_address" value="" />
				<input type="submit" name="check" id="check" value="V&eacute;rifier l'adresse IP" />
			</form>
			<?php } elseif ($check_ip === true) { ?>
			<p>
				Vous avez choisi de supprimer tous les messages de l'adresse IP <?php echo $ip_address ?> ce qui 
				repr&eacute;sente <?php echo $nb_message ?> messages.
			</p>
			<p>
				En validant votre action, tous les messages de l'adresse IP <?php echo $ip_address ?> seront supprim&eacute;s
				et l'adresse IP sera d&eacute;finitivement bannie.
			</p>
			<div class="confirm_input">
				<form action="kill_spam.php" method="post">
					<input type="hidden" name="ip_address" id="ip_address" value="<?php echo $ip_address ?>" />
					<input type="submit" name="ban" value="Bannir l'adresse IP" />
					<input type="submit" name="cancel" value="Annuler" />
				</form>
			</div>
			<?php } elseif ($confirm === true) { ?>
			<p>L'adresse IP <?php echo $ip_address ?> a &eacute;t&eacute; d&eacute;finitivement bannie et tous les messages provenant de cette adresse IP ont &eacute;t&eacute; supprim&eacute;.</p>
			<p><a href="kill_spam.php" title="Retourner au formulaire de bannisement">Retour au formulaire.</a></p>
			<?php } ?>
		</div>
	</body>
</html>
