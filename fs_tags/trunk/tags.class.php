<?php
class Tags
{
	var $__id;
	var $__tag;
	var $__tagLoading;
	var $__poids;
	var $__max;
	var $__id_article;
	var $__options;
	
	
	function Tags()
	{
		$this->__construct();
	}
	
	function __construct()
	{
		$this->init();
	}
	
	
	/**
	 * Initialise la classe
	 */
	function init()
	{
		$this->__id		 = array();
		$this->__tag		= array();
		$this->__tagLoading = false;
		$this->__poids	  = array();
		$this->__id_article = 0;
		
		// options
		$this->__options['min']		 = 2;
		$this->__options['max']		 = 255;
		$this->__options['interdit']	= true;
	}
	
	
	/**
	 * Charge un article dans la classe
	 * Eventuellement charge les tags de l'article
	 *
	 * @param   Int	 $id_article	 L'identifiant de l'article à charger
	 * @param   Bool	$load_tag	   Vrai: charge les tags, Faux sinon
	 * @return  Bool					Vrai: si l'article est bien chargé, Faux sinon
	 */
	function load($id_article, $load_tag = false)
	{
		if (!is_numeric($id_article) || empty($id_article)) {
			return false;
		} else {
			$this->__id_article = $id_article;
		}
		
		if ($load_tag) {
			$this->__loadTag();
		}
	
		return true;
	}
	
	
	/**
	 * Génére un tableau avec les identifiants des tags, les tags
	 * et les tags génériques de l'article
	 *
	 * @return  Array				   Le tableau avec les identifiants des tags, les tags et les tags générique
	 */
	function showTags()
	{
		$tags = array();
		if (!$this->__tagLoading) {
			if (!$this->__loadTag()) {
				return $tags;
			}
		}
		
		$nb_tags = count($this->__id);
		if ($nb_tags == 0) {
			return $tags;
		}
		
		for ($i = 0; $i < $nb_tags; $i++) {
			$tags[$i]['id'] = $this->__id[$i];
			$tags[$i]['tag'] = $this->__tag[$i];
			$tags[$i]['gen'] = $this->__tagGenerique($this->__tag[$i]);
		}
	
		return $tags;
	}
	
	
	/**
	 * Ajout un tag à l'article
	 *
	 * @param   Str	 $tag			Le tag a ajouter à l'article
	 * @return  Bool					Vrai: le tag a bien été ajouté, Faux: sinon
	 */
	function addTag($tag)
	{
		if (empty($tag)) {
			return false;
		}
		
		if (empty($this->__id_article)) {
			return false;
		}
		
		$tag_generique = $this->__tagGenerique($tag);
		$len = strlen($tag_generique);
		if ($len < $this->__options['min'] || $len > $this->__options['max']) {
			return false;
		}
		
		if ($this->__tagInterdit($tag_generique, true)) {
			return false;
		}
		
		$id_tag = $this->__searchTag($tag_generique, true);
		$insert = false;
		if($id_tag == 0) {
			$sql = 'INSERT INTO spip_tags (tag, tag_generique)
					VALUES
					(\''.mysql_real_escape_string($tag).'\', \''.mysql_real_escape_string($tag_generique).'\')';
			$res = spip_query($sql);
			if (!$res) {
				return false;
			}
			$id_tag = spip_insert_id();
			$insert = true;
		}
	
		if (!$this->__tagLoading) {
			if (!$this->__loadTag()) {
				return false;
			}
		}
		
		if (!in_array($id_tag, $this->__id)) {
			$insert = true;
		}
			
		if ($insert) {
			$sql = 'INSERT INTO spip_tags_articles (id_tag, id_article, poids)
					VALUES
					(\''.$id_tag.'\', \''.$this->__id_article.'\', 1)';
			$res = spip_query($sql);
		} else {
			$sql = 'UPDATE spip_tags_articles 
					SET poids=poids+1
					WHERE id_tag='.$id_tag.' AND id_article='.$this->__id_article;
			$res = spip_query($sql);
		}
		
		return $res;
	}
	
	
	/**
	 * Supprime un tag associé à l'article
	 *
	 * @param   Int	 $id_tag		 L'identifiant du tag à supprimer
	 * @param   Bool	$all			Vrai: supprime entièrement le tag de tous les articles
	 * @return  Bool					Vrai: le tag est supprimé
	 */
	function delTag($id_tag, $all = false)
	{
		if (empty($this->__id_article) && !$all) {
			return false;
		}
		
		if (!is_numeric($id_tag) || empty($id_tag)) {
			return false;
		}
		
		$sql = 'DELETE FROM spip_tags_articles
				WHERE id_tag='.$id_tag;
		if (!$all) {
			$sql .= ' AND id_article='.$this->__id_article;
		}
		$res = spip_query($sql);
		if (!$res) {
			return false;
		}
		
		if ($all) {
			// on insere le tag dans les mots interdits
			$tag_interdit =  $this->__tagGenerique($this->__genTag($id_tag));
			$sql = 'INSERT INTO spip_tags_interdit (tag_generique) VALUES (\''.$tag_interdit.'\')';
			$res = spip_query($sql);
			if (!$res) {
				return false;
			}
		
			// on supprime le tag de la liste
			$sql = 'DELETE FROM spip_tags
					WHERE id_tag='.$id_tag;
			$res = spip_query($sql);
			if (!$res) {
				return false;
			}
		} else {
			$sql = 'SELECT count(*) as nb FROM spip_tags_articles
					WHERE id_tag='.$id_tag;
			$res = spip_query($sql);
			if (!$res) {
				return false;
			}
			$row = spip_fetch_array($res);
			if ($row['nb'] == 0) {
				// on supprime le tag de la liste
				$sql = 'DELETE FROM spip_tags
						WHERE id_tag='.$id_tag;
				$res = spip_query($sql);
				if (!$res) {
					return false;
				}
			}
		}
		
		return true;
	}
	
	
	/**
	 * Renvoie la liste des articles associé au tag
	 *
	 * @param   Str	 $tag			Le tag recherché
	 * @param   Bool	$generique	  Vrai: le tag est au format générique, Faux sinon
	 * @return  Array				   Le tableau des articles associé au tag
	 */
	function articlesTag($tag, $generique = false)
	{
		if ($generique) {
			$tag = $this->__tagGenerique($tag);
		}
		
		$articles = array();
		$id_tag = $this->__searchTag($tag);
		if ($id_tag == 0) {
			return $articles;
		}
		
		$sql = 'SELECT DISTINCT spip_tags_articles.id_article as id, titre
				FROM spip_tags_articles
				LEFT JOIN spip_articles ON spip_tags_articles.id_article=spip_articles.id_article
				WHERE id_tag='.$id_tag;
		$res = spip_query($sql);
		if (!$res) {
			return $articles;
		}
		
		$i = 0;
		while ($row = spip_fetch_array($res)) {
			$articles[$i]['id'] = $row['id'];
			$articles[$i]['titre'] = $row['titre'];
			$i++;
		}
		
		return $articles;
	}
	
	
	/**
	 * Charge les tags relatif à l'article
	 *
	 * @return  Bool					Vrai: si les tags sont bien cargé, Faux sinon
	 */
	function __loadTag()
	{
		if (empty($this->__id_article)) {
			return false;
		}
		
		$sql = 'SELECT id_tag, count(id_tag) as nb_tag
				FROM spip_tags_articles
				WHERE id_article='.$this->__id_article.'
				GROUP BY id_tag';
		$res = spip_query($sql);
		if (!$res) {
			return false;
		}
		
		while ($row = spip_fetch_array($res)) {
			$this->__id[]   = $row['id_tag'];
			$this->__poids[]= $row['nb_tag'];
			if ($this->__max < $row['nb_tag']) {
				$this->__max = $row['nb_tag'];
			}
			$this->__tag[]	= $this->__genTag($row['id_tag']);
		}
		
		$this->__tagLoading = true;
	
		return true;
	}
	
	
	/**
	 * Renvoie le tag
	 *
	 * @param   Int	 $id_tag		 L'identifiant du tag
	 * @return  Str					 Le tag ou une chaine vide en cas d'erreur
	 */
	function __genTag($id_tag)
	{
		if (!is_numeric($id_tag) || empty($id_tag)) {
			return '';
		}
		
		$sql = 'SELECT tag
				FROM spip_tags
				WHERE id_tag='.$id_tag;
		$res = spip_query($sql);
		if (!$res) {
			return false;
		}
		
		$row = spip_fetch_array($res);
		if ($row === false) {
			return '';
		}
		
		return $row['tag'];
	}
	
	
	/**
	 * Vérifie que le tag ne fait pas parti des tags interdit
	 *
	 * @param   Str	 $tag			Le tag
	 * @param   Bool	$generique	  Vrai: le tag est le tag générique, Faux sinon
	 * @return  Bool					Vrai: le mot est interdit, Faux sinon
	 */
	function __tagInterdit($tag, $generique = false)
	{
		if (!$generique) {
			$tag = $this->__tagGenerique($tag);
		}
		
		$sql = 'SELECT count(id_tags_interdit) as nb
				FROM spip_tags_interdit
				WHERE tag_generique=\''.$tag.'\'';
		$res = spip_query($sql);
		if (!$res) {
			return true;
		}
		
		$row = spip_fetch_array($res);
		if ($row["nb"] > 0) {
			return true;
		} else {
			return false;
		}
	}
	
	
	function __searchTag($tag, $generique = false)
	{
		if (!$generique) {
			$tag = $this->__tagGenerique($tag);
		}
		$sql = 'SELECT id_tag
				FROM spip_tags
				WHERE tag_generique=\''.$tag.'\'';
		$res = spip_query($sql);
		if (!$res) {
			return 0;
		}
		
		$row = spip_fetch_array($res);
		if ($row === false) {
			return 0;
		} else {
			return $row['id_tag'];
		}
	}
	
	/**
	 * Transforme le tag en tag générique, c'est à dire débarassé de ses caractères spéciaux
	 *
	 * @param   Str	 $str			Le tag dans sa forme brute
	 * @return  Str					 Le tag dans sa form générique
	 */
	function __tagGenerique($str)
	{
		$str = trim($str);
		
		// A
		$str = str_replace("À", "A", $str);
		$str = str_replace("Á", "A", $str);
		$str = str_replace("Â", "A", $str);
		$str = str_replace("Ã", "A", $str);
		$str = str_replace("Ä", "A", $str);
		$str = str_replace("Å", "A", $str);
		$str = str_replace("à", "a", $str);
		$str = str_replace("á", "a", $str);
		$str = str_replace("â", "a", $str);
		$str = str_replace("ã", "a", $str);
		$str = str_replace("ä", "a", $str);
		$str = str_replace("å", "a", $str);
		
		// O
		$str = str_replace("Ò", "O", $str);
		$str = str_replace("Ó", "O", $str);
		$str = str_replace("Ô", "O", $str);
		$str = str_replace("Õ", "O", $str);
		$str = str_replace("Ö", "O", $str);
		$str = str_replace("Ø", "O", $str);
		$str = str_replace("ò", "o", $str);
		$str = str_replace("ó", "o", $str);
		$str = str_replace("ô", "o", $str);
		$str = str_replace("õ", "o", $str);
		$str = str_replace("ö", "o", $str);
		$str = str_replace("ø", "o", $str);
		
		// E
		$str = str_replace("È", "E", $str);
		$str = str_replace("É", "E", $str);
		$str = str_replace("Ê", "E", $str);
		$str = str_replace("Ë", "E", $str);
		$str = str_replace("è", "e", $str);
		$str = str_replace("é", "e", $str);
		$str = str_replace("ê", "e", $str);
		$str = str_replace("ë", "e", $str);
		
		// C
		$str = str_replace("Ç", "C", $str);
		$str = str_replace("ç", "c", $str);
		
		// I
		$str = str_replace("Ì", "I", $str);
		$str = str_replace("Í", "I", $str);
		$str = str_replace("Î", "I", $str);
		$str = str_replace("Ï", "I", $str);
		$str = str_replace("ì", "i", $str);
		$str = str_replace("í", "i", $str);
		$str = str_replace("î", "i", $str);
		$str = str_replace("ï", "i", $str);
		
		// U
		$str = str_replace("Ù", "U", $str);
		$str = str_replace("Ú", "U", $str);
		$str = str_replace("Û", "U", $str);
		$str = str_replace("Ü", "U", $str);
		$str = str_replace("ù", "u", $str);
		$str = str_replace("ú", "u", $str);
		$str = str_replace("û", "u", $str);
		$str = str_replace("ü", "u", $str);
		
		// Y
		$str = str_replace("ÿ", "y", $str);
		
		// N
		$str = str_replace("Ñ", "N", $str);
		$str = str_replace("ñ", "n", $str);
		
		// Devise
		$str = str_replace("", "", $str);
		$str = str_replace("£", "", $str);
		$str = str_replace("¥", "", $str);
		
		// Symbole
		$str = str_replace("©", "", $str);
		$str = str_replace("®", "", $str);
		$str = str_replace("", "", $str);
		$str = str_replace("<", "&lt;", $str);
		$str = str_replace(">", "&gt;", $str);
		$str = str_replace("µ", "", $str);
		
		// Ligature
		$str = str_replace("Æ", "AE", $str);
		$str = str_replace("æ", "ae", $str);
		$str = str_replace("", "OE", $str);
		$str = str_replace("", "oe", $str);
		$str = str_replace("ß", "sz", $str);

		// Majuscule
		$str = strtolower($str);

		return $str;
	}
}
?>