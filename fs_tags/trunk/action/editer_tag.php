<?php

if (!defined("_ECRIRE_INC_VERSION")) return;

include_once _DIR_PLUGINS.'/Tags/tags_fns.php';

$args = explode(',', $_GET['arg']);

switch ($args[1]) {
	case 'add_tag':
		if (!empty($_POST['mail']) || !empty($_POST['name'])) {
			header('Location: '.$_GET['redirect']);
			exit();
		}
		tagsAdd($_POST['tags'], $args[2]);
		break;
	case 'del_tag':
		if (!autoriser('modifier', 'article', $args[2])) {
			header('Location: '.$_GET['redirect']);
			exit();
		}
		delTag($args[0], $args[2]);
		break;
	case 'del_tag_all':
		if (!autoriser('modifier', 'article')) {
			header('Location: '.$_GET['redirect']);
			exit();
		}
		delTagTotaly($args[0]);
		break;
}

header('Location: '.$_GET['redirect']);
exit();
?>