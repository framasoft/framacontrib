<?php
if (!defined("_ECRIRE_INC_VERSION")) return;

include_spip('inc/presentation');
include_spip('inc/texte');
include_spip('inc/actions');
include_once _DIR_PLUGINS.'Tags/tags_fns.php';

function exec_tags_dist()
{
	global $debut;
	
	$debut= intval($debut);
	
	pipeline('exec_init',array('args'=>array('exec'=>'tags', 'pages' => '1'),'data'=>''));

	$commencer_page = charger_fonction('commencer_page', 'inc');
	echo $commencer_page("&laquo; Gestion des tags &raquo;", "naviguer", "tags");

	echo '<br /><br /><br />';
	echo gros_titre('Derniers tags ins�r�s','',false);
	debut_gauche();
	creer_colonne_droite();
	echo debut_boite_info(true),
		 'Supprimez les tags qui n\'ont rien � faire sur votre site',
		 fin_boite_info(true);
	debut_droite();
	
	echo debut_cadre(''),
		 showPageTags($debut),
		 showLastTags($debut),
		 fin_cadre('');
	
	echo fin_gauche(), fin_page();
}
?>