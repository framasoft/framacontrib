<?php
include_once _DIR_PLUGINS.'Tags/tags.class.php';

function elementsInterdit($str) {
	$interdits = array('http','ftp','www','\'','#','"','.com','.net','.fr','@', 'url',
					   '{', '(', '[', ']', ')', '}', '|', '`', '\\', '!', '?', ':', '/');
	foreach ($interdits as $interdit) {
		if (stristr($str, $interdit) !== false) {
			return false;
		}
	}
	
	return true;
}


/**
 * Fonction permettant d'ajouter des tags à un article
 *
 * @param   Str     $liste_tags         Liste des tags à ajouter
 * @param   int     $id_article         L'identifiant de l'article
 */
function tagsAdd($liste_tags, $id_article)
{
    if (!elementsInterdit($liste_tags)) {
		return false;
    }
    
    $liste_tags = str_replace(' ', ',', $liste_tags);
    $liste_tags = str_replace(';', ',', $liste_tags);
    
    $tags = explode(',', $liste_tags);
    
    $KTags = new Tags();
    if (!$KTags->load($id_article)) {
        return false;
    }
    
    foreach($tags as $tag) {
        $KTags->addTag($tag);
    }
    
    return true;
}


/**
 * Fonction permettant de supprimer un tag
 *
 * @param   Int     $id_tag             Identifiant du tag à supprimer
 * @param   Int     $id_article         Identifiant de l'article
 */
function delTag($id_tag, $id_article)
{
    $KTags = new Tags();
    if (!$KTags->load($id_article)) {
        return false;
    }
    return $KTags->delTag($id_tag);
}


/**
 * Fonction permettant de supprimer totalement un tag
 *
 * @param   Int     $id_tag             Identifiant du tag à supprimer
 */
function delTagTotaly($id_tag)
{
    $KTags = new Tags();
    return $KTags->delTag($id_tag, true);
}


/**
 * Fonction permettant d'afficher les tags en liste et avec les icônes
 */
function showTags($id_article)
{
    $KTags = new Tags();
    if (!$KTags->load($id_article)) {
        return false;
    }
    
    $tags = $KTags->showTags();
    $nbTags = count($tags);
    $return_value = '';
    for ($i = 0; $i < $nbTags; $i++) {
		$return_value .= '<li>'.$tags[$i]['tag'];
		$return_value .= ' '.ajax_action_auteur('editer_tag', $tags[$i]['id'].',del_tag,'.$id_article, 'articles', 'id_article='.$id_article,  array('<img src="'._DIR_PLUGINS.'Tags/img_pack/croix-rouge.png" alt="Retirer le mot clé associé à cet article" class="puce">',''), '');
		$return_value .= ' '.ajax_action_auteur('editer_tag', $tags[$i]['id'].',del_tag_all', 'articles', 'id_article='.$id_article,  array('<img src="'._DIR_PLUGINS.'Tags/img_pack/croix-noire.png" alt="Supprimer totalement le mot clé (sur cet article et tous les autres et le passer en mots interdit)" class="puce">',''), '');
    }
    if (!empty($return_value)) {
        $return_value = '<ul>'.$return_value.'</ul>';
    }
    
    return $return_value;
}


/**
 * Fonction permettant d'afficher les derniers tags
 */
function showLastTags($page = 1)
{
	$nb_items = 30;
	
	if ($page < 1 || !is_numeric($page)) {
		$page = 1;
	}
	$debut = ($page - 1) * $nb_items;
	
	$sql = 'SELECT * FROM spip_tags ORDER BY id_tag DESC LIMIT '.$debut.', '.$nb_items;
	$res = spip_query($sql);
    $return_value = '<ul style="text-align: left;">';
	while ($row = spip_fetch_array($res)) {
		$return_value .= '<li><a href="/spip.php?page=tag&id_tag='.$row['id_tag'].'">'.$row['tag'].'</a>';
		$return_value .= ' '.ajax_action_auteur('editer_tag', $row['id_tag'].',del_tag_all', 'tags', 'debut='.$page,  array('<img src="'._DIR_PLUGINS.'Tags/img_pack/croix-noire.png" alt="Supprimer totalement le mot clé (sur cet article et tous les autres et le passer en mots interdit)" class="puce">',''), '');
	}
	$return_value .= '</ul>';
	
	return $return_value;
}


/**
 * Permet de générer la liste des pages
 */
function showPageTags($page)
{
	$nb_items = 30;
	
	if ($page < 1 || !is_numeric($page)) {
		$page = 1;
	}
	
	$sql = 'SELECT count(*) as nb FROM spip_tags';
	$res = spip_query($sql);
	$row = spip_fetch_array($res);
	
	$nb = $row['nb'];
	$nb_page = ceil($nb / $nb_items);
	
	$return_value = '';
	for ($i = 1; $i <= $nb_page; $i++) {
		$page_v = ($i - 1) * $nb_items;
		if ($i > 1) {
			$return_value .= ' | ';
		}
		if ($i == $page) {
			$return_value .= '<b>'.$page_v.'</b>';
		} else {
			$return_value .= '<a href="'.generer_url_ecrire('tags', 'debut='.$i).'">'.$page_v.'</a>';
		}
	}
	
	return $return_value;
}
?>