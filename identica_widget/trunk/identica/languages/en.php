<?php
  $english = array(
    /**
     * identica widget details
     */
    'identica:username' => 'Enter your identi.ca username.',
    'identica:num'      => 'The number of tweets to show.',
    'identica:visit'    => 'visit my identi.ca',
    'identica:notset'   => 'This identica widget is not yet set to go. To display your latest tweets, click on - edit - and fill in your details',
    
    /**
     * identica widget time
     */
    'identica:identica_less_60'           => 'less than a minute ago',
    'identica:identica_minute'            => 'about a minute ago',
    'identica:identica_more_minute_first' => '',
    'identica:identica_more_minute'       => ' minutes ago',
    'identica:identica_hour'              => 'about an hour ago',
    'identica:identica_more_hour_first'   => 'about ',
    'identica:identica_more_hour'         => ' hours ago',
    'identica:identica_day'               => '1 day ago',
    'identica:identica_more_day_first'    => '',
    'identica:identica_more_day'          => ' days ago',
    
    /**
     * identica widget river
     */
    'identica:river:created'  => "%s added the identica widget.",
    'identica:river:updated'  => "%s updated their identica widget.",
    'identica:river:delete'   => "%s removed their identica widget.",
  );


add_translation("en",$english);