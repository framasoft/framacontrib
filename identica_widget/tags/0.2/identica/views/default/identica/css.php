<?php
  /**
   * Elgg identica CSS (based in twitter widget for Elgg of Curverider Ltd)
   *
   * @package ElggIdentica
   * @license http://www.gnu.org/licenses/old-licenses/gpl-2.0.html GNU Public License version 2
   * @author Curverider <info@elgg.com>
   * @author Simon Leblanc <contact@leblanc-simon.eu>
   * @copyright Simon Leblanc 2009
   * @link http://elgg.com/
   */
?>

#identica_widget {
    margin:0 10px 0 10px;
}

#identica_widget ul {
	margin:0;
	padding:0;
}

#identica_widget li {
	background: url(<?php echo $vars['url']; ?>mod/identica/graphics/thewire_speech_bubble.gif) no-repeat right bottom;
	list-style-image:none;
	list-style-position:outside;
	list-style-type:none;
	margin:0 0 5px 0;
	padding:0;
	overflow-x: hidden;
}

#identica_widget li span {
	color:#666666;
	background:white;
	-webkit-border-radius: 8px; 
	-moz-border-radius: 8px;
	padding:5px;
	display:block;
}

p.visit_identica a {
    background:url(<?php echo $vars['url']; ?>mod/identica/graphics/identica.png) left no-repeat;
    padding:0 0 0 20px;
    margin:0;
}
.visit_identica {
	background:white;
	-webkit-border-radius: 8px; 
	-moz-border-radius: 8px;
	padding:2px;
	margin:0 0 5px 0;
}

#identica_widget li a {
	display:block;
	margin:0 0 0 4px;
}

#identica_widget li span a {
	display:inline !important;
	margin-left: 0px !important;
}