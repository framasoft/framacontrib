<?php
  $french = array(
    /**
     * identica widget details
     */
    'identica:username' => 'Entrez votre nom d\'utilisateur Identi.ca.',
    'identica:num'      => 'Le nombre de tweets à voir.',
    'identica:visit'    => 'Visiter mon Identi.ca',
    'identica:notset'   => 'Ce widget Identi.ca n\'est pas encore configuré. Pour voir vos derniers tweets, cliquez sur - éditer - et remplissez les détails',
    
    /**
     * identica widget time
     */
    'identica:identica_less_60'           => 'il y a quelques secondes',
    'identica:identica_minute'            => 'il y a 1 minute',
    'identica:identica_more_minute_first' => 'il y a ',
    'identica:identica_more_minute'       => ' minutes',
    'identica:identica_hour'              => 'il y a 1 heure',
    'identica:identica_more_hour_first'   => 'il y a ',
    'identica:identica_more_hour'         => ' heures',
    'identica:identica_day'               => 'il y a 1 jour',
    'identica:identica_more_day_first'    => 'il y a ',
    'identica:identica_more_day'          => ' jours',
    
    /**
     * identica widget river
     */
    'identica:river:created'  => "%s a ajouté le widget identica.",
    'identica:river:updated'  => "%s a mis à jour son widget identica.",
    'identica:river:delete'   => "%s a supprimé son widget identica.",
  );


add_translation("fr", $french);