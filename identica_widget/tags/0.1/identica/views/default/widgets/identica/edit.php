<?php
  /**
   * Elgg identica edit page
   * Based in the twitter widget for Elgg of Curverider Ltd
   *
   * @package ElggIdentica
   * @license http://www.gnu.org/licenses/old-licenses/gpl-2.0.html GNU Public License version 2
   * @author Curverider Ltd <info@elgg.com>
   * @author Simon Leblanc <contact@leblanc-simon.eu>
   * @copyright Simon Leblanc 2009
   * @link http://elgg.com/
   */
?>
  <p>
    <?php echo elgg_echo("identica:username"); ?>
    <input type="text" name="params[identica_username]" value="<?php echo htmlentities($vars['entity']->identica_username); ?>" />
    <br />
    <?php echo elgg_echo("identica:num"); ?>
    <input type="text" name="params[identica_num]" value="<?php echo htmlentities($vars['entity']->identica_num); ?>" />
  </p>