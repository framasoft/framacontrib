<?php
  /**
   * Elgg identi.ca view page
   * Based in the twitter widget for Elgg of Curverider Ltd
   *
   * @package ElggIdentica
   * @license http://www.gnu.org/licenses/old-licenses/gpl-2.0.html GNU Public License version 2
   * @author Curverider Ltd <info@elgg.com>
   * @author Simon Leblanc <contact@leblanc-simon.eu>
   * @copyright Simon Leblanc 2009
   * @link http://elgg.com/
   */
  
  //some required params
  $username = $vars['entity']->identica_username;
  $num = $vars['entity']->identica_num;
?>
  <div id="identica_widget">
  <?php
  // if the identica username is empty, then do not show
  if ($username) {
  ?>
    <ul id="identica_update_list"></ul>
    <p class="visit_identica"><a href="http://identi.ca/<?php echo $username; ?>"><?php echo elgg_echo("identica:visit"); ?></a></p>
    <script type="text/javascript" src="<?php echo $vars['url']; ?>mod/identica/views/default/identica/identi.ca.js"></script>
    <script type="text/javascript" src="https://identi.ca/api/statuses/user_timeline/<?php echo $username; ?>.json?callback=identicaCallback&count=<?php echo $num; ?>"></script>
  <?php
  } else {
    echo "<div class=\"contentWrapper\"><p>" . elgg_echo("identica:notset") . ".</p></div>";
  }
  ?>
  </div>