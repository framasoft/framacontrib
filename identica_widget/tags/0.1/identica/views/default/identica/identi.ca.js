  /**
   * Elgg identica JS
   *
   * @package ElggIdentica
   * @license http://www.gnu.org/licenses/old-licenses/gpl-2.0.html GNU Public License version 2
   * @author Simon Leblanc <contact@leblanc-simon.eu>
   * @copyright Simon Leblanc 2009
   * @link http://elgg.com/
   */

/**
 * Parse the JSON result for show this in HTML
 *
 * @param   JSON string    the JSON result of the identi.ca's call
 */
function identicaCallback(identicas) {
  var statusHTML = [];
  for (var i=0; i<identicas.length; i++){
    var username = identicas[i].user.screen_name;
    var status = identicas[i].text.replace(/((https?|s?ftp|ssh)\:\/\/[^"\s\<\>]*[^.,;'">\:\s\<\>\)\]\!])/g, function(url) {
      return '<a href="'+url+'">'+url+'</a>';
    }).replace(/\B@([_a-z0-9]+)/ig, function(reply) {
      return  reply.charAt(0)+'<a href="http://identi.ca/'+reply.substring(1)+'">'+reply.substring(1)+'</a>';
    }).replace(/\B#([-_a-z0-9]+)/ig, function(tag) {
      return  tag.charAt(0)+'<a href="http://identi.ca/tag/'+tag.substring(1).replace(/[^a-z0-9]+/ig, '')+'">'+tag.substring(1)+'</a>';
    }).replace(/\B!([_a-z0-9]+)/ig, function(group) {
      return  group.charAt(0)+'<a href="http://identi.ca/group/'+group.substring(1)+'">'+group.substring(1)+'</a>';
    });
    statusHTML.push('<li><span>'+status+'</span> <a style="font-size:85%" href="http://identi.ca/notice/'+identicas[i].id+'">'+relative_time(identicas[i].created_at)+'</a></li>');
  }
  document.getElementById('identica_update_list').innerHTML = statusHTML.join('');
}

/**
 * Modified the time in human readable
 *
 * @param   string    time_value    The time in RFC format
 * @return  string                  The time in human readable format
 */
function relative_time(time_value) {
  var values = time_value.split(" ");
  time_value = values[1] + " " + values[2] + ", " + values[5] + " " + values[3];
  var parsed_date = Date.parse(time_value);
  var relative_to = (arguments.length > 1) ? arguments[1] : new Date();
  var delta = parseInt((relative_to.getTime() - parsed_date) / 1000);
  delta = delta + (relative_to.getTimezoneOffset() * 60);

  if (delta < 60) {
    return 'less than a minute ago';
  } else if(delta < 120) {
    return 'about a minute ago';
  } else if(delta < (60*60)) {
    return (parseInt(delta / 60)).toString() + ' minutes ago';
  } else if(delta < (120*60)) {
    return 'about an hour ago';
  } else if(delta < (24*60*60)) {
    return 'about ' + (parseInt(delta / 3600)).toString() + ' hours ago';
  } else if(delta < (48*60*60)) {
    return '1 day ago';
  } else {
    return (parseInt(delta / 86400)).toString() + ' days ago';
  }
}