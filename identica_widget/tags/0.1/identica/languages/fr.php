<?php
  $english = array(
    /**
     * identica widget details
     */
    'identica:username' => 'Entrez votre nom d\'utilisateur Identi.ca.',
    'identica:num'      => 'Le nombre de tweets à voir.',
    'identica:visit'    => 'Visiter mon Identi.ca',
    'identica:notset'   => 'Ce widget Identi.ca n\'est pas encore configuré. Pour voir vos derniers tweets, cliquez sur - éditer - et remplissez les détails',
    
    /**
     * identica widget river
     */
    'identica:river:created'  => "%s a ajouté le widget identica.",
    'identica:river:updated'  => "%s a mis à jour son widget identica.",
    'identica:river:delete'   => "%s a supprimé son widget identica.",
  );


add_translation("fr",$english);