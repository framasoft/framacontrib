<?php
  $english = array(
    /**
     * identica widget details
     */
    'identica:username' => 'Enter your identi.ca username.',
    'identica:num'      => 'The number of tweets to show.',
    'identica:visit'    => 'visit my identi.ca',
    'identica:notset'   => 'This identica widget is not yet set to go. To display your latest tweets, click on - edit - and fill in your details',
    
    /**
     * identica widget river
     */
    'identica:river:created'  => "%s added the identica widget.",
    'identica:river:updated'  => "%s updated their identica widget.",
    'identica:river:delete'   => "%s removed their identica widget.",
  );


add_translation("en",$english);