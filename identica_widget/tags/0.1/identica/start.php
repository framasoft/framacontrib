<?php
  /**
   * Elgg Identi.ca widget
   * This plugin allows users to pull in their identi.ca feed to display on their profile
   * Based in the twitter widget for Elgg of Curverider Ltd
   *
   * @package ElggIdentica
   * @license http://www.gnu.org/licenses/old-licenses/gpl-2.0.html GNU Public License version 2
   * @author Curverider Ltd <info@elgg.com>
   * @author Simon Leblanc <contact@leblanc-simon.eu>
   * @copyright Simon Leblanc 2009
   * @link http://elgg.com/
   */
  
  function identica_init()
  {
    //extend css if style is required
    extend_view('css','identica/css');
    
    //add a widget
    add_widget_type('identica', "Identi.ca", "This is your identi.ca feed");
    
  }
  
  register_elgg_event_handler('init','system','identica_init');